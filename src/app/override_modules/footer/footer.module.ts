import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterRoutingModule } from './footer-routing.module';
import { FooterComponent } from './footer.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { FooterControlComponent } from './footer-control/footer-control.component';
import { AllFooterComponent } from './all-footer/all-footer.component';


@NgModule({
  declarations: [FooterComponent, FooterControlComponent, AllFooterComponent],
  imports: [
    CommonModule,
    FooterRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class FooterModule { }
