import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-footer-control',
  templateUrl: './footer-control.component.html',
  styleUrls: ['./footer-control.component.scss']
})
export class FooterControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'footer',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'headerSection',
          label: this.translate.instant('Head Text'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'titleheader',
          label: this.translate.instant('Title 1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'titleCard',
          label: this.translate.instant('Title 2'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'descriptionCard',
          label: this.translate.instant('Description'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text',
          name: 'phoneNumber',
          label: this.translate.instant('Phone Numpers'),
          required: true,
          value: '',
          validators: [Validators.required, Validators.pattern(this.override.phoneNumberRegExp)],
          class: 'col-md-6'
        },
        {
          type: 'text',
          name: 'email',
          label: this.translate.instant('Email'),
          required: true,
          value: '',
          multiline: false,
          validators: [Validators.required, Validators.minLength(3), Validators.email],
          class: 'col-md-6'
        },
        {
          type: 'text',
          name: 'address',
          label: this.translate.instant('Address'),
          required: true,
          value: '',
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService,
    private override: OverrideService,
  ) { }

  ngOnInit(): void {
  }

}
