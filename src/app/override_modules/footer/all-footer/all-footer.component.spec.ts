import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllFooterComponent } from './all-footer.component';

describe('AllFooterComponent', () => {
  let component: AllFooterComponent;
  let fixture: ComponentFixture<AllFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
