import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-footer',
  templateUrl: './all-footer.component.html',
  styleUrls: ['./all-footer.component.scss']
})
export class AllFooterComponent implements OnInit {

    entityData: entityData = {
      name: 'footer',
      firestore: true,
      // path:'/pages/Brand',
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Head Text'),
            field: this.override.isRTL() ? 'headerSection.ar' : 'headerSection.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Title 1'),
            field: this.override.isRTL() ? 'titleheader.ar' : 'titleheader.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Title 2'),
            field: this.override.isRTL() ? 'titleCard.ar' : 'titleCard.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Description'),
            field: this.override.isRTL() ? 'descriptionCard.ar' : 'descriptionCard.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Phone Numpers'),
            field: this.override.isRTL() ? 'phoneNumber' : 'phoneNumber',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Email'),
            field: this.override.isRTL() ? 'email' : 'email',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

          {
            headerName: this.translate.instant('Address'),
            field: this.override.isRTL() ? 'address' : 'address',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: true,
      view: false,
      add: true,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,

      ) { }

  ngOnInit(): void {
  }

}
