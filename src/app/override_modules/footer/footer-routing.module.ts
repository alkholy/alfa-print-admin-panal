import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FooterComponent } from './footer.component';
import { AllFooterComponent } from './all-footer/all-footer.component';
import { FooterControlComponent } from './footer-control/footer-control.component';

const routes: Routes = [
  { path: '', component: FooterComponent , children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllFooterComponent },
    {path: 'control', component: FooterControlComponent },
    {path: 'edit/:entityId', component: FooterControlComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FooterRoutingModule { }
