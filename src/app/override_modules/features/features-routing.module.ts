import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeaturesComponent } from './features.component';
import { AllFeaturesComponent } from './all-features/all-features.component';
import { FeaturesControlComponent } from './features-control/features-control.component';

const routes: Routes = [
  { path: '', component: FeaturesComponent, children:[
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllFeaturesComponent },
    {path: 'control', component: FeaturesControlComponent },
    {path: 'edit/:entityId', component: FeaturesControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
