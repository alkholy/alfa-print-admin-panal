import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-features-control',
  templateUrl: './features-control.component.html',
  styleUrls: ['./features-control.component.scss']
})
export class FeaturesControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'features',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'texthead1',
          label: this.translate.instant('Texthead1'),
          required: true,
          value: {ar: '', en: ''},
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'texthead2',
          label: this.translate.instant('Texthead2'),
          required: true,
          value: {ar: '', en: ''},
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'texthead3',
          label: this.translate.instant('Texthead3'),
          required: true,
          value: {ar: '', en: ''},
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
         {
            type: 'array',
            name: 'cards',
            label: this.translate.instant('benefits'),
            required: true,
            // value: '',
            children: [
              {
                type: 'text',
                name: 'text_en',
                label: this.translate.instant('Text Head Card in English'),
                required: true,
                value: '',
                multiline: false,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-6'
              },
              {
                type: 'text',
                name: 'text_ar',
                label: this.translate.instant('Text Head Card in Arabic'),
                required: true,
                value: '',
                multiline: false,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-6'
              },
              {
                type: 'text',
                name: 'bodycard_en',
                label: this.translate.instant('Body Card in English'),
                required: true,
                value: '',
                multiline: true,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-6'
              },
              {
                type: 'text',
                name: 'bodycard_ar',
                label: this.translate.instant('Body Card in Arabic'),
                required: true,
                value: '',
                multiline: true,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-6'
              },
              {
                type: 'file',
                name: 'image',
                label: this.translate.instant('Image'),
                required: false,
                value: '',
                // validators: [Validators.required],
                class: 'col-md-6'
              }
            ],

          },

      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
