import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-features',
  templateUrl: './all-features.component.html',
  styleUrls: ['./all-features.component.scss']
})
export class AllFeaturesComponent implements OnInit {

    entityData: entityData = {
      name: 'features',
      // path:'/pages/Brand',
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead1'),
            field: this.override.isRTL() ? 'texthead1.ar' : 'texthead1.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead2'),
            field: this.override.isRTL() ? 'texthead2.ar' : 'texthead2.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead3'),
            field: this.override.isRTL() ? 'texthead3.ar' : 'texthead3.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: true,
      view: false,
      add: true,
      firestore: true,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,

     ) { }


  ngOnInit(): void {
  }

}
