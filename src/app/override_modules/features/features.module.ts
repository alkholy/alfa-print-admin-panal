import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesComponent } from './features.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllFeaturesComponent } from './all-features/all-features.component';
import { FeaturesControlComponent } from './features-control/features-control.component';


@NgModule({
  declarations: [FeaturesComponent, AllFeaturesComponent, FeaturesControlComponent],
  imports: [
    CommonModule,
    FeaturesRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class FeaturesModule { }
