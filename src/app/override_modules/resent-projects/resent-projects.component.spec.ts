import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResentProjectsComponent } from './resent-projects.component';

describe('ResentProjectsComponent', () => {
  let component: ResentProjectsComponent;
  let fixture: ComponentFixture<ResentProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResentProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResentProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
