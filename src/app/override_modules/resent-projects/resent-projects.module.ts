import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResentProjectsRoutingModule } from './resent-projects-routing.module';
import { ResentProjectsComponent } from './resent-projects.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { RecentProjectsControlComponent } from './recent-projects-control/recent-projects-control.component';
import { AllRecentProjectsComponent } from './all-recent-projects/all-recent-projects.component';


@NgModule({
  declarations: [ResentProjectsComponent, RecentProjectsControlComponent, AllRecentProjectsComponent],
  imports: [
    CommonModule,
    ResentProjectsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class ResentProjectsModule { }
