import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-recent-projects-control',
  templateUrl: './recent-projects-control.component.html',
  styleUrls: ['./recent-projects-control.component.scss']
})
export class RecentProjectsControlComponent implements OnInit {

  form: genericForm = {
    entityName: 'resentProjects',
    firestore: true,
    fields: [
      {
        type: 'text-localized',
        name: 'headsectiontitle1',
        label: this.translate.instant('Title1'),
        required: true,
        value: {
          ar: '',
          en: ''
        },
        multiline: false,
        validators: [Validators.required, Validators.minLength(3)],
        class: 'col-md-6'
      },
      {
        type: 'text-localized',
        name: 'headsectiontitle2',
        label: this.translate.instant('Title2'),
        required: true,
        value: {
          ar: '',
          en: ''
        },
        multiline: false,
        validators: [Validators.required, Validators.minLength(3)],
        class: 'col-md-6'
      },
      {
        type: 'text-localized',
        name: 'body',
        label: this.translate.instant('body'),
        required: true,
        value: {
          ar: '',
          en: ''
        },
        multiline: true,
        validators: [Validators.required, Validators.minLength(3)],
        class: 'col-md-6'
      },
       {
        type: 'array',
        name: 'projects',
        label: this.translate.instant('projects'),
        required: true,
        value: '',
        shape: 'list',

        children: [
          {
            type: 'text',
            name: 'projectName_en',
            label: this.translate.instant('Project Name in English'),
            required: true,
            value: '',
            multiline: false,
            validators: [Validators.required, Validators.minLength(3)],
            class: 'col-md-12'
          },
          {
            type: 'text',
            name: 'projectName_ar',
            label: this.translate.instant('Project Name in Arabic'),
            required: true,
            value: '',
            multiline: false,
            validators: [Validators.required, Validators.minLength(3)],
            class: 'col-md-12'
          },
          {
            type: 'text',
            name: 'projectType_en',
            label: this.translate.instant('Project Type in English'),
            required: true,
            value: '',
            multiline: false,
            validators: [Validators.required, Validators.minLength(3)],
            class: 'col-md-12'
          },
          {
            type: 'text',
            name: 'projectType_ar',
            label: this.translate.instant('Project Type in Arabic'),
            required: true,
            value: '',
            multiline: false,
            validators: [Validators.required, Validators.minLength(3)],
            class: 'col-md-12'
          },
          {
            type: 'file',
            name: 'imageUrl',
            label: this.translate.instant('Image'),
            required: true,
            value: '',
            validators: [Validators.required],
            class: 'col-md-12'
          },
        ],

      },
    ],
  };
  forms: FormGroup = new FormGroup({});
  recieveForm(event) {
    this.forms = event;
  }
  saved: boolean = false;
  save(event) {
    this.saved = event;
  }
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
