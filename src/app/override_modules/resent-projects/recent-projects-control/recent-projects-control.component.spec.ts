import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentProjectsControlComponent } from './recent-projects-control.component';

describe('RecentProjectsControlComponent', () => {
  let component: RecentProjectsControlComponent;
  let fixture: ComponentFixture<RecentProjectsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentProjectsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentProjectsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
