import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRecentProjectsComponent } from './all-recent-projects.component';

describe('AllRecentProjectsComponent', () => {
  let component: AllRecentProjectsComponent;
  let fixture: ComponentFixture<AllRecentProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllRecentProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllRecentProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
