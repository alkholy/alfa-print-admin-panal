import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResentProjectsComponent } from './resent-projects.component';
import { AllRecentProjectsComponent } from './all-recent-projects/all-recent-projects.component';
import { RecentProjectsControlComponent } from './recent-projects-control/recent-projects-control.component';

const routes: Routes = [
  { path: '', component: ResentProjectsComponent, children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllRecentProjectsComponent },
    {path: 'control', component: RecentProjectsControlComponent},
    {path: 'edit/:entityId', component: RecentProjectsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResentProjectsRoutingModule { }
