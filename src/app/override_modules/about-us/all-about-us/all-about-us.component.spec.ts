import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllAboutUsComponent } from './all-about-us.component';

describe('AllAboutUsComponent', () => {
  let component: AllAboutUsComponent;
  let fixture: ComponentFixture<AllAboutUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllAboutUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
