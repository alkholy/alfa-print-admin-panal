import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-about-us',
  templateUrl: './all-about-us.component.html',
  styleUrls: ['./all-about-us.component.scss']
})
export class AllAboutUsComponent implements OnInit {

    entityData: entityData = {
      name: 'aboutUs',
      // path:'/pages/Brand',
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead1'),
            field: this.override.isRTL() ? 'headerSection.ar' : 'headerSection.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead2'),
            field: this.override.isRTL() ? 'titlepage.ar' : 'titlepage.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('how Works Title'),
            field: this.override.isRTL() ? 'howWorksTitle.ar' : 'howWorksTitle.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('how Works Description'),
            field: this.override.isRTL() ? 'howWorksDescription.ar' : 'howWorksDescription.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Promise Title'),
            field: this.override.isRTL() ? 'PromiseTitle.ar' : 'PromiseTitle.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Promise Description'),
            field: this.override.isRTL() ? 'PromiseDescription.ar' : 'PromiseDescription.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('About Title'),
            field: this.override.isRTL() ? 'aboutTitle.ar' : 'aboutTitle.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('About Description'),
            field: this.override.isRTL() ? 'aboutDescription.ar' : 'aboutDescription.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: true,
      view: false,
      add: true,
      firestore: true,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,
) { }

  ngOnInit(): void {
  }

}
