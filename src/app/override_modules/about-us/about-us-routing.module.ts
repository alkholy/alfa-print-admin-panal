import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutUsComponent } from './about-us.component';
import { AllAboutUsComponent } from './all-about-us/all-about-us.component';
import { AboutUsControlComponent } from './about-us-control/about-us-control.component';

const routes: Routes = [
  { path: '', component: AboutUsComponent, children: [
    {path: 'all', component: AllAboutUsComponent },
    {path: 'control', component: AboutUsControlComponent },
    {path: 'edit/:entityId', component: AboutUsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsRoutingModule { }
