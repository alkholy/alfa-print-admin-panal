import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutUsControlComponent } from './about-us-control.component';

describe('AboutUsControlComponent', () => {
  let component: AboutUsControlComponent;
  let fixture: ComponentFixture<AboutUsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutUsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutUsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
