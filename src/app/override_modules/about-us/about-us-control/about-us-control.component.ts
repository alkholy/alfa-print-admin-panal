import { Component, OnInit } from '@angular/core';
import { OverrideService } from 'src/app/shared/services/override.service';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-about-us-control',
  templateUrl: './about-us-control.component.html',
  styleUrls: ['./about-us-control.component.scss']
})
export class AboutUsControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'aboutUs',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'headerSection',
          label: this.translate.instant('Texthead1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'titlepage',
          label: this.translate.instant('Texthead2'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'file',
          name: 'howWorksImage',
          label: this.translate.instant('how Works Image'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        },
        {
          type: 'text-localized',
          name: 'howWorksTitle',
          label: this.translate.instant('how Works Title'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'howWorksDescription',
          label: this.translate.instant('how Works Description'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'file',
          name: 'promiseImage',
          label: this.translate.instant('Promise Image'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        },
        {
          type: 'text-localized',
          name: 'PromiseTitle',
          label: this.translate.instant('Promise Title'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'PromiseDescription',
          label: this.translate.instant('Promise Description'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'file',
          name: 'aboutImage',
          label: this.translate.instant('About Image'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        },
        {
          type: 'text-localized',
          name: 'aboutTitle',
          label: this.translate.instant('About Title'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'aboutDescription',
          label: this.translate.instant('About Description'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },

      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private override: OverrideService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
