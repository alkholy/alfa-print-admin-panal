import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutUsRoutingModule } from './about-us-routing.module';
import { AboutUsComponent } from './about-us.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AboutUsControlComponent } from './about-us-control/about-us-control.component';
import { AllAboutUsComponent } from './all-about-us/all-about-us.component';


@NgModule({
  declarations: [AboutUsComponent, AboutUsControlComponent, AllAboutUsComponent],
  imports: [
    CommonModule,
    AboutUsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class AboutUsModule { }
