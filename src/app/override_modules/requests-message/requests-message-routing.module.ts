import { AllRequestsMessageComponent } from './all-requests-message/all-requests-message.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestsMessageComponent } from './requests-message.component';

const routes: Routes = [
  { path: '', component: RequestsMessageComponent, children: [
    {path: 'all', component: AllRequestsMessageComponent },

  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestsMessageRoutingModule { }
