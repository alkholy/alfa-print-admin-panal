import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-requests-message',
  templateUrl: './all-requests-message.component.html',
  styleUrls: ['./all-requests-message.component.scss']
})
export class AllRequestsMessageComponent implements OnInit {

    entityData: entityData = {
      name: 'messageRequests',
      firestore: true,
      // path:'/pages/Brand',
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('name'),
            field: this.override.isRTL() ? 'name' : 'name',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Email'),
            field: this.override.isRTL() ? 'email' : 'email',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Phone Numpers'),
            field: this.override.isRTL() ? 'phone' : 'phone',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('message'),
            field: this.override.isRTL() ? 'message' : 'message',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: false,
      view: false,
      add: false,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,
    ) { }


  ngOnInit(): void {
  }

}
