import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRequestsMessageComponent } from './all-requests-message.component';

describe('AllRequestsMessageComponent', () => {
  let component: AllRequestsMessageComponent;
  let fixture: ComponentFixture<AllRequestsMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllRequestsMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllRequestsMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
