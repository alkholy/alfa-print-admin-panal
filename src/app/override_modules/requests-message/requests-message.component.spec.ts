import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsMessageComponent } from './requests-message.component';

describe('RequestsMessageComponent', () => {
  let component: RequestsMessageComponent;
  let fixture: ComponentFixture<RequestsMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
