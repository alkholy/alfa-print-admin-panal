import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestsMessageRoutingModule } from './requests-message-routing.module';
import { RequestsMessageComponent } from './requests-message.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllRequestsMessageComponent } from './all-requests-message/all-requests-message.component';


@NgModule({
  declarations: [RequestsMessageComponent, AllRequestsMessageComponent],
  imports: [
    CommonModule,
    RequestsMessageRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class RequestsMessageModule { }
