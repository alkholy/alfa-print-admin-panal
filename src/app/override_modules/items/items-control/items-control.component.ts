import { Component, OnInit } from '@angular/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-items-control',
  templateUrl: './items-control.component.html',
  styleUrls: ['./items-control.component.scss']
})
export class ItemsControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'items',
      firestore: true,
      fields: [
         {
          type: 'array',
          name: 'items',
          label: this.translate.instant('Items'),
          required: true,
          value: '',
          // shape: 'list',
          children: [
            {
              type: 'text',
              name: 'id',
              label: this.translate.instant('id'),
              required: true,
              value: uuid(),
              multiline: false,
              // disabled: true,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-2 d-none'
            },
            {
              type: 'text',
              name: 'itemName_en',
              label: this.translate.instant('Item Name In English'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'itemName_ar',
              label: this.translate.instant('Item Name In Arabic'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },

            {
              type: 'file',
              name: 'image',
              label: this.translate.instant('Image'),
              required: true,
              value: '',
              validators: [Validators.required],
              class: 'col-md-12'
            },
            {
              type: 'number',
              name: 'priceOfM',
              label: this.translate.instant('Price/M'),
              required: true,
              value: '',
              validators: [Validators.required],
              class: 'col-md-6'
            }
          ],

        },
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService,

  ) { }

  ngOnInit(): void {
  }

}
