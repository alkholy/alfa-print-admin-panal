import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-items',
  templateUrl: './all-items.component.html',
  styleUrls: ['./all-items.component.scss']
})
export class AllItemsComponent implements OnInit {

  entityData: entityData = {
    name: 'items',
   firestore: true,
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        // {
        //   headerName: this.translate.instant('Item Name in English'),
        //   field: 'itemName_en',
        //   filter: true,
        //   checkboxSelection: false,
        //   sortable: true,
        //   editable: false,
        // },
        // {
        //   headerName: this.translate.instant('Item Name in Arabic'),
        //   field: 'itemName_ar',
        //   filter: true,
        //   checkboxSelection: false,
        //   sortable: true,
        //   editable: false,
        // },
        // {
        //   headerName: this.translate.instant('Item Price Of 1Meter'),
        //   field: 'priceOfM',
        //   filter: true,
        //   checkboxSelection: false,
        //   sortable: true,
        //   editable: false,
        // },

      ],
    },
    edit: true,
    view: false,
    add: true,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,
  ) { }

  ngOnInit(): void {
  }

}
