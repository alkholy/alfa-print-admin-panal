import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { AllItemsComponent } from './all-items/all-items.component';
import { ItemsControlComponent } from './items-control/items-control.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ItemsComponent, AllItemsComponent, ItemsControlComponent],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class ItemsModule { }
