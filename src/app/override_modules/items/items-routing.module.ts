import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItemsComponent } from './items.component';
import { AllItemsComponent } from './all-items/all-items.component';
import { ItemsControlComponent } from './items-control/items-control.component';

const routes: Routes = [
  { path: '', component: ItemsComponent, children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllItemsComponent },
    {path: 'control', component: ItemsControlComponent },
    {path: 'edit/:entityId', component: ItemsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }
