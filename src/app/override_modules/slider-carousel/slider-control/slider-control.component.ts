import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-slider-control',
  templateUrl: './slider-control.component.html',
  styleUrls: ['./slider-control.component.scss']
})
export class SliderControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'slider',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'title1',
          label: this.translate.instant('Title 1'),
          required: true,
          value: {ar: '', en: ''},
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'title2',
          label: this.translate.instant('Title 2'),
          required: true,
          value: {ar: '', en: ''},
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'file',
          name: 'image',
          label: this.translate.instant('Image'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        }
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }


}
