import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SliderCarouselComponent } from './slider-carousel.component';
import { AllSliderComponent } from './all-slider/all-slider.component';
import { SliderControlComponent } from './slider-control/slider-control.component';

const routes: Routes = [
  { path: '', component: SliderCarouselComponent, children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllSliderComponent },
    {path: 'control', component: SliderControlComponent },
    {path: 'edit/:entityId', component: SliderControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SliderCarouselRoutingModule { }
