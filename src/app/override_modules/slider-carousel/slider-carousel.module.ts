import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderCarouselRoutingModule } from './slider-carousel-routing.module';
import { SliderCarouselComponent } from './slider-carousel.component';
import { AllSliderComponent } from './all-slider/all-slider.component';
import { SliderControlComponent } from './slider-control/slider-control.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [SliderCarouselComponent, AllSliderComponent, SliderControlComponent],
  imports: [
    CommonModule,
    SliderCarouselRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class SliderCarouselModule { }
