import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-slider',
  templateUrl: './all-slider.component.html',
  styleUrls: ['./all-slider.component.scss']
})
export class AllSliderComponent implements OnInit {

    entityData: entityData = {
      name: 'slider',
      // path:'/pages/Brand',
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Title 1'),
            field: this.override.isRTL() ? 'title1.ar' : 'title1.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Title 2'),
            field: this.override.isRTL() ? 'title2.ar' : 'title2.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: true,
      view: false,
      add: true,
      firestore: true,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,
     ) { }


  ngOnInit(): void {
  }

}
