"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AllSliderComponent = void 0;
var core_1 = require("@angular/core");
var AllSliderComponent = /** @class */ (function () {
    function AllSliderComponent(translate, override) {
        this.translate = translate;
        this.override = override;
        this.entityData = {
            name: 'slider',
            // path:'/pages/Brand',
            all: {
                columnDefs: [
                    {
                        headerName: this.translate.instant('id'),
                        field: 'id',
                        checkboxSelection: true,
                        filter: true,
                        sortable: true,
                        editable: false
                    },
                    {
                        headerName: this.translate.instant('Title 1'),
                        field: this.override.isRTL() ? 'title1.ar' : 'title1.en',
                        filter: true,
                        checkboxSelection: false,
                        sortable: true,
                        editable: false
                    },
                    {
                        headerName: this.translate.instant('Title 2'),
                        field: this.override.isRTL() ? 'title2.ar' : 'title2.en',
                        filter: true,
                        checkboxSelection: false,
                        sortable: true,
                        editable: false
                    },
                ]
            },
            edit: true,
            view: false,
            add: true,
            firestore: true,
            upload: false
        };
    }
    AllSliderComponent.prototype.ngOnInit = function () {
    };
    AllSliderComponent = __decorate([
        core_1.Component({
            selector: 'app-all-slider',
            templateUrl: './all-slider.component.html',
            styleUrls: ['./all-slider.component.scss']
        })
    ], AllSliderComponent);
    return AllSliderComponent;
}());
exports.AllSliderComponent = AllSliderComponent;
