import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './test-routing.module';
import { TestComponent } from './test.component';
import { ControlComponent } from './control/control.component';
import { AllComponent } from './all/all.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [TestComponent, ControlComponent, AllComponent],
  imports: [
    CommonModule,
    TestRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class TestModule { }
