import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestComponent } from './test.component';
import { AllComponent } from './all/all.component';
import { ControlComponent } from './control/control.component';

const routes: Routes = [
  { path: '', component: TestComponent , children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllComponent },
    {path: 'control', component: ControlComponent },

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule { }
