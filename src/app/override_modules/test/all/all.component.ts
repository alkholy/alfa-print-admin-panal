import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';
import { EntityService } from 'src/app/shared/services/entity.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss']
})
export class AllComponent implements OnInit {

 
  entityData: entityData = {
    name: "article",
    label: "Article",
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant("ID"),
          field: "id",
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Type'),
          field: "userId",
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Category'),
          field: "title",
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant("Title"),
          field: "completed" ,
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant("Body"),
          field: "body." + this.override.currentLang,
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false
        }

      
      ],
    },
    edit: true,
    view: false,
    add:true,
    upload: false,
    getURI:'https://jsonplaceholder.typicode.com/todos'
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,
    private entity: EntityService,
    private toastrService: NbToastrService) {}
  ngOnInit(): void {
  }

}
