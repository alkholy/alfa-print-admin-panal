import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-services',
  templateUrl: './all-services.component.html',
  styleUrls: ['./all-services.component.scss']
})
export class AllServicesComponent implements OnInit {

  entityData: entityData = {
    name: 'serveices',
    firestore: true,
    // path:'/pages/Brand',
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Title1'),
          field: this.override.isRTL() ? 'title1.ar' : 'title1.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Title2'),
          field: this.override.isRTL() ? 'title2.ar' : 'title2.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('body1'),
          field: this.override.isRTL() ? 'body1.ar' : 'body1.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('body2'),
          field: this.override.isRTL() ? 'body2.ar' : 'body2.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },

      ],
    },
    edit: true,
    view: false,
    add: true,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,

    ) { }

  ngOnInit(): void {
  }

}
