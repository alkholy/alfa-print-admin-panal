import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesControlComponent } from './services-control.component';

describe('ServicesControlComponent', () => {
  let component: ServicesControlComponent;
  let fixture: ComponentFixture<ServicesControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
