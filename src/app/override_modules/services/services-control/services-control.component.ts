import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-services-control',
  templateUrl: './services-control.component.html',
  styleUrls: ['./services-control.component.scss']
})
export class ServicesControlComponent implements OnInit {


    form: genericForm = {
      entityName: 'serveices',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'title1',
          label: this.translate.instant('Title1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'title2',
          label: this.translate.instant('Title2'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'body1',
          label: this.translate.instant('body1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'body2',
          label: this.translate.instant('body2'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'number',
          name: 'phoneNumpers',
          label: this.translate.instant('Phone Numpers'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-6'
        },
         {
          type: 'array',
          name: 'blocks',
          label: this.translate.instant('blocks'),
          required: true,
          value: '',
          children: [
              {
                type: 'text',
                name: 'title_en',
                label: this.translate.instant('Title In English'),
                required: true,
                value: '',
                multiline: false,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-12'
              },
              {
                type: 'text',
                name: 'title_ar',
                label: this.translate.instant('Title In Arabic'),
                required: true,
                value: '',
                multiline: false,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-12'
              },
              {
                type: 'dropdown',
                name: 'shap',
                label: this.translate.instant('Shap'),
                multiple: false,
                value: '',
                validators: [Validators.required],
                details: this.translate.instant('Select Shap'),
                class: 'col-md-12',
                options: [
                  {
                    value: 'fa-tshirt',
                    label: this.translate.instant('Tshirt'),
                  },
                  {
                    value: 'fa-print',
                    label: this.translate.instant('Print'),
                  },
                  {
                    value: 'fa-money-check',
                    label: this.translate.instant('Money Check'),
                  },
                  {
                    value: 'fa-shapes',
                    label: this.translate.instant('Shapes'),
                  },

                ],
                required: true,
                disabled: false,
              },
          ],

        },
      ],
    };

    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
