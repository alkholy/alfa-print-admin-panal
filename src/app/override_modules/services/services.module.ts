import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from './services.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllServicesComponent } from './all-services/all-services.component';
import { ServicesControlComponent } from './services-control/services-control.component';


@NgModule({
  declarations: [ServicesComponent, AllServicesComponent, ServicesControlComponent],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class ServicesModule { }
