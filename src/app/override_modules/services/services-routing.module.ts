import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServicesComponent } from './services.component';
import { AllServicesComponent } from './all-services/all-services.component';
import { ServicesControlComponent } from './services-control/services-control.component';

const routes: Routes = [
  { path: '', component: ServicesComponent, children: [
    {path: 'all', component: AllServicesComponent },
    {path: 'control', component: ServicesControlComponent },
    {path: 'edit/:entityId', component: ServicesControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
