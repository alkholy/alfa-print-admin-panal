import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-benefits',
  templateUrl: './all-benefits.component.html',
  styleUrls: ['./all-benefits.component.scss']
})
export class AllBenefitsComponent implements OnInit {
  entityData: entityData = {
    name: 'benefits',
    // path:'/pages/Brand',
    firestore: true,
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Title1'),
          field: this.override.isRTL() ? 'title1.ar' : 'title1.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Title2'),
          field: this.override.isRTL() ? 'title2.ar' : 'title2.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },


      ],
    },
    edit: true,
    view: false,
    add: true,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,

  ) { }
  ngOnInit(): void {
  }

}
