import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BenefitsComponent } from './benefits.component';
import { AllBenefitsComponent } from './all-benefits/all-benefits.component';
import { BenefitsControlComponent } from './benefits-control/benefits-control.component';

const routes: Routes = [
  { path: '', component: BenefitsComponent, children: [
    {path: 'all', component: AllBenefitsComponent },
    {path: 'control', component: BenefitsControlComponent },
    {path: 'edit/:entityId', component: BenefitsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BenefitsRoutingModule { }
