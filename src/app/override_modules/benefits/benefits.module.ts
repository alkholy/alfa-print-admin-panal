import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenefitsRoutingModule } from './benefits-routing.module';
import { BenefitsComponent } from './benefits.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllBenefitsComponent } from './all-benefits/all-benefits.component';
import { BenefitsControlComponent } from './benefits-control/benefits-control.component';


@NgModule({
  declarations: [BenefitsComponent, AllBenefitsComponent, BenefitsControlComponent],
  imports: [
    CommonModule,
    BenefitsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class BenefitsModule { }
