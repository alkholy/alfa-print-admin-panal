import { Component, OnInit } from '@angular/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { Validators, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-benefits-control',
  templateUrl: './benefits-control.component.html',
  styleUrls: ['./benefits-control.component.scss']
})
export class BenefitsControlComponent implements OnInit {

  form: genericForm = {
    entityName: 'benefits',
    firestore: true,
    fields: [
      {
        type: 'text-localized',
        name: 'title1',
        label: this.translate.instant('Title1'),
        required: true,
        value: {
          ar: '',
          en: ''
        },
        multiline: false,
        validators: [Validators.required, Validators.minLength(3)],
        class: 'col-md-6'
      },
      {
        type: 'text-localized',
        name: 'title2',
        label: this.translate.instant('Title2'),
        required: true,
        value: {
          ar: '',
          en: ''
        },
        multiline: false,
        validators: [Validators.required, Validators.minLength(3)],
        class: 'col-md-6'
      },
       {
        type: 'array',
        name: 'benefits',
        label: this.translate.instant('Benefits'),
        required: true,
        value: '',
        children: [
          {
            type: 'text',
            name: 'benefit_en',
            label: this.translate.instant('Benefit in English'),
            required: true,
            value: '',
            multiline: false,
            validators: [Validators.required, Validators.minLength(3)],
            class: 'col-md-12'
          },
          {
            type: 'text',
            name: 'benefit_ar',
            label: this.translate.instant('Benefit in Arabic'),
            required: true,
            value: '',
            multiline: false,
            validators: [Validators.required, Validators.minLength(3)],
            class: 'col-md-12'
          }
        ],

      },
    ],
  };

  forms: FormGroup = new FormGroup({});
  recieveForm(event) {
    this.forms = event;
  }
  saved: boolean = false;
  save(event) {
    this.saved = event;
  }
constructor(
  private translate: TranslateService
) { }

  ngOnInit(): void {
  }

}
