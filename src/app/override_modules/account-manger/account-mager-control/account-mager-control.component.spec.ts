import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountMagerControlComponent } from './account-mager-control.component';

describe('AccountMagerControlComponent', () => {
  let component: AccountMagerControlComponent;
  let fixture: ComponentFixture<AccountMagerControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountMagerControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountMagerControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
