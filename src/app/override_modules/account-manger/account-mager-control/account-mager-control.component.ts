import { Component, OnInit } from '@angular/core';
import { OverrideService } from 'src/app/shared/services/override.service';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-mager-control',
  templateUrl: './account-mager-control.component.html',
  styleUrls: ['./account-mager-control.component.scss']
})
export class AccountMagerControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'accountManger',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'accountName',
          label: this.translate.instant('Account Name'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text',
          name: 'accountNumber',
          label: this.translate.instant('Account Number'),
          required: true,
          value: '',
          validators: [Validators.required, Validators.pattern(this.override.phoneNumberRegExp)],
          class: 'col-md-6'
        },
        {
          type: 'file',
          name: 'image',
          label: this.translate.instant('Logo'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        }
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private override: OverrideService,
    private translate: TranslateService,

  ) { }

  ngOnInit(): void {
  }

}
