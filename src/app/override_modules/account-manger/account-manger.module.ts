import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountMangerRoutingModule } from './account-manger-routing.module';
import { AccountMangerComponent } from './account-manger.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AccountMagerControlComponent } from './account-mager-control/account-mager-control.component';
import { AllAccountManagerComponent } from './all-account-manager/all-account-manager.component';


@NgModule({
  declarations: [AccountMangerComponent, AccountMagerControlComponent, AllAccountManagerComponent],
  imports: [
    CommonModule,
    AccountMangerRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class AccountMangerModule { }
