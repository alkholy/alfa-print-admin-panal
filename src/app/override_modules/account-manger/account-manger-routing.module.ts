import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountMangerComponent } from './account-manger.component';
import { AllAccountManagerComponent } from './all-account-manager/all-account-manager.component';
import { AccountMagerControlComponent } from './account-mager-control/account-mager-control.component';

const routes: Routes = [
  { path: '', component: AccountMangerComponent, children: [
    {path: 'all', component: AllAccountManagerComponent },
    {path: 'control', component: AccountMagerControlComponent },
    {path: 'edit/:entityId', component: AccountMagerControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountMangerRoutingModule { }
