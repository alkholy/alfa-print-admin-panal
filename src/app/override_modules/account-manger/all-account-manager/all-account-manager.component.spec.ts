import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllAccountManagerComponent } from './all-account-manager.component';

describe('AllAccountManagerComponent', () => {
  let component: AllAccountManagerComponent;
  let fixture: ComponentFixture<AllAccountManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllAccountManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllAccountManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
