import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-account-manager',
  templateUrl: './all-account-manager.component.html',
  styleUrls: ['./all-account-manager.component.scss']
})
export class AllAccountManagerComponent implements OnInit {

  entityData: entityData = {
    name: 'accountManger',
    firestore: true,
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Account Name'),
          field: this.override.isRTL() ? 'accountName.ar' : 'accountName.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Account Number'),
          field: 'accountNumber',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },

      ],
    },
    edit: true,
    view: false,
    add: true,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,

    ) { }

  ngOnInit(): void {
  }

}
