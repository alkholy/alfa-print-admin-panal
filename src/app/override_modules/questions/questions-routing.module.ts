import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuestionsComponent } from './questions.component';
import { QuestionsControlComponent } from './questions-control/questions-control.component';
import { AllQuestionsComponent } from './all-questions/all-questions.component';

const routes: Routes = [
  { path: '', component: QuestionsComponent, children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllQuestionsComponent },
    {path: 'control', component: QuestionsControlComponent },
    {path: 'edit/:entityId', component: QuestionsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionsRoutingModule { }
