import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsControlComponent } from './questions-control.component';

describe('QuestionsControlComponent', () => {
  let component: QuestionsControlComponent;
  let fixture: ComponentFixture<QuestionsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
