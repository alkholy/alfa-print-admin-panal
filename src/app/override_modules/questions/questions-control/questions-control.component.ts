import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-questions-control',
  templateUrl: './questions-control.component.html',
  styleUrls: ['./questions-control.component.scss']
})
export class QuestionsControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'questions',
      firestore: true,
      fields: [
        {
          type: 'file',
          name: 'image',
          label: this.translate.instant('Image'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        },
        {
          type: 'text-localized',
          name: 'texthead',
          label: this.translate.instant('Texthead1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'title',
          label: this.translate.instant('Title'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'description',
          label: this.translate.instant('Description'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-12'
        },
         {
          type: 'array',
          name: 'accordion',
          label: this.translate.instant('accordion'),
          required: true,
          value: '',
          children: [
            {
              type: 'text',
              name: 'tite_en',
              label: this.translate.instant('Title In English'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'tite_ar',
              label: this.translate.instant('Title In Arabic'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'description_en',
              label: this.translate.instant('Description In English'),
              required: true,
              value: '',
              multiline: true,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'description_ar',
              label: this.translate.instant('Description In Arabic'),
              required: true,
              value: '',
              multiline: true,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
          ],

        },
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService

  ) { }

  ngOnInit(): void {
  }

}
