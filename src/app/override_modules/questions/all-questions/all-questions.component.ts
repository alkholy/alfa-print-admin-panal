import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-questions',
  templateUrl: './all-questions.component.html',
  styleUrls: ['./all-questions.component.scss']
})
export class AllQuestionsComponent implements OnInit {

    entityData: entityData = {
      name: 'questions',
      // path:'/pages/Brand',
      firestore: true,
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead1'),
            field: this.override.isRTL() ? 'texthead.ar' : 'texthead.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Title'),
            field: this.override.isRTL() ? 'title.ar' : 'title.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Description'),
            field: this.override.isRTL() ? 'description.ar' : 'description.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: true,
      view: false,
      add: true,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,

      ) { }

  ngOnInit(): void {
  }

}
