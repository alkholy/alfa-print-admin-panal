import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionsRoutingModule } from './questions-routing.module';
import { QuestionsComponent } from './questions.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllQuestionsComponent } from './all-questions/all-questions.component';
import { QuestionsControlComponent } from './questions-control/questions-control.component';


@NgModule({
  declarations: [QuestionsComponent, AllQuestionsComponent, QuestionsControlComponent],
  imports: [
    CommonModule,
    QuestionsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class QuestionsModule { }
