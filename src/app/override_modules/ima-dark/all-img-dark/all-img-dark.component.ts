import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-img-dark',
  templateUrl: './all-img-dark.component.html',
  styleUrls: ['./all-img-dark.component.scss']
})
export class AllImgDarkComponent implements OnInit {

    entityData: entityData = {
      name: 'imgDark',
      firestore: true,
      // path:'/pages/Brand',
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Head Text'),
            field: this.override.isRTL() ? 'headtext.ar' : 'headtext.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead1'),
            field: this.override.isRTL() ? 'titleCard1.ar' : 'titleCard1.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Texthead1'),
            field: this.override.isRTL() ? 'titleCard2.ar' : 'titleCard2.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('body1'),
            field: this.override.isRTL() ? 'bodyCard2.ar' : 'bodyCard2.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: true,
      view: false,
      add: true,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,

  ) { }


  ngOnInit(): void {
  }

}
