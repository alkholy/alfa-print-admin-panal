import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllImgDarkComponent } from './all-img-dark.component';

describe('AllImgDarkComponent', () => {
  let component: AllImgDarkComponent;
  let fixture: ComponentFixture<AllImgDarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllImgDarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllImgDarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
