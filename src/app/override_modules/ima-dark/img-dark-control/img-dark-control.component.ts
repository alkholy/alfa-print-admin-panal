import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-img-dark-control',
  templateUrl: './img-dark-control.component.html',
  styleUrls: ['./img-dark-control.component.scss']
})
export class ImgDarkControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'imgDark',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'headtext',
          label: this.translate.instant('Head Text'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'titleCard1',
          label: this.translate.instant('Texthead1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'bodyCard2',
          label: this.translate.instant('body1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'titleCard2',
          label: this.translate.instant('Texthead1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
