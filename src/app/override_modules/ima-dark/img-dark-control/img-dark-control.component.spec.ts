import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgDarkControlComponent } from './img-dark-control.component';

describe('ImgDarkControlComponent', () => {
  let component: ImgDarkControlComponent;
  let fixture: ComponentFixture<ImgDarkControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgDarkControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgDarkControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
