import { AllImgDarkComponent } from './all-img-dark/all-img-dark.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImaDarkComponent } from './ima-dark.component';
import { ImgDarkControlComponent } from './img-dark-control/img-dark-control.component';

const routes: Routes = [
  { path: '', component: ImaDarkComponent, children: [
    {path: '', redirectTo: 'all', pathMatch: 'full'},
    {path: 'all', component: AllImgDarkComponent },
    {path: 'control', component: ImgDarkControlComponent },
    {path: 'edit/:entityId', component: ImgDarkControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImaDarkRoutingModule { }
