import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImaDarkRoutingModule } from './ima-dark-routing.module';
import { ImaDarkComponent } from './ima-dark.component';
import { ImgDarkControlComponent } from './img-dark-control/img-dark-control.component';
import { AllImgDarkComponent } from './all-img-dark/all-img-dark.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ImaDarkComponent, ImgDarkControlComponent, AllImgDarkComponent],
  imports: [
    CommonModule,
    ImaDarkRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class ImaDarkModule { }
