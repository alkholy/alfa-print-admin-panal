import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImaDarkComponent } from './ima-dark.component';

describe('ImaDarkComponent', () => {
  let component: ImaDarkComponent;
  let fixture: ComponentFixture<ImaDarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImaDarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImaDarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
