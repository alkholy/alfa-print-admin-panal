import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardsComponent } from './cards.component';
import { AllCardsComponent } from './all-cards/all-cards.component';
import { CardsControlComponent } from './cards-control/cards-control.component';

const routes: Routes = [
  { path: '', component: CardsComponent, children: [
    {path: 'all', component: AllCardsComponent },
    {path: 'control', component: CardsControlComponent },
    {path: 'edit/:entityId', component: CardsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsRoutingModule { }
