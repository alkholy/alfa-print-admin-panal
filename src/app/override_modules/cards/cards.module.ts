import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsRoutingModule } from './cards-routing.module';
import { CardsComponent } from './cards.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllCardsComponent } from './all-cards/all-cards.component';
import { CardsControlComponent } from './cards-control/cards-control.component';


@NgModule({
  declarations: [CardsComponent, AllCardsComponent, CardsControlComponent],
  imports: [
    CommonModule,
    CardsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class CardsModule { }
