import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cards-control',
  templateUrl: './cards-control.component.html',
  styleUrls: ['./cards-control.component.scss']
})
export class CardsControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'cards',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'titleCard1',
          label: this.translate.instant('Texthead1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'titleCard2',
          label: this.translate.instant('Texthead2'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text',
          name: 'bleed',
          label: this.translate.instant('Bleed'),
          required: true,
          value: '',
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-4'
        },
        {
          type: 'text',
          name: 'trim',
          label: this.translate.instant('Trim'),
          required: true,
          value: '',
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-4'
        },
        {
          type: 'text',
          name: 'safe',
          label: this.translate.instant('Safe'),
          required: true,
          value: '',
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-4'
        },
        {
          type: 'file',
          name: 'imageUrl',
          label: this.translate.instant('Image'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        },
         {
          type: 'array',
          name: 'designTypes',
          label: this.translate.instant('Design Types'),
          required: true,
          value: '',
          children: [
            {
              type: 'text',
              name: 'name_en',
              label: this.translate.instant('Name in English'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-4'
            },
            {
              type: 'text',
              name: 'name_ar',
              label: this.translate.instant('Name in Arabic'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-4'
            },
            {
              type: 'text',
              name: 'extintion',
              label: this.translate.instant('extension'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-4'
            },

          ],

        },

      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
