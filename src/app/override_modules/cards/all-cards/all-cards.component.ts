import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-cards',
  templateUrl: './all-cards.component.html',
  styleUrls: ['./all-cards.component.scss']
})
export class AllCardsComponent implements OnInit {

  entityData: entityData = {
    name: 'cards',
    firestore: true,
    // path:'/pages/Brand',
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Title Card1'),
          field: this.override.isRTL() ? 'titleCard1.ar' : 'titleCard1.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Title Card2'),
          field: this.override.isRTL() ? 'titleCard2.ar' : 'titleCard2.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Bleed'),
          field: this.override.isRTL() ? 'bleed' : 'bleed',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Trim'),
          field: this.override.isRTL() ? 'trim' : 'trim',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Safe'),
          field: this.override.isRTL() ? 'safe' : 'safe',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },

      ],
    },
    edit: true,
    view: false,
    add: true,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,

    ) { }

  ngOnInit(): void {
  }

}
