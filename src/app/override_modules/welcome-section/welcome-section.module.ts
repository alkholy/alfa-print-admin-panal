import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomeSectionRoutingModule } from './welcome-section-routing.module';
import { WelcomeSectionComponent } from './welcome-section.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllWelcomeComponent } from './all-welcome/all-welcome.component';
import { WelcomeControlComponent } from './welcome-control/welcome-control.component';


@NgModule({
  declarations: [WelcomeSectionComponent, AllWelcomeComponent, WelcomeControlComponent],
  imports: [
    CommonModule,
    WelcomeSectionRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class WelcomeSectionModule { }
