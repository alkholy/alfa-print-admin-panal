import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';
import { EntityService } from 'src/app/shared/services/entity.service';

@Component({
  selector: 'app-all-welcome',
  templateUrl: './all-welcome.component.html',
  styleUrls: ['./all-welcome.component.scss']
})
export class AllWelcomeComponent implements OnInit {

  entityData: entityData = {
    name: 'welcome',
    // path:'/pages/Brand',
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Texthead1'),
          field: this.override.isRTL() ? 'texthead1.ar' : 'texthead1.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Texthead2'),
          field: this.override.isRTL() ? 'texthead1.ar' : 'texthead2.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('body1'),
          field: this.override.isRTL() ? 'body1.ar' : 'body1.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('body2'),
          field: this.override.isRTL() ? 'body2.ar' : 'body2.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },

      ],
    },
    edit: true,
    view: false,
    firestore: true,
    add: true,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,
    private entity: EntityService,
    ) { }
  ngOnInit(): void {
  }

}
