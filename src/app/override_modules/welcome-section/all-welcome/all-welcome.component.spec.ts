import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllWelcomeComponent } from './all-welcome.component';

describe('AllWelcomeComponent', () => {
  let component: AllWelcomeComponent;
  let fixture: ComponentFixture<AllWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
