import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-welcome-control',
  templateUrl: './welcome-control.component.html',
  styleUrls: ['./welcome-control.component.scss']
})
export class WelcomeControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'welcome',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'texthead1',
          label: this.translate.instant('Texthead1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'texthead2',
          label: this.translate.instant('Texthead2'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'body1',
          label: this.translate.instant('body1'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'body2',
          label: this.translate.instant('body2'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: true,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'file',
          name: 'image',
          label: this.translate.instant('Image'),
          required: true,
          value: '',
          validators: [Validators.required],
          class: 'col-md-12'
        }
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
