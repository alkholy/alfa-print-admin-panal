import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeSectionComponent } from './welcome-section.component';
import { AllWelcomeComponent } from './all-welcome/all-welcome.component';
import { WelcomeControlComponent } from './welcome-control/welcome-control.component';

const routes: Routes = [
  { path: '', component: WelcomeSectionComponent, children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllWelcomeComponent },
    {path: 'control', component: WelcomeControlComponent },
    {path: 'edit/:entityId', component: WelcomeControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeSectionRoutingModule { }
