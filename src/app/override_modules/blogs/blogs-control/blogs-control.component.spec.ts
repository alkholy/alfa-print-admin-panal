import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogsControlComponent } from './blogs-control.component';

describe('BlogsControlComponent', () => {
  let component: BlogsControlComponent;
  let fixture: ComponentFixture<BlogsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
