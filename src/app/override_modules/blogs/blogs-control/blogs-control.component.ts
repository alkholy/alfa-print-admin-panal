import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-blogs-control',
  templateUrl: './blogs-control.component.html',
  styleUrls: ['./blogs-control.component.scss']
})
export class BlogsControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'blogs',
      firestore: true,
      fields: [
        {
          type: 'text-localized',
          name: 'titleSection',
          label: this.translate.instant('Title'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
        {
          type: 'text-localized',
          name: 'blogtitle',
          label: this.translate.instant('Blog Title'),
          required: true,
          value: {
            ar: '',
            en: ''
          },
          multiline: false,
          validators: [Validators.required, Validators.minLength(3)],
          class: 'col-md-6'
        },
         {
          type: 'array',
          name: 'blogs',
          label: this.translate.instant('Blogs'),
          required: true,
          shape: 'list',
          value: '',
          children: [
            {
              type: 'text',
              name: 'name_en',
              label: this.translate.instant('Name in English'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'name_ar',
              label: this.translate.instant('Name in Arabic'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'title_en',
              label: this.translate.instant('Title In English'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'title_ar',
              label: this.translate.instant('Title In Arabic'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'description_en',
              label: this.translate.instant('Description In English'),
              required: true,
              value: '',
              multiline: true,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'description_ar',
              label: this.translate.instant('Description In Arabic'),
              required: true,
              value: '',
              multiline: true,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'description_en2',
              label: this.translate.instant('Description 2 In English'),
              required: true,
              value: '',
              multiline: true,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'description_ar2',
              label: this.translate.instant('Description 2 In Arabic'),
              required: true,
              value: '',
              multiline: true,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'text',
              name: 'quote_en',
              label: this.translate.instant('Quote in English'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },

            {
              type: 'text',
              name: 'quote_ar',
              label: this.translate.instant('Quote in Arabic'),
              required: true,
              value: '',
              multiline: false,
              validators: [Validators.required, Validators.minLength(3)],
              class: 'col-md-6'
            },
            {
              type: 'date',
              name: 'date',
              label: this.translate.instant('Date'),
              required: false,
              readonly: true,
              value: '',
              // validators: [Validators.required],
              class: 'col-md-6',
            },
            {
              type: 'number',
              name: 'numberOfComments',
              label: this.translate.instant('numberOfComments'),
              required: true,
              value: '',
              validators: [Validators.required],
              class: 'col-md-6'
            },

            {
              type: 'file',
              name: 'imageUrl',
              label: this.translate.instant('Image'),
              required: true,
              value: '',
              validators: [Validators.required],
              class: 'col-md-6'
            },
            {
              type: 'file',
              name: 'image',
              label: this.translate.instant('Image'),
              required: true,
              value: '',
              validators: [Validators.required],
              class: 'col-md-6'
            },


          ],

        },
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService,
  ) { }

  ngOnInit(): void {
  }

}
