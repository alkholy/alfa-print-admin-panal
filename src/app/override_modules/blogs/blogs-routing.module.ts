import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogsComponent } from './blogs.component';
import { AllBlogsComponent } from './all-blogs/all-blogs.component';
import { BlogsControlComponent } from './blogs-control/blogs-control.component';

const routes: Routes = [
  { path: '', component: BlogsComponent, children:[
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllBlogsComponent },
    {path: 'control', component: BlogsControlComponent },
    {path: 'edit/:entityId', component: BlogsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogsRoutingModule { }
