import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogsRoutingModule } from './blogs-routing.module';
import { BlogsComponent } from './blogs.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { AllBlogsComponent } from './all-blogs/all-blogs.component';
import { BlogsControlComponent } from './blogs-control/blogs-control.component';


@NgModule({
  declarations: [BlogsComponent, AllBlogsComponent, BlogsControlComponent],
  imports: [
    CommonModule,
    BlogsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class BlogsModule { }
