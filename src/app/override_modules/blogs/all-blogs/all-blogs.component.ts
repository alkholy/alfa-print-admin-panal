import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-blogs',
  templateUrl: './all-blogs.component.html',
  styleUrls: ['./all-blogs.component.scss']
})
export class AllBlogsComponent implements OnInit {

    entityData: entityData = {
      name: 'blogs',
      firestore: true,
      // path:'/pages/Brand',
      all: {
        columnDefs: [
          {
            headerName: this.translate.instant('id'),
            field: 'id',
            checkboxSelection: true,
            filter: true,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Title'),
            field: this.override.isRTL() ? 'titleSection.ar' : 'titleSection.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },
          {
            headerName: this.translate.instant('Blog Title'),
            field: this.override.isRTL() ? 'blogtitle.ar' : 'blogtitle.en',
            filter: true,
            checkboxSelection: false,
            sortable: true,
            editable: false,
          },

        ],
      },
      edit: true,
      view: false,
      add: true,
      upload: false,
    };
    constructor(
      private translate: TranslateService,
      public override: OverrideService,

      ) { }


  ngOnInit(): void {
  }

}
