import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientsComponent } from './clients.component';
import { AllClientsComponent } from './all-clients/all-clients.component';
import { ClientsControlComponent } from './clients-control/clients-control.component';

const routes: Routes = [
  { path: '', component: ClientsComponent, children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllClientsComponent },
    {path: 'control', component: ClientsControlComponent },
    {path: 'edit/:entityId', component: ClientsControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
