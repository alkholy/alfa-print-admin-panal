import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-clients',
  templateUrl: './all-clients.component.html',
  styleUrls: ['./all-clients.component.scss']
})
export class AllClientsComponent implements OnInit {

  entityData: entityData = {
    name: 'clients',
    firestore: true,
    // path:'/pages/Brand',
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('name'),
          field: this.override.isRTL() ? 'name.ar' : 'name.en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },

      ],
    },
    edit: true,
    view: false,
    add: true,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,

    ) { }
  ngOnInit(): void {
  }

}
