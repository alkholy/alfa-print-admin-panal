import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-clients-control',
  templateUrl: './clients-control.component.html',
  styleUrls: ['./clients-control.component.scss']
})
export class ClientsControlComponent implements OnInit {

    form: genericForm = {
      entityName: 'clients',
      firestore: true,
      fields: [
         {
            type: 'array',
            name: 'clients',
            label: this.translate.instant('clients'),
            required: true,
            value: '',
            children: [
              {
                type: 'text',
                name: 'name_en',
                label: this.translate.instant('Name in English'),
                required: true,
                value: '',
                multiline: false,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-12'
              },
              {
                type: 'text',
                name: 'name_ar',
                label: this.translate.instant('Name in Arabic'),
                required: true,
                value: '',
                multiline: false,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-12'
              },
              {
                type: 'text',
                name: 'quote_en',
                label: this.translate.instant('Quote in English'),
                required: true,
                value: '',
                multiline: true,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-12'
              },
              {
                type: 'text',
                name: 'quote_ar',
                label: this.translate.instant('Quote in Arabic'),
                required: true,
                value: '',
                multiline: true,
                validators: [Validators.required, Validators.minLength(3)],
                class: 'col-md-12'
              },
              {
                type: 'file',
                name: 'image',
                label: this.translate.instant('Image'),
                required: true,
                value: '',
                validators: [Validators.required],
                class: 'col-md-6'
              }

            ],

          },
      ],
    };
    forms: FormGroup = new FormGroup({});
    recieveForm(event) {
      this.forms = event;
    }
    saved: boolean = false;
    save(event) {
      this.saved = event;
    }
  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
  }

}
