import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsControlComponent } from './clients-control.component';

describe('ClientsControlComponent', () => {
  let component: ClientsControlComponent;
  let fixture: ComponentFixture<ClientsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
