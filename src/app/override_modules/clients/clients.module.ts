import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsComponent } from './clients.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';
import { ClientsControlComponent } from './clients-control/clients-control.component';
import { AllClientsComponent } from './all-clients/all-clients.component';


@NgModule({
  declarations: [ClientsComponent, ClientsControlComponent, AllClientsComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class ClientsModule { }
