import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestPurchaseRoutingModule } from './request-purchase-routing.module';
import { RequestPurchaseComponent } from './request-purchase.component';
import { RequestParchaseControlComponent } from './request-parchase-control/request-parchase-control.component';
import { AllRequestParchaseComponent } from './all-request-parchase/all-request-parchase.component';
import { GenericModule } from 'src/app/shared/generic/generic.module';
import { DynamicFormBuilderModule } from 'src/app/shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [RequestPurchaseComponent, RequestParchaseControlComponent, AllRequestParchaseComponent],
  imports: [
    CommonModule,
    RequestPurchaseRoutingModule,
    GenericModule,
    DynamicFormBuilderModule,
    TranslateModule
  ]
})
export class RequestPurchaseModule { }
