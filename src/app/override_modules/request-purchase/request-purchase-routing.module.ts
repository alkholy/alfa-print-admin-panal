import { RequestParchaseControlComponent } from './request-parchase-control/request-parchase-control.component';
import { AllRequestParchaseComponent } from './all-request-parchase/all-request-parchase.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestPurchaseComponent } from './request-purchase.component';

const routes: Routes = [
  { path: '', component: RequestPurchaseComponent, children: [
    {path: '', pathMatch: 'full' , redirectTo: 'all'},
    {path: 'all', component: AllRequestParchaseComponent},
    {path: 'control', component: RequestParchaseControlComponent },
    {path: 'edit/:entityId', component: RequestParchaseControlComponent },
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestPurchaseRoutingModule { }
