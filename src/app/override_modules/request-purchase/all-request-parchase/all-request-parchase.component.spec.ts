import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRequestParchaseComponent } from './all-request-parchase.component';

describe('AllRequestParchaseComponent', () => {
  let component: AllRequestParchaseComponent;
  let fixture: ComponentFixture<AllRequestParchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllRequestParchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllRequestParchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
