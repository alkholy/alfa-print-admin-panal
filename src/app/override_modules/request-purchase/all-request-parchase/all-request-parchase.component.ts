import { Component, OnInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'app-all-request-parchase',
  templateUrl: './all-request-parchase.component.html',
  styleUrls: ['./all-request-parchase.component.scss']
})
export class AllRequestParchaseComponent implements OnInit {

  entityData: entityData = {
    name: 'requestPurchase',
    firestore: true,
    // path:'/pages/Brand',
    all: {
      columnDefs: [
        {
          headerName: this.translate.instant('id'),
          field: 'id',
          checkboxSelection: true,
          filter: true,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('name'),
          field: this.override.isRTL() ? 'name' : 'name',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Email'),
          field: this.override.isRTL() ? 'email' : 'email',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('Phone Numpers'),
          field: this.override.isRTL() ? 'phone' : 'phone',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },

        {
          headerName: this.translate.instant('item'),
          field: this.override.isRTL() ? 'item.itemName_ar' : 'item.itemName_en',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('quantity'),
          field: this.override.isRTL() ? 'quantity' : 'quantity',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('total'),
          field: this.override.isRTL() ? 'total' : 'total',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
        {
          headerName: this.translate.instant('file'),
          field: this.override.isRTL() ? 'file' : 'file',
          filter: true,
          checkboxSelection: false,
          sortable: true,
          editable: false,
        },
      ],
    },
    edit: false,
    view: false,
    add: false,
    upload: false,
  };
  constructor(
    private translate: TranslateService,
    public override: OverrideService,
) { }

  ngOnInit(): void {
  }

}
