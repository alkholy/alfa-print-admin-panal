import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestParchaseControlComponent } from './request-parchase-control.component';

describe('RequestParchaseControlComponent', () => {
  let component: RequestParchaseControlComponent;
  let fixture: ComponentFixture<RequestParchaseControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestParchaseControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestParchaseControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
