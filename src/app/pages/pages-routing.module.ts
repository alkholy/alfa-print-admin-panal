import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { AuthGuard } from '../shared/services/auth.guard';

const routes: Routes = [
    {path: '', component: PagesComponent, children: [
        { path: 'entity', loadChildren: () => import('./entity/entity.module').then(m => m.EntityModule) , canActivate: [AuthGuard] },
        // { path: 'test', loadChildren: () => import('../override_modules/test/test.module').then(m => m.TestModule) },
        { path: '', redirectTo: 'entity', pathMatch: 'full' },
        { path: 'slider', loadChildren: () => import('../override_modules/slider-carousel/slider-carousel.module').then(m => m.SliderCarouselModule) , canActivate: [AuthGuard]},
        { path: 'welcome', loadChildren: () => import('../override_modules/welcome-section/welcome-section.module').then(m => m.WelcomeSectionModule) , canActivate: [AuthGuard] },
        { path: 'features', loadChildren: () => import('../override_modules/features/features.module').then(m => m.FeaturesModule) , canActivate: [AuthGuard] },
        { path: 'clients', loadChildren: () => import('../override_modules/clients/clients.module').then(m => m.ClientsModule) , canActivate: [AuthGuard] },
        { path: 'questions', loadChildren: () => import('../override_modules/questions/questions.module').then(m => m.QuestionsModule) , canActivate: [AuthGuard] },
        { path: 'benefits', loadChildren: () => import('../override_modules/benefits/benefits.module').then(m => m.BenefitsModule) , canActivate: [AuthGuard] },
        { path: 'serveices', loadChildren: () => import('../override_modules/services/services.module').then(m => m.ServicesModule) , canActivate: [AuthGuard] },
        { path: 'cards', loadChildren: () => import('../override_modules/cards/cards.module').then(m => m.CardsModule) , canActivate: [AuthGuard] },
        { path: 'imgDark', loadChildren: () => import('../override_modules/ima-dark/ima-dark.module').then(m => m.ImaDarkModule) , canActivate: [AuthGuard] },
        { path: 'resentProjects', loadChildren: () => import('../override_modules/resent-projects/resent-projects.module').then(m => m.ResentProjectsModule) , canActivate: [AuthGuard] },
        { path: 'footer', loadChildren: () => import('../override_modules/footer/footer.module').then(m => m.FooterModule) , canActivate: [AuthGuard] },
        { path: 'blogs', loadChildren: () => import('../override_modules/blogs/blogs.module').then(m => m.BlogsModule) , canActivate: [AuthGuard] },
        { path: 'aboutUs', loadChildren: () => import('../override_modules/about-us/about-us.module').then(m => m.AboutUsModule) , canActivate: [AuthGuard] },
        { path: 'messageRequests', loadChildren: () => import('../override_modules/requests-message/requests-message.module').then(m => m.RequestsMessageModule) , canActivate: [AuthGuard] },
        { path: 'items', loadChildren: () => import('../override_modules/items/items.module').then(m => m.ItemsModule) , canActivate: [AuthGuard] },
        { path: 'requestPurchase', loadChildren: () => import('../override_modules/request-purchase/request-purchase.module').then(m => m.RequestPurchaseModule) , canActivate: [AuthGuard] },
        { path: 'accountManger', loadChildren: () => import('../override_modules/account-manger/account-manger.module').then(m => m.AccountMangerModule) , canActivate: [AuthGuard] },

    ]},
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})

export class PagesRoutingModule {}
