import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { entityData } from 'src/app/shared/entityData';
import { EntityService } from 'src/app/shared/services/entity.service';
import { OverrideService } from 'src/app/shared/services/override.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { API_URLS } from 'src/app/shared/API_URLS';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() columnDefs: any[];
  @Input() rowData: Object[];
  @Input() entityName: string; 


  @Input() entityData: entityData;
  // rowData;

  constructor(
    private entity: EntityService, 
    private override: OverrideService, 
    private router: Router, 
    private route: ActivatedRoute,
    private translate: TranslateService,
    private toaster: NbToastrService) { }

  ngOnInit(): void {


    this.entity.getAll("https://jsonplaceholder.typicode.com/posts").then(
      res => {
        res.subscribe(
          data => {
            this.rowData = data.slice(0,11);
          }
        )
      }
    );
  }

  onChange(row) {
    this.router.navigate([`/pages/${this.entityName}/edit/${row.id}`], {relativeTo: this.route});

  }

  onDeleteClick(row) {
    // const data = event.rowData.data;

    if (confirm(this.translate.instant("sure?"))) {
      this.rowData = this.rowData.filter(
        item => item["id"] !== row.id
      );
      this.entity
        .delete(API_URLS[`${this.entityName}`]["delete"], row, row.id)
        .then((res) => {
          res.subscribe(
            () => {
              // this.gridApi.updateRowData({ remove: [data] });

              this.toaster.show(
                this.translate.instant("record deleted successfully") +
                  ` ${this.translate.instant("ID")} : (${row.id})`,
                this.translate.instant(`Success`),
                {
                  status: "success" as NbComponentStatus,
                  destroyByClick: true,
                  duration: 5000,
                  hasIcon: false,
                  position: this.override.isRTL()
                    ? NbGlobalPhysicalPosition.TOP_LEFT
                    : NbGlobalPhysicalPosition.TOP_RIGHT,
                  preventDuplicates: false,
                }
              );
            },
            (error) => {
              this.toaster.show(
                this.translate.instant(error.error),
                this.translate.instant(`Faild`),
                {
                  status: "danger" as NbComponentStatus,
                  destroyByClick: true,
                  duration: 5000,
                  hasIcon: false,
                  position: this.override.isRTL()
                    ? NbGlobalPhysicalPosition.TOP_LEFT
                    : NbGlobalPhysicalPosition.TOP_RIGHT,
                  preventDuplicates: false,
                }
              );
              // alert(error.error)
            }
          );
        });
    }
  }
}
