import { Component, OnInit, Input } from '@angular/core';
import { entityData } from "../../../shared/entityData";
import { EntityService } from "../../../shared/services/entity.service";
import { OverrideService } from 'src/app/shared/services/override.service';
import { take, takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-entity-all',
  templateUrl: './entity-all.component.html',
  styleUrls: ['./entity-all.component.scss']
})
export class EntityAllComponent implements OnInit {
  @Input() entityData: entityData;
  rowData;

  constructor(private entity: EntityService, private override: OverrideService) { }

  ngOnInit(): void {
    this.entityData = {
      name: "entity",
      label: "articles",
      path: "entity",
      all: {
        columnDefs: [
          {
            headerName: "id"
          },
          {
            headerName: "userId"
          },
          {
            headerName: "title"
          },
          {
            headerName: "body"
          },
        ]
      },
      edit: true,
      view: true,
      add: true
    };

    // this.entity.getAll("https://jsonplaceholder.typicode.com/posts").then(
    //   res => {
    //     res.subscribe(
    //       data => {
    //         this.rowData = data.slice(0, 11);
    //         console.log(data);
    //       }
    //     )
    //   }
    // )
  }

}
