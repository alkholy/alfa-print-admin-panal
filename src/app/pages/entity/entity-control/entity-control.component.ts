import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from 'src/app/shared/services/override.service';
import { genericForm } from 'src/app/shared/generic/form/genericForm';
import { Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-entity-control',
  templateUrl: './entity-control.component.html',
  styleUrls: ['./entity-control.component.scss']
})
export class EntityControlComponent implements OnInit {

  constructor(
    private translate: TranslateService,
    public override: OverrideService
  ) {}
  form: genericForm = {
    entityName: "entity",
    path: "entity",

    fields: [
      {
        type: "text",
        name: "username",
        label: this.translate.instant("User Name"),
        required: true,
        value: "",
        validators: [Validators.required, Validators.minLength(6)],
        class: "col-md-6",
      },

      {
        type: "password",
        name: "password",
        label: this.translate.instant("Password"),
        required: true,
        validators: [Validators.required, Validators.minLength(6)],
        details: this.translate.instant("User Default Password minimum 6"),
        class: "col-md-6",
      },
      {
        type: "text-localized",
        name: "name",
        label: this.translate.instant("Name"),
        required: true,
        value: "",
        validators: [Validators.required],
        class: "col-md-3",
      },
      {
        type: "dropdown",
        name: "userType",
        label: this.translate.instant("User Type"),
        multiple: false,
        validators: [Validators.required],
        details: this.translate.instant("Select User Permissons"),

        options: [
          {
            value: "admin",
            label: this.translate.instant("admin"),
          },
          {
            value: "user",
            label: this.translate.instant("user")
          }
        ],
        required: true,
        disabled: false,
        class: "col-md-3",
      },
      {
        type: "checkbox",
        name: "isActive",
        label: this.translate.instant("Active"),
        required: true,
        class: "col-md-3",
      },
    ],
  };
  companyies = [];
  forms: FormGroup;
  recieve(event) {
    this.forms = event;
  }

  saved: boolean;
  rSaved(event) {
    this.saved = event;
  }

  ngOnInit(): void {
  }

}
