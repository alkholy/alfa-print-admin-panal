import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EntityComponent } from "./entity.component";
import { EntityAllComponent } from './entity-all/entity-all.component';
import { EntityControlComponent } from './entity-control/entity-control.component';

const routes: Routes = [
    {
        path: "", component: EntityComponent, children: [
            { path: "", component: EntityAllComponent },
            { path: "all", component: EntityAllComponent },
            { path: "control", component: EntityControlComponent },
            { path: "edit/:entityId", component: EntityControlComponent },
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class EntityRoutingModule {}