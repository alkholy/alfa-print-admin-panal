import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityComponent } from './entity.component';
import { TableButtonsComponent } from '../table-buttons/table-buttons.component';
import { EntityRoutingModule } from './entity-routing.module';
import { EntityAllComponent } from './entity-all/entity-all.component';
import { EntityControlComponent } from './entity-control/entity-control.component';
import { DynamicFormBuilderModule } from '../../shared/generic/form/dynamic-form-builder/dynamic-form-builder.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenericModule } from '../../shared/generic/generic.module';
import { TranslateModule } from '@ngx-translate/core';
import { NbLayoutModule } from '@nebular/theme';

@NgModule({
  declarations: [
    EntityComponent,
    TableButtonsComponent,
    // TableComponent,
    EntityAllComponent,
    EntityControlComponent
  ],
  imports: [
    EntityRoutingModule,
    CommonModule,
    DynamicFormBuilderModule,
    GenericModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    NbLayoutModule
  ]
})
export class EntityModule { }
