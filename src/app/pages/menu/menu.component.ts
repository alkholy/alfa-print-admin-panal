import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
    private translate: TranslateService
  ) { }



  // public menuItemList = [

  //   { link: true, dropdown: false, href: '/', title: 'Dashboard', icon: '' },
  //   {
  //     link: false,  dropdown: true, href: '/',  title: 'Pages',   icon: '',
  //     droplist:
  //       [
  //         { href: '/', listTitle: 'Page one',  icon: '' },
  //         { href: '/', listTitle: 'Page two',  icon: '' },
  //       ],

  //   },
  //   {
  //     link: false,  dropdown: true, href: '/',  title: 'Users',   icon: '',
  //     droplist:
  //       [
  //         { href: '/', listTitle: 'Add User',  icon: '' },
  //         { href: '/', listTitle: 'Users',  icon: '' },
  //       ],

  //   },

  //   {
  //     link: false,  dropdown: true, href: '/',  title: 'Articals',   icon: '',
  //     droplist:
  //       [
  //         { href: '/', listTitle: 'Add Artical ',  icon: '' },
  //         { href: '/', listTitle: 'View Articals',  icon: '' },
  //       ],

  //   },


  // ];

  items: NbMenuItem[] = [

    {
      title: this.translate.instant('Account Manger'),
      icon: 'people-outline',
      expanded: false,
      children: [
        {
          title: this.translate.instant('Account Manger Control'),
          icon: 'people-outline',
          link: 'accountManger/control'
        },
        {
          title: this.translate.instant('All Account Manger'),
          icon: 'people-outline',
          link: 'accountManger/all'

        },

      ],
    },
    {
      title: this.translate.instant('Home Page'),
      icon: 'home-outline',
      expanded: false,
      children: [
        {
          title: this.translate.instant('Slider Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Slider Control'),
              // icon: 'monitor-outline',
              link: 'slider/control'
            },
            {
              title: this.translate.instant('All Slider'),
              // icon: 'monitor-outline',
              link: 'slider/all'

            },

          ],
        },
        {
          title: this.translate.instant('Welcome Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Welcome Control'),
              // icon: 'monitor-outline',
              link: 'welcome/control'
            },
            {
              title: this.translate.instant('All Welcome'),
              // icon: 'monitor-outline',
              link: 'welcome/all'

            },

          ],
        },
        {
          title: this.translate.instant('Features Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Features Control'),
              // icon: 'monitor-outline',
              link: 'features/control'
            },
            {
              title: this.translate.instant('All Features'),
              // icon: 'monitor-outline',
              link: 'features/all'

            },

          ],
        },
        {
          title: this.translate.instant('Clients Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Clients Control'),
              // icon: 'monitor-outline',
              link: 'clients/control'
            },
            {
              title: this.translate.instant('All Clients'),
              // icon: 'monitor-outline',
              link: 'clients/all'

            },

          ],
        },
        {
          title: this.translate.instant('Questions Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Questions Control'),
              // icon: 'monitor-outline',
              link: 'questions/control'
            },
            {
              title: this.translate.instant('All Questions'),
              // icon: 'monitor-outline',
              link: 'questions/all'

            },

          ],
        },
        {
          title: this.translate.instant('Benefits Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Benefits Control'),
              // icon: 'monitor-outline',
              link: 'benefits/control'
            },
            {
              title: this.translate.instant('All Benefits'),
              // icon: 'monitor-outline',
              link: 'benefits/all'

            },

          ],
        },
        {
          title: this.translate.instant('Serveices Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Serveices Control'),
              // icon: 'monitor-outline',
              link: 'serveices/control'
            },
            {
              title: this.translate.instant('All Serveices'),
              // icon: 'monitor-outline',
              link: 'serveices/all'

            },

          ],
        },
        {
          title: this.translate.instant('Cards Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Cards Control'),
              // icon: 'monitor-outline',
              link: 'cards/control'
            },
            {
              title: this.translate.instant('All Cards'),
              // icon: 'monitor-outline',
              link: 'cards/all'

            },

          ],
        },
        {
          title: this.translate.instant('Img Dark Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Img Dark Control'),
              // icon: 'monitor-outline',
              link: 'imgDark/control'
            },
            {
              title: this.translate.instant('All Img Dark'),
              // icon: 'monitor-outline',
              link: 'imgDark/all'

            },

          ],
        },
        {
          title: this.translate.instant('Resent Projects Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Resent Projects Control'),
              // icon: 'monitor-outline',
              link: 'resentProjects/control'
            },
            {
              title: this.translate.instant('All Resent Projects'),
              // icon: 'monitor-outline',
              link: 'resentProjects/all'

            },

          ],
        },
        {
          title: this.translate.instant('Footer Section'),
          icon: 'monitor-outline',
          expanded: false,
          children: [
            {
              title: this.translate.instant('Footer Control'),
              icon: 'monitor-outline',
              link: 'footer/control'
            },
            {
              title: this.translate.instant('All Footer'),
              icon: 'monitor-outline',
              link: 'footer/all'

            },

          ],
        },

      ],
    },

    {
      title: this.translate.instant('blogs Page'),
      icon: 'monitor-outline',
      expanded: false,
      children: [
        {
          title: this.translate.instant('Blogs Control'),
          // icon: 'monitor-outline',
          link: 'blogs/control'
        },
        {
          title: this.translate.instant('All Blogs'),
          // icon: 'monitor-outline',
          link: 'blogs/all'

        },

      ],
    },
    {
      title: this.translate.instant('About Us Pages'),
      icon: 'question-mark-circle-outline',
      expanded: false,
      children: [
        {
          title: this.translate.instant('aboutUs Control'),
          // icon: 'monitor-outline',
          link: 'aboutUs/control'
        },
        {
          title: this.translate.instant('All aboutUs'),
          // icon: 'monitor-outline',
          link: 'aboutUs/all'

        },

      ],
    },
    {
      title: this.translate.instant('Items'),
      icon: 'folder-outline',
      expanded: false,
      children: [
        {
          title: this.translate.instant('Items Control'),
          // icon: 'monitor-outline',
          link: 'items/control'
        },
        {
          title: this.translate.instant('All Items'),
          // icon: 'monitor-outline',
          link: 'items/all'

        },

      ],
    },
    {
      title: this.translate.instant('Request Purchase'),
      icon: 'shopping-cart-outline',
      expanded: false,
      children: [
        // {
        //   title: this.translate.instant('Request Purchase Control'),
        //   icon: 'monitor-outline',
        //   link: 'requestParchase/control'
        // },
        {
          title: this.translate.instant('All Request Purchase'),
          // icon: 'monitor-outline',
          link: 'requestPurchase/all'

        },

      ],
    },
    {
      title: this.translate.instant('Requests Message'),
      icon: 'message-square-outline',
      expanded: false,
      link: 'messageRequests/all'
    },

  ];

  ngOnInit(): void {
  }





}
