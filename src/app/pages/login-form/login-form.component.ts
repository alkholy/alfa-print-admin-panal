import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../theming.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  form: FormGroup;
  constructor(
    private themeService: ThemeService,
    private authService: AuthService
  ) {}

  imgSrc = '';
  ngOnInit(): void{
    this.form = new FormGroup({
      email: new FormControl('', { validators: [Validators.required, Validators.email] }),
      password: new FormControl('', { validators: [Validators.required, Validators.minLength(8)] }),
    });
    this.logoIMG();
  }

  get email(){return this.form.get('email'); }
  get password(){return this.form.get('password'); }
  onSubmit(){
    console.log(this.form.value);

    if (this.form.invalid) {
      return;
    }

    this.authService.login(
      this.form.value.email,
     this.form.value.password
    ).then(res => { console.log(res);
    });

  }
  logoIMG() {
    if (this.themeService.isDarkTheme() || localStorage.getItem('activeTheme') === 'dark') {
      this.imgSrc = '../../../assets/logo.png';
    } else {
      this.imgSrc = '../../../assets/logodark.png';
    }
  }

}
