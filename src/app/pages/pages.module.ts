import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { NbLayoutModule, NbSidebarModule, NbIconModule, NbButtonModule, NbMenuModule } from '@nebular/theme';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuComponent } from './menu/menu.component';
import { TableButtonsComponent } from './table-buttons/table-buttons.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { TableComponent } from './table/table.component';
import { FormCardComponent } from './form-card/form-card.component';
import { CardsComponent } from './cards/cards.component';
import { EntityComponent } from './entity/entity.component';
import { EntityModule } from './entity/entity.module';
import { ThemeModule } from '../@theme/theme.module';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { GenericModule } from '../shared/generic/generic.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PagesComponent,

    MenuComponent,
    // BreadcrumbComponent,
    // TableButtonsComponent,
    LoginFormComponent,
    FormCardComponent,
    CardsComponent],
  imports: [
    CommonModule,
    PagesRoutingModule, // RouterModule.forRoot(routes, { useHash: true }), if this is your app.module
    NbLayoutModule,
    NbSidebarModule, // NbSidebarModule.forRoot(), //if this is your app.module
    NbButtonModule,
    EntityModule,
    ThemeModule,
    NbMenuModule,
    ReactiveFormsModule,
    // GenericModules
  ]
})
export class PagesModule { }
