import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ThemingModule { }


// theming
export interface Theme {
  name: string;
  properties: any;

}

export const light: Theme = {
  name: "light",
  properties: {
    "--color-default": "#08090A",
    "--color-secondary": "#41474D",
    "--color-tertiary": "#797C80",
    "--color-quaternary": "#F4FAFF",
    "--color-light": "#41474D",

    "--background-default": "#F4FAFF",
    "--background-secondary": "#A3B9CC",
    "--background-tertiary": "#f8f8f8",
    "--background-light": "#FFFFFF",

    "--primary-default": "#0069D9",
    "--primary-dark": "#24B286",
    "--primary-light": "#B2FFE7",

    "--error-default": "#EF3E36",
    "--error-dark": "#800600",
    "--error-light": "#FFCECC",

    "--background-tertiary-shadow": "0 1px 3px 0 rgba(92, 125, 153, 0.5)",
    "--shadow-sm": "0 0.125rem 0.25rem rgba(0, 0, 0, 0.075)",
    "--shadow-md": "0 0.5rem 1rem rgba(0, 0, 0, 0.15",
    "--shadow-lg": "0 1rem 3rem rgba(0, 0, 0, 0.175)"
  }
};

export const dark: Theme = {
  name: "dark",
  properties: {
    "--color-default": "gray",
    "--color-secondary": "#A3B9CC",
    "--color-tertiary": "#F4FAFF",
    "--color-quaternary": "#E5E5E5",
    "--color-light": "#FFFFFF",

    "--background-default": "#797C80",
    "--background-secondary": "#41474D",
    "--background-tertiary": "#18191B",
    "--background-light": "#1E2022",

    "--primary-default": "#0069D9",
    "--primary-dark": "#24B286",
    "--primary-light": "#B2FFE7",

    "--error-default": "#EF3E36",
    "--error-dark": "#800600",
    "--error-light": "#FFCECC",

    "--background-tertiary-shadow": "0 1px 3px 0 rgba(8, 9, 10, 0.5)",
    "--shadow-sm": "0 0.5rem 1rem rgba(0, 0, 0, 0.15",
    "--shadow-md": "0 1rem 3rem rgba(0, 0, 0, 0.175)",
    "--shadow-lg": "0 1.125rem 3.5rem rgba(0, 0, 0, 0.195)"
  }
};