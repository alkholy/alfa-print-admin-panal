import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  // tslint:disable-next-line: no-input-rename
  @Input('path') path: { entity: string, section: string };

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
  }
}
