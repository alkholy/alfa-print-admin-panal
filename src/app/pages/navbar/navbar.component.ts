import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../theming.service';
import { NbSidebarService, NbThemeService } from '@nebular/theme';
import { OverrideService } from 'src/app/shared/services/override.service';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

   imgSrc = '../../../assets/logodark.png';
   title = 'Dark mode';
   checked = '';
  constructor(
    private themeService: ThemeService,
    private nbTheme: NbThemeService,
    private sidebarService: NbSidebarService,
    public override: OverrideService,
    public authService: AuthService,
    private router: Router
  ) {
    this.language.setValue(override.currentLang);
  }
    language:FormControl = new FormControl({});
  ngOnInit() {

    this.language.valueChanges.subscribe(val => {
      // console.log(val);
      this.override.changeLanguage(val);

    })
    this.selectTheme();
  }

  selectTheme() {
    if (this.themeService.isDarkTheme() ) {
      this.imgSrc = '../../../assets/logo.png';
      this.title = 'Light mode';
      this.checked = 'checked';
      localStorage.setItem('currentTheme', 'cosmic');
      this.nbTheme.changeTheme('cosmic');

    } else {
      this.imgSrc = '../../../assets/logodark.png';
      this.title = 'Dark mode';
      this.nbTheme.changeTheme('corporate');
      localStorage.setItem('currentTheme', 'corporate');


    }
  }

  toggleTheme() {
    if (this.themeService.isDarkTheme()) {
      this.themeService.setLightTheme();
      localStorage.setItem( 'activeTheme', 'light');
      this.nbTheme.changeTheme('cosmic');
      this.nbTheme.changeTheme('cosmic');

    } else {
      this.themeService.setDarkTheme();
      localStorage.setItem( 'activeTheme', 'dark');
      this.nbTheme.changeTheme('corporate');
      localStorage.setItem('currentTheme', 'corporate');


    }

    this.selectTheme();
  }

  active: boolean = false;
  // toggleMenu(){
  //     this.active = !this.active;
  // }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
    this.active = !this.active;
  }

  login(){
    this.authService.signOut();
    // this.router.navigate(['/login']);
  }

}
