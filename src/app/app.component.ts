import { Component, OnInit } from '@angular/core';
import { ThemeService } from './theming.service';
import { TranslateService } from '@ngx-translate/core';
import { OverrideService } from './shared/services/override.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'admin-panel';
  constructor(
    private themeService: ThemeService,
    private traslate: TranslateService,
    private override: OverrideService
  ) {}

  ngOnInit() {
    this.selectTheme();
    this.traslate.setDefaultLang(this.override.currentLang ? this.override.currentLang : 'en');
    this.traslate.use(this.override.currentLang);
    this.override.changeLanguage(this.override.currentLang ? this.override.currentLang : 'en');
  }
  selectTheme() {
    if (this.themeService.isDarkTheme() ||  localStorage.getItem('activeTheme') === 'dark') {
      this.themeService.setDarkTheme();
    } else {

    }
  }

}
