import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EntityService } from '../services/entity.service';
import { OverrideService } from '../services/override.service';

@Injectable({
  providedIn: 'root'
})
export class SystemAdminGuard implements CanActivate {
  constructor(public override:OverrideService,private entity:EntityService,private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let auth =  this.override.userType=='systemAdmin';

    // if(!auth){
    //   this.router.navigate(['/auth/login']);
    //   return false ;
    // } else {
    //   return true;
    // }
    return true;
  }

}
