import { TestBed, async, inject } from '@angular/core/testing';

import { CompanyOwnerGuard } from './company-owner.guard';

describe('CompanyOwnerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyOwnerGuard]
    });
  });

  it('should ...', inject([CompanyOwnerGuard], (guard: CompanyOwnerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
