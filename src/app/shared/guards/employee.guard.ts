import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OverrideService } from '../services/override.service';
import { EntityService } from '../services/entity.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeGuard implements CanActivate {
  constructor(public override:OverrideService,private entity:EntityService,private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // return this.override.userType==='user'

    // let auth =  this.override.userType=='systemAdmin' || this.override.userType =='companyOwner' || this.override.userType =='companyAdmin'  || this.override.userType =='user' ;

    let auth = this.override.loggedIn;
    // console.log(auth);
    
    if(!auth){
      // this.router.navigate(['/auth/login']);
      return true ;
    } else {
      return true;
    }
    return true;
  }

}
