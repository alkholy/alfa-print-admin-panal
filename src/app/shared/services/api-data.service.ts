import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
import { Config } from "protractor";
import { Observable, throwError, EMPTY } from "rxjs";
import { map, retry, catchError } from "rxjs/operators";
import { CookieConsentService } from "./cookie-consent.service";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { API_URLS } from '../API_URLS';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';
import { OverrideService } from './override.service';
import { environment } from '../../../environments/environment';
import { EntityService } from './entity.service';

@Injectable({
  providedIn: "root",
})
export class ApiDataService {
  headers: HttpHeaders;

  config: Config;

  data: any;
  error: any;
  httpOptions: { headers: HttpHeaders; observe: "response" };
  public online: boolean = navigator.onLine;

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private router: Router,
    private toastrService: NbToastrService,
    private override: OverrideService,
  ) { }

  async add(
    url,
    body,
    headers?: HttpHeaders,
    contentType?: string
  ): Promise<Observable<any>> {
    if (this.online) {

    this.httpOptions = this.getHeadders(headers, contentType);
    if (url && body) {
      return await this.http.post<Config>(url, body, this.httpOptions).pipe(
        map(
          (data) => {
            return data.body;
          },
          (error) => {
            return error;
          }
        ),
        catchError((err) => {
          this.resolveNotAuthrized(err);
          return err;
        })
      );
    }
  } else {
    this.showErrorToast( "You Are Offilne, Please Check your Internet Connection")
  }
  }

  // async login(url: string, credintials: any): Promise<Observable<any>> {
  //   const headers = new HttpHeaders({
  //     "content-type": "Application/json",
  //     // Authorization: "Bearer Y29tLm92ZXJyaWRlZWcuYXBwcy5vcGFzcy5hZG1pblBhbmVs"
  //   });

  //   if (url && credintials) {
  //     return this.http
  //       .post<Config>(url, credintials, { headers, observe: "response" })
  //       .pipe(
  //         map(
  //           (data) => {
  //             return data.body;
  //           },
  //           (error) => {
  //             return error;
  //           }
  //         )
  //       );
  //   }
  // }

  async login(url: string, credintials: any): Promise<Observable<any>> {
    if (this.online) {
      this.httpOptions = {
        headers: new HttpHeaders({
          Authorization: "Basic " + environment.clientId,
          "content-type": "application/x-www-form-urlencoded",
        }),
        observe: "response",
      };
      if (url && credintials) {
        // const params = new HttpParams({
        //   fromObject: {
        //     grant_type: "password",
        //     username: credintials.username,
        //     password: credintials.password,
        //   },
        // });
        const body = JSON.stringify(credintials);
        console.log(url, body);

        return await this.http.post<Config>(url, body, this.httpOptions).pipe(
          retry(3),
          map(
            (data) => {
              return data.body;
            },
            (error) => {
              return error;
            }
          )
        );
      } else {
        this.showErrorToast( "You Are Offilne, Please Check your Internet Connection")
      }

    }
  }

  async refreshAuth(url: string): Promise<Observable<any>> {
    if (this.online) {

      this.httpOptions = {
        headers: new HttpHeaders({
          Authorization: "Basic " + environment.clientId,
          "content-type": "application/x-www-form-urlencoded",
        }),
        observe: "response",
      };
      if (url) {
        const params = new HttpParams({
          fromObject: {
            grant_type: "refresh_token",
            refresh_token: sessionStorage.getItem('token'),
          },
        });

        return await this.http.post<Config>(url, params, this.httpOptions).pipe(
          retry(3),
          map(
            (data) => {
              return data.body;
            },
            (error) => {
              return error;
            }
          )
        );
      }
    } else {
      this.showErrorToast( "You Are Offilne, Please Check your Internet Connection")
    }
  }

  async update(
    url,
    body,
    headers?: HttpHeaders,
    contentType?: string
  ): Promise<Observable<any>> {
    if (this.online){
      this.httpOptions = this.getHeadders(headers, contentType);

      if (url && body) {
        return await this.http.put(url, body, this.httpOptions).pipe(
          map(
            (data) => {
              return data.body;
            },
            (error) => {
              return error;
            }
          ),
          catchError((err) => {
            this.resolveNotAuthrized(err);
            return err;
          })
        );
      }
    }else {
      this.showErrorToast( "You Are Offilne, Please Check your Internet Connection")
    }
 
  }

  async delete(
    url,
    body,
    headers?: HttpHeaders,
    contentType?: string
  ): Promise<Observable<any>> {
    this.httpOptions = this.getHeadders(headers, contentType);
    if (this.online){
    if (url) {
      return await this.http
        .request("delete", url, {
          body: body,
          headers: this.httpOptions.headers,
          observe: this.httpOptions.observe,
        })
        .pipe(
          map((data) => {
            return data.body;
          }),
          catchError((err) => {
            if (err.status == 500) {
              retry(3);
            }
            this.resolveNotAuthrized(err);
            return err;
          })
        );
    }
  } else {
    this.showErrorToast( "You Are Offilne, Please Check your Internet Connection")
  }
  }

  async getData(
    url: string,
    headers?: HttpHeaders,
    contentType?: string,
    lastId?: number
  ): Promise<Observable<any>> {
    if (this.online) {

    this.httpOptions = this.getHeadders(headers, contentType, lastId);

    if (url) {
      return this.http.get<Config>(url, this.httpOptions).pipe(
        map(
          (data) => {
            return data.body;
          },
          (error) => {
            return error;
          }
        ),
        catchError((err) => {
          this.resolveNotAuthrized(err);
          return err;
        })
      );
    }
  } else {
    this.showErrorToast( "You Are Offilne, Please Check your Internet Connection")
  }
  }
  resolveNotAuthrized(error) {
    console.log(error);

    if (error.status == 401) {
      this.refreshAuth(API_URLS.auth.auth)
      this.toastrService.show(
        this.translate.instant(error.error.error),
        error.error.status,
        {
          status: "danger" as NbComponentStatus,
          destroyByClick: true,
          duration: 5000,
          hasIcon: false,
          position: this.override.isRTL() ? NbGlobalPhysicalPosition.TOP_LEFT : NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: false,
        }
      );
      this.router.navigate(["/auth/login"]);
    } else {
      this.toastrService.show(
        this.translate.instant(error.error.error),
        error.error.status,
        {
          status: "danger" as NbComponentStatus,
          destroyByClick: true,
          duration: 5000,
          hasIcon: false,
          position: this.override.isRTL()
            ? NbGlobalPhysicalPosition.TOP_LEFT
            : NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: false,
        }
      );
      return error;

    }
  }
  showErrorToast(message){
    this.toastrService.show(
      this.translate.instant(message),this.translate.instant("Internet"),
      {
        status: "warning" as NbComponentStatus,
        destroyByClick: true,
        duration: 5000,
        hasIcon: false,
        position: this.override.isRTL()
          ? NbGlobalPhysicalPosition.TOP_LEFT
          : NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: false,
      }
    );
  }

  private getHeadders(
    headers?: HttpHeaders,
    contentType?: string,
    lastId?: number
  ): { headers: HttpHeaders; observe: "response" } {
    if (!lastId)
      return {
        headers: headers
          ? headers
          : new HttpHeaders({
            "content-type": contentType || "Application/json",
            Authorization: "Bearer " + sessionStorage.getItem("toekn"),
          }),
        observe: "response",
      };

    else {
      return {
        headers: headers
          ? headers
          : new HttpHeaders({
            "content-type": contentType || "Application/json",
            Authorization: "Bearer " + sessionStorage.getItem("toekn"),
            lastId: `${lastId}`
          }),
        observe: "response",
      }
    }
  }

  public getHTTPHeadders(
    headers?: HttpHeaders,
    contentType?: string
  ): HttpHeaders {
    var headers =
      headers ||
      new HttpHeaders()
        // .set("content-type", contentType || "Application/json")
        .set("Authorization", "Bearer " + sessionStorage.getItem("toekn"));
    return headers;
  }
}
