export interface User {
  id?: string;
  uid?: string;
  email?: string;
  photoURL?: string;
  displayName?: string;
  myCustomData?: string;
  enabled?:boolean,
  password?:string
}