import { Injectable } from "@angular/core";
import { ApiDataService } from "./api-data.service";
import { map, switchMap } from "rxjs/operators";
import { Observable } from "rxjs";
import { OverrideService } from "./override.service";
import {
  NbToastrService,
  NbComponentStatus,
  NbGlobalPhysicalPosition,
} from "@nebular/theme";
import { TranslateService } from "@ngx-translate/core";
@Injectable({
  providedIn: "root",
})
export class EntityService {
  allrecords: any[];
  record: any;

  constructor(
    private APIService: ApiDataService,
    private toastrService: NbToastrService,
    private override: OverrideService,
    private translate: TranslateService
  ) {}
  async save(
    url: string,
    body: any,
    contentType?: any
  ): Promise<Observable<any>> {
    if (body) {
      return (
        await this.APIService.add(url, body, null, contentType)
      ).pipe(
        map(
          (res) => {
            this.showSuccessToast("Created");
            return res;
          },

          (error) => {
            this.showErrorToast(error);
            return error;
          }
        )
      );
    }
  }

  showSuccessToast(type: string) {
    this.toastrService.show(
      this.translate.instant("record " + type + " successfully"),
      "",
      {
        status: "info" as NbComponentStatus,
        destroyByClick: true,
        duration: 5000,
        hasIcon: false,
        position: this.override.isRTL()
          ? NbGlobalPhysicalPosition.TOP_LEFT
          : NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: false,
      }
    );
  }

  showErrorToast(error?: any, errorString?: string) {
    
    this.toastrService.show(
      this.translate.instant(errorString ? errorString : error.error.error),
      errorString ? this.translate.instant("error") : error.error.error,
      {
        status: "danger" as NbComponentStatus,
        destroyByClick: true,
        duration: 5000,
        hasIcon: false,
        position: this.override.isRTL()
          ? NbGlobalPhysicalPosition.TOP_LEFT
          : NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: false,
      }
    );
  }

  async update(url, body, entityId?: any) {
    if (body) {
      if (entityId) {
        var url = url.replace("{entityId}", entityId);
      }
      return (
        await this.APIService.update(url, body, null, null)
      ).pipe(
        map(
          (res) => {
            this.showSuccessToast("Updated");

            return res;
          },
          (error) => {
            this.showErrorToast(error);

            return error;
          }
        )
      );
    }
  }

  async delete(url, body?, entityId?: string) {
    if (body) {
      if (entityId) {
        var url = url.replace("{entityId}", entityId);
      }
      return (
        await this.APIService.delete(url, body, null, null)
      ).pipe(
        map(
          (res) => {
            return res;
          },
          (error) => {
            this.showErrorToast(error);
            return error;
          }
        )
      );
    }
  }

  async getAll(
    url: string,
    lastId?: number
  ): Promise<Observable<any>> {
    this.allrecords = [];
    return (await this.APIService.getData(url, null, null,lastId)).pipe(
      map(
        (res) => {
          return res;
        },
        (error) => {
          this.showErrorToast(error);
          return error;
        }
      )
    );
  }

  async getOne(
    url,
    entityId: any
  ): Promise<Observable<any>> {
    if (entityId) {
      var url = url.replace("{entityId}", entityId);
      return (await this.APIService.getData(url, null, null)).pipe(
        map(
          (res) => {
            return res;
          },
          (error) => {
            this.showErrorToast(error);
            return error;
          }
        )
      );
    }
  }

  async getData(  url: string  ): Promise<Observable<any>> {

      return (await this.APIService.getData(url, null, null)).pipe(
        map(
          (res) => {
            return res;
          },
          (error) => {
            this.showErrorToast(error);
            return error;
          }
        )
      );
    
  }
}
