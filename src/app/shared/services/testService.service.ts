import { Inject, Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: "root"
})

export class TestService {
    studentsSubject: Subject<Object[]> = new Subject<Object[]>();
    constructor() {}

    students: Object[] = [
        {
            id: 0,
            name: "student 1",
            section: 2,
            level: 3
        },
        {
            id: 1,
            name: "student 1",
            section: 2,
            level: 3
        },
        {
            id: 2,
            name: "student 1",
            section: 2,
            level: 3
        },
        {
            id: 3,
            name: "student 1",
            section: 2,
            level: 3
        },
    ];

    getStudents() {
        this.studentsSubject.next(this.students)
    }
}