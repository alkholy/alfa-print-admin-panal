import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Styles } from '../styles';
import { Lang } from '../lang.enum';
import { NbSidebarComponent, NbSidebarService } from '@nebular/theme';
import { AbstractControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { PagesComponent } from '../../pages/pages.component';

@Injectable({
  providedIn: 'root',
})
export class OverrideService {

  constructor(
    private translate: TranslateService,
    private titleService: Title
  ) {
    this.language = of(localStorage.getItem('language'));
  }

  // Languages
  get currentLang() {
    return localStorage.getItem('language');
  }
  get lang(): Observable<string> {
    return this.language;
  }

  public get userType(): string {
    return atob(sessionStorage.getItem(btoa('userType')));
  }

  public get loggedIn(): boolean {

   return   sessionStorage.getItem('toekn') != null;

  }

  get otherLang() {
    switch (localStorage.getItem('language')) {
      case 'en':
        return 'ar';
      case 'ar':
        return 'en';
      default:
        return 'en';
    }

  }


public get timeZone(): string {
  return Intl.DateTimeFormat().resolvedOptions().timeZone ;
}
  language: Observable<string>;
  PagesComponent: PagesComponent ;



  // groupBy =  function (xs, key) {
  //   return xs.reduce(function (rv, x) {
  //     // console.info(x,rv);
  //     (rv[x[key]] = rv[x[key]] || []).push(x);

  //     let ob = {
  //       key : rv[x[key]],
  //       object:x
  //     }
  //     return rv;
  //   }, {});
  // };

  phoneNumberRegExp =
    '(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))s*[)]?[-s.]?[(]?[0-9]{1,3}[)]?([-s.]?[0-9]{3})([-s.]?[0-9]{3,4})';
    websiteRegExp='(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})';



  localeText = {
    // for filter panel
    page: this.translate.instant('page'),
    more: this.translate.instant('more'),
    to: this.translate.instant('to'),
    of: this.translate.instant('of'),
    next: this.translate.instant('next'),
    last: this.translate.instant('last'),
    first: this.translate.instant('first'),
    previous: this.translate.instant('previous'),
    loadingOoo: this.translate.instant('loadingOoo'),

    // for set filter
    selectAll: this.translate.instant('selectAll'),
    searchOoo: this.translate.instant('searchOoo'),
    blanks: this.translate.instant('blanks'),

    // for number filter and text filter
    filterOoo: this.translate.instant('filterOoo'),
    applyFilter: this.translate.instant('applyFilter'),
    equals: this.translate.instant('equals'),
    notEqual: this.translate.instant('notEqual'),

    // for number filter
    lessThan: this.translate.instant('lessThan'),
    greaterThan: this.translate.instant('greaterThan'),
    lessThanOrEqual: this.translate.instant('lessThanOrEqual'),
    greaterThanOrEqual: this.translate.instant('greaterThanOrEqual'),
    inRange: this.translate.instant('inRange'),

    // for text filter
    contains: this.translate.instant('contains'),
    notContains: this.translate.instant('notContains'),
    startsWith: this.translate.instant('startsWith'),
    endsWith: this.translate.instant('endsWith'),

    // filter conditions
    andCondition: this.translate.instant('andCondition'),
    orCondition: this.translate.instant('orCondition'),

    // the header of the default group column
    group: this.translate.instant('group'),

    // tool panel
    columns: this.translate.instant('columns'),
    filters: this.translate.instant('filters'),
    rowGroupColumns: this.translate.instant('rowGroupColumns'),
    rowGroupColumnsEmptyMessage: this.translate.instant(
      'rowGroupColumnsEmptyMessage'
    ),
    valueColumns: this.translate.instant('valueColumns'),
    pivotMode: this.translate.instant('pivotMode'),
    groups: this.translate.instant('pivotMode'),
    values: this.translate.instant('values'),
    pivots: this.translate.instant('pivots'),
    valueColumnsEmptyMessage: this.translate.instant(
      'valueColumnsEmptyMessage'
    ),
    pivotColumnsEmptyMessage: this.translate.instant(
      'pivotColumnsEmptyMessage'
    ),
    toolPanelButton: this.translate.instant('toolPanelButton'),

    // other
    noRowsToShow: this.translate.instant('noRowsToShow'),
    enabled: this.translate.instant('enabled'),

    // enterprise menu
    pinColumn: this.translate.instant('pinColumn'),
    valueAggregation: this.translate.instant('valueAggregation'),
    autosizeThiscolumn: this.translate.instant('autosizeThiscolumn'),
    autosizeAllColumns: this.translate.instant('autosizeAllColumns'),
    groupBy: this.translate.instant('groupBy'),
    ungroupBy: this.translate.instant('ungroupBy'),
    resetColumns: this.translate.instant('resetColumns'),
    expandAll: this.translate.instant('expandAll'),
    collapseAll: this.translate.instant('collapseAll'),
    toolPanel: this.translate.instant('toolPanel'),
    export: this.translate.instant('export'),
    csvExport: this.translate.instant('csvExport'),
    excelExport: this.translate.instant('excelExport'),
    excelXmlExport: this.translate.instant('excelXmlExport'),

    // enterprise menu (charts)
    pivotChartAndPivotMode: this.translate.instant('pivotChartAndPivotMode'),
    pivotChart: this.translate.instant('pivotChart'),
    chartRange: this.translate.instant('chartRange'),

    columnChart: this.translate.instant('columnChart'),
    groupedColumn: this.translate.instant('groupedColumn'),
    stackedColumn: this.translate.instant('stackedColumn'),
    normalizedColumn: this.translate.instant('normalizedColumn'),

    barChart: this.translate.instant('barChart'),
    groupedBar: this.translate.instant('groupedBar'),
    stackedBar: this.translate.instant('stackedBar'),
    normalizedBar: this.translate.instant('normalizedBar'),

    pieChart: this.translate.instant('pieChart'),
    pie: this.translate.instant('pie'),
    doughnut: this.translate.instant('doughnut'),

    line: this.translate.instant('line'),

    xyChart: this.translate.instant('xyChart'),
    scatter: this.translate.instant('scatter'),
    bubble: this.translate.instant('bubble'),

    areaChart: this.translate.instant('areaChart'),
    area: this.translate.instant('area'),
    stackedArea: this.translate.instant('stackedArea'),
    normalizedArea: this.translate.instant('normalizedArea'),

    // enterprise menu pinning
    pinLeft: this.translate.instant('pinLeft'),
    pinRight: this.translate.instant('pinRight'),
    noPin: this.translate.instant('noPin'),

    // enterprise menu aggregation and status bar
    sum: this.translate.instant('sum'),
    min: this.translate.instant('min'),
    max: this.translate.instant('max'),
    none: this.translate.instant('none'),
    count: this.translate.instant('count'),
    average: this.translate.instant('average'),
    filteredRows: this.translate.instant('filteredRows'),
    selectedRows: this.translate.instant('selectedRows'),
    totalRows: this.translate.instant('totalRows'),
    totalAndFilteredRows: this.translate.instant('totalAndFilteredRows'),

    // standard menu
    copy: this.translate.instant('copy'),
    copyWithHeaders: this.translate.instant('copyWithHeaders'),
    ctrlC: this.translate.instant('ctrlC'),
    paste: this.translate.instant('paste'),
    ctrlV: this.translate.instant('ctrlV'),

    // charts
    pivotChartTitle: this.translate.instant('pivotChartTitle'),
    rangeChartTitle: this.translate.instant('rangeChartTitle'),
    settings: this.translate.instant('settings'),
    data: this.translate.instant('data'),
    format: this.translate.instant('format'),
    categories: this.translate.instant('categories'),
    series: this.translate.instant('series'),
    xyValues: this.translate.instant('xyValues'),
    paired: this.translate.instant('paired'),
    axis: this.translate.instant('axis'),
    color: this.translate.instant('color'),
    thickness: this.translate.instant('thickness'),
    xRotation: this.translate.instant('xRotation'),
    yRotation: this.translate.instant('yRotation'),
    ticks: this.translate.instant('ticks'),
    width: this.translate.instant('width'),
    length: this.translate.instant('length'),
    padding: this.translate.instant('padding'),
    chart: this.translate.instant('chart'),
    title: this.translate.instant('title'),
    background: this.translate.instant('background'),
    font: this.translate.instant('font'),
    top: this.translate.instant('top'),
    right: this.translate.instant('right'),
    bottom: this.translate.instant('bottom'),
    left: this.translate.instant('left'),
    labels: this.translate.instant('labels'),
    size: this.translate.instant('size'),
    minSize: this.translate.instant('minSize'),
    maxSize: this.translate.instant('maxSize'),
    legend: this.translate.instant('legend'),
    position: this.translate.instant('position'),
    markerSize: this.translate.instant('markerSize'),
    markerStroke: this.translate.instant('markerStroke'),
    markerPadding: this.translate.instant('markerPadding'),
    itemPaddingX: this.translate.instant('itemPaddingX'),
    itemPaddingY: this.translate.instant('itemPaddingY'),
    strokeWidth: this.translate.instant('strokeWidth'),
    offset: this.translate.instant('offset'),
    offsets: this.translate.instant('offsets'),
    tooltips: this.translate.instant('tooltips'),
    callout: this.translate.instant('callout'),
    markers: this.translate.instant('markers'),
    shadow: this.translate.instant('shadow'),
    blur: this.translate.instant('blur'),
    xOffset: this.translate.instant('xOffset'),
    yOffset: this.translate.instant('yOffset'),
    lineWidth: this.translate.instant('lineWidth'),
    normal: this.translate.instant('normal'),
    bold: this.translate.instant('bold'),
    italic: this.translate.instant('italic'),
    boldItalic: this.translate.instant('boldItalic'),
    predefined: this.translate.instant('predefined'),
    fillOpacity: this.translate.instant('fillOpacity'),
    strokeOpacity: this.translate.instant('strokeOpacity'),
    columnGroup: this.translate.instant('columnGroup'),
    barGroup: this.translate.instant('barGroup'),
    pieGroup: this.translate.instant('pieGroup'),
    lineGroup: this.translate.instant('lineGroup'),
    scatterGroup: this.translate.instant('scatterGroup'),
    areaGroup: this.translate.instant('areaGroup'),
    groupedColumnTooltip: this.translate.instant('groupedColumnTooltip'),
    stackedColumnTooltip: this.translate.instant('stackedColumnTooltip'),
    normalizedColumnTooltip: this.translate.instant('normalizedColumnTooltip'),
    groupedBarTooltip: this.translate.instant('groupedBarTooltip'),
    stackedBarTooltip: this.translate.instant('stackedBarTooltip'),
    normalizedBarTooltip: this.translate.instant('normalizedBarTooltip'),
    pieTooltip: this.translate.instant('pieTooltip'),
    doughnutTooltip: this.translate.instant('doughnutTooltip'),
    lineTooltip: this.translate.instant('lineTooltip'),
    groupedAreaTooltip: this.translate.instant('groupedAreaTooltip'),
    stackedAreaTooltip: this.translate.instant('stackedAreaTooltip'),
    normalizedAreaTooltip: this.translate.instant('normalizedAreaTooltip'),
    scatterTooltip: this.translate.instant('scatterTooltip'),
    bubbleTooltip: this.translate.instant('bubbleTooltip'),
    noDataToChart: this.translate.instant('noDataToChart'),
    pivotChartRequiresPivotMode: this.translate.instant(
      'pivotChartRequiresPivotMode'
    ),
  };

     // tslint:disable-next-line: typedef
    changeLanguage(val: string) {
      //  console.log(val);
       localStorage.setItem('language', val);
       this.translate.use(val);
       const htmlDocument =  document.getElementsByTagName('html')[0];
       htmlDocument.setAttribute('lang', val);
       if (val === 'en'){
         htmlDocument.setAttribute('dir', 'ltr');
        //  htmlDocument.dir = 'ltr';
        }else{
          htmlDocument.setAttribute('dir', 'rtl');
          // htmlDocument.dir = 'rtl';

       }
       return true;
    }

  switchLanguage(language: string, key?: string) {
    if (language in Lang) {
      if (this.currentLang != language) {
        this.translate.use(language);
        this.language = of(language);
        localStorage.setItem('language', language);
        this.setTitle(key || 'Al Eamar');
        this.setStyle();
        console.log(this.PagesComponent);

        // this.PagesComponent.changeMenu()
        // window.location.reload();
      }
    } else {
      this.translate.use('en');
    }
  }

  inverseLang(keyLang) {
    return keyLang === 'en' ? 'ar' : 'en';
  }
  // Title
  setTitle(key: string) {
    return this.translate.get(key).subscribe((data) => {
      this.titleService.setTitle(data);
    });
  }

  // Style
  setStyle() {
    const keyLang = this.currentLang || this.translate.getDefaultLang();
    const styleFiles = Styles[keyLang];
    const htmlElement = document.getElementsByTagName('html')[0];
    const headElements = document.getElementsByTagName('head')[0];
    // const paras = document.getElementsByClassName(this.inverseLang(keyLang));
    // if (paras) {
    //   while (paras[0]) {
    //     paras[0].parentNode.removeChild(paras[0]);
    //   }
    // }
    htmlElement.setAttribute('lang', keyLang);
    if (keyLang === 'ar') {
      htmlElement.setAttribute('dir', 'rtl');
    } else {
      htmlElement.setAttribute('dir', 'ltr');
    }
    for (let i = 0; i < styleFiles.length; i++) {
      const node = document.createElement('link');
      node.setAttribute('rel', 'stylesheet');
      node.setAttribute('type', 'text/css');
      node.setAttribute('href', Styles[keyLang][i]);
      // node.setAttribute('class', keyLang);
      headElements.appendChild(node);
    }
  }
  isRTL(): boolean {
    let rtl = false;
    this.lang.subscribe(lang => {
       lang =='ar'? rtl=true : rtl=false;
    });
    return rtl;
  }
  validateEntityId(control: AbstractControl){
    return control.value?control.value.id != null ? null : { unTaken: true }:{ unTaken: false };
  }

  validateArrayofObjects(control: AbstractControl) {
    let valid = false;
    control.value.forEach(element => {
        element.id !=null ? valid = true : valid = false;
    });
    return { unTaken: valid };
  }
}
