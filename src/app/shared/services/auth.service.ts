import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { Router } from '@angular/router';
import { User } from './User';
import { switchMap } from 'rxjs/operators';
import { auth } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<User>;
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        // Logged in
        if (user) {
          return this.afs.doc<User>(`users/${user.email}`).valueChanges();
        } else {
          // Logged out
          return of(null);
        }
      })
    );
  }

  async googleSignin() {
    const provider = new auth.GoogleAuthProvider();
    await this.afAuth.auth.signInWithPopup(provider).then(
      async res => {
        let user = res.user;
        // this is what you need
        let isNewUser = res.additionalUserInfo.isNewUser;
        if (isNewUser) {
          // delete the created user
          res.user.delete();
        } else {
          // your sign in flow
          console.log('user ' + user.email + ' does exist!');
          this.updateUserData(user);
        }
      }).catch(function (error) {
        console.error(error);
      }); ;

  }



  deleteUser(email){
    let user = this.afAuth.auth.fetchSignInMethodsForEmail(email).then ( res => {
      console.log(res);

    });

  }

  updateUserData(user, cred?: boolean) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<User> =
      this.afs.collection('users').doc(`${user.email}`);
    let data = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      enabled: true
    };

    console.log(data);

    userRef.set(data, { merge: true });

    this.router.navigate(['/']).then((re) => data);

  }

  async signOut() {
    await this.afAuth.auth.signOut();
    this.router.navigate(['/login']);
  }
  async facebookSignin() {
    const provider = new auth.FacebookAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    return this.updateUserData(credential.user);
  }

  async register(email: string, password: string) {
    const credential = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    return this.updateUserData(credential.user, true);

  }
  async login(email: string, password: string) {
    const credential = await this.afAuth.auth.signInWithEmailAndPassword(email, password).then(res => {
      return this.updateUserData(res.user, true);
    }, error => {
      alert('Email Or Passord invalid, please contact system administrator');
    });

  }
}
