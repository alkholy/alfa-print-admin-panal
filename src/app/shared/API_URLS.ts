import { environment as env } from "../../environments/environment";
/* This file should contain All EndPoint Routes Across all the application*/
/* Follow the Naming Convention*/

const server = env.serverUrl;
const contextURI = env.contextURI;
const uri = server + contextURI;
// const BaseUrl = environment.api + environment.apiLang + environment.apiVersion;
export const API_URLS = {
  auth: {
    auth: uri + "/auth/login",
    changePassword: uri + "/Auth/changePassword",
    resetPassword: uri + "/Auth/resetPassword",
    register: uri + "/register",
  },
  appsettings: {
    post: uri + "/appsettings",
    get: uri + "/appsettings",
    put: uri + "/appsettings/{entityId}",
    delete: uri + "/appsettings/{entityId}",
  },
  projectCategory: {
    post: uri + "/projectCategory",
    get: uri + "/projectCategory",
    getOne: uri + "/projectCategory/{entityId}",
    put: uri + "/projectCategory/{entityId}",
    delete: uri + "/projectCategory/{entityId}",
  },
  user: {
    post: uri + "/user",
    get: uri + "/user/all",
    getOne: uri + "/user/{entityId}",
    put: uri + "/user/{entityId}",
    delete: uri + "/user/{entityId}",
  },
  entity: {
    post: uri + "/entity",
    get: uri + "/entity/all",
    getOne: uri + "/entity/{entityId}",
    put: uri + "/entity/{entityId}",
    delete: uri + "/entity/{entityId}",
  },
  subscription: {
    post: uri + "/subscription",
    get: uri + "/subscription/all",
    getOne: uri + "/subscription/{entityId}",
    put: uri + "/subscription/{entityId}",
    delete: uri + "/subscription/{entityId}",
  },
  certificate: {
    post: uri + "/certificate",
    get: uri + "/certificate/all",
    getOne: uri + "/certificate/{entityId}",
    put: uri + "/certificate/{entityId}",
    delete: uri + "/certificate/{entityId}",
  },

  exam: {
    post: uri + "/Exam",
    postArr: uri + "/Exam/arr",
    get: uri + "/Exam/all",
    getOne: uri + "/Exam/{entityId}",
    run: uri + "/Exam/{entityId}",
    put: uri + "/Exam",
    putArr: uri + "/Exam/arr",
    delete: uri + "/Exam/{entityId}",
  },
  project: {
    post: uri + "/project",
    get: uri + "/project",
    getOne: uri + "/project/{entityId}",
    put: uri + "/project/{entityId}",
    delete: uri + "/project/{entityId}",
  },
  projectEngineer: {
    post: uri + "/projectEngineer",
    getOne: uri + "/projectEngineer/{entityId}",
    put: uri + "/projectEngineer/{entityId}",
    delete: uri + "/projectEngineer/{entityId}",
  },
  projectWorkStage: {
    post: uri + "/projectWorkStage",
    get: uri + "/projectWorkStage",
    getOne: uri + "/projectWorkStage/{entityId}",
    put: uri + "/projectWorkStage/{entityId}",
    delete: uri + "/projectWorkStage/{entityId}",
  },
  projectFile: {
    post: uri + "/projectfile",
    getOne: uri + "/projectfile/{entityId}",
    put: uri + "/projectfile/{entityId}",
    delete: uri + "/projectfile/{entityId}",
  }, projectWorkStageFile: {
    post: uri + "/projectWorkStageFile",
    get: uri + "/projectWorkStageFile",
    getOne: uri + "/projectWorkStageFile/{entityId}",
    put: uri + "/projectWorkStageFile/{entityId}",
    delete: uri + "/projectWorkStageFile/{entityId}",
  },
  workStage: {
    post: uri + "/workStage",
    get: uri + "/workStage",
    getOne: uri + "/workStage/{entityId}",
    put: uri + "/workStage/{entityId}",
    delete: uri + "/workStage/{entityId}",
  },
   suggestionAndCompliant: {
    get: uri + "/suggestionAndCompliant",
    getOne: uri + "/suggestionAndCompliant/{entityId}",
    delete: uri + "/suggestionAndCompliant/{entityId}",
  },
  projectFinances: {
    post:   uri + "/projectFinances",
    get:    uri + "/projectFinances",
    getOne: uri + "/projectFinances/{entityId}",
    delete: uri + "/projectFinances/{entityId}",
  },
  purchaseRequest: {
    post:   uri + "/purchaseRequest",
    get:    uri + "/purchaseRequest",
    getOne: uri + "/purchaseRequest/{entityId}",
    put: uri + "/purchaseRequest/{entityId}",
    delete: uri + "/purchaseRequest/{entityId}",
  },
  projectAttendance: {
    post:   uri + "/projectAttendance",
    get:    uri + "/projectAttendance",
    getOne: uri + "/projectAttendance/{entityId}",
    put:    uri + "/projectAttendance/{entityId}",
    delete: uri + "/projectAttendance/{entityId}",
  },
  paymentInvoice: {
    post:   uri + "/paymentInvoice",
    get:    uri + "/paymentInvoice",
    getOne: uri + "/paymentInvoice/{entityId}",
    delete: uri + "/paymentInvoice/{entityId}",
  },
  newCustomer: {
    post:   uri + "/newCustomer",
    get:    uri + "/newCustomer",
    getOne: uri + "/newCustomer/{entityId}",
    put:    uri + "/newCustomer/{entityId}",
    delete: uri + "/newCustomer/{entityId}",
  },
  userRating: {
    post:   uri + "/userRating",
    get:    uri + "/userRating",
    getOne: uri + "/userRating/{entityId}",
    put:    uri + "/userRating/{entityId}",
    delete: uri + "/userRating/{entityId}",
  },
  projectInstalments:{
    get: uri +"/projectInstalments"
  },
  appreports:{
    get:uri+ '/appreports/1'
  }
};
