import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgGridButtonRunReportComponent } from './ag-grid-button-run-report.component';

describe('AgGridButtonRunReportComponent', () => {
  let component: AgGridButtonRunReportComponent;
  let fixture: ComponentFixture<AgGridButtonRunReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgGridButtonRunReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridButtonRunReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
