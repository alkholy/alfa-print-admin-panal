import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { GridOptions, GridApi } from 'ag-grid-community';

@Component({
  selector: 'ngx-ag-grid-button-run-report',
  templateUrl: './ag-grid-button-run-report.component.html',
  styleUrls: ['./ag-grid-button-run-report.component.scss']
})
export class AgGridButtonRunReportComponent implements ICellRendererAngularComp {
  params;
  girdApi:GridApi;
  cell: { row: any; col: any };

  id;
  agInit(params: any): void {
    this.params = params;
    this.girdApi = params.api
    // console.log(this.girdApi.getDisplayedRowAtIndex(params.node.rowIndex) != undefined ? this.girdApi.getDisplayedRowAtIndex(params.node.rowIndex).data : null);
    // console.log(params.node)
    this.cell = { row: this.girdApi.getDisplayedRowAtIndex(params.node.rowIndex) != undefined ? this.girdApi.getDisplayedRowAtIndex(params.node.rowIndex).data : null, col: params.colDef.field };

  }

  refresh(params?: any): boolean {

    return true
  }
  onClick($event) {
    // console.log($event);

    // if (this.params.onClick instanceof Function) {
    //   // put anything into params u want pass into parents component
    //   const params = {
    //     event: $event,
    //     rowData: this.params.node
    //     // ...something
    //   }
    //   this.params.onClick(params);

    // }
  }

}
