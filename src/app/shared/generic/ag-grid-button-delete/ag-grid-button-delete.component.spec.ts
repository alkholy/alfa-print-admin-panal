import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgGridButtonDeleteComponent } from './ag-grid-button-delete.component';

describe('AgGridButtonDeleteComponent', () => {
  let component: AgGridButtonDeleteComponent;
  let fixture: ComponentFixture<AgGridButtonDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgGridButtonDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridButtonDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
