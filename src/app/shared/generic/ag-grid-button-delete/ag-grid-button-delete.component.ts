import { Component, OnInit, AfterViewInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ICellEditorAngularComp, ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-ag-grid-button-delete',
  templateUrl: './ag-grid-button-delete.component.html',
  styleUrls: ['./ag-grid-button-delete.component.scss']
})
export class AgGridButtonDeleteComponent implements ICellRendererAngularComp {
params: any;
getLabelFunction: any;

agInit(params: any): void {
  this.params = params;
  this.getLabelFunction = this.params.getLabelFunction;


}

refresh(params?: any): boolean {
  return true;
}
isPopup(): boolean {
  return false;
}
onClick($event) {
  if (this.params.onClick instanceof Function) {
    // put anything into params u want pass into parents component
    const params = {
      event: $event,
      rowData: this.params.node
      // ...something
    }
    this.params.onClick(params);

  }
}
}
