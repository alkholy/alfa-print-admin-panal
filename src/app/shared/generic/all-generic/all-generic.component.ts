import { FirebaseService } from './../../services/firebase.service';
import {
  Component,
  OnInit,
  Input,
  ViewChild,
  TemplateRef,
} from '@angular/core';
import { EntityService } from '../../services/entity.service';
import { API_URLS } from '../../API_URLS';
import { TranslateService } from '@ngx-translate/core';
import { entityData } from '../entityData';
import { AgGridButtonDeleteComponent } from '../ag-grid-button-delete/ag-grid-button-delete.component';
import { GridApi, ColDef } from 'ag-grid-community';
import { IdRenderComponent } from '../id-render/id-render.component';
import { AgGridButtonApproveComponent } from '../ag-grid-button-approve/ag-grid-button-approve.component';
import { OverrideService } from '../../services/override.service';
import { NbWindowRef, NbWindowService, NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';
import { ExcelService } from '../../services/excel.service';
import { AgGridResetPasswordComponent } from '../ag-grid-reset-password/ag-grid-reset-password.component';
import { ActivatedRoute } from '@angular/router';
// import { AllModules } from "@ag-grid-enterprise/all-modules";

@Component({
  selector: 'app-all-generic',
  templateUrl: './all-generic.component.html',
  styleUrls: ['./all-generic.component.scss'],
})
export class AllGenericComponent implements OnInit {
  // public modules = AllModules;

  @Input() entityData: entityData;
  @Input() receivedData: any;
  sideBar = {
    toolPanels: [
      {
        id: 'columns',
        labelDefault: this.translate.instant('Columns'),
        labelKey: 'columns',
        iconKey: 'columns',
        toolPanel: 'agColumnsToolPanel',
      },
      {
        id: 'filters',
        labelDefault: this.translate.instant('filter'),
        labelKey: 'filters',
        iconKey: 'filter',
        toolPanel: 'agFiltersToolPanel',
      },
    ],
    defaultToolPanel: '',
    position: 'right',
  };
  frameworkComponents = {
    deleteRender: AgGridButtonDeleteComponent,
    IdRender: IdRenderComponent,
    approve: AgGridButtonApproveComponent,
    resetPasswordRenderer: AgGridResetPasswordComponent,
  };
  rowData = [];
  gridApi: GridApi;
  gridColumnApi: any;
  paginationPageSize;
  event: any;
  fileName: any;

  constructor(
    private service: EntityService,
    private translate: TranslateService,
    public override: OverrideService,
    private windowService: NbWindowService,
    private excel: ExcelService,
    private toastrService: NbToastrService,
    private firestore: FirebaseService,
    private activeRoute: ActivatedRoute,
  ) { }

  get class() {
    var theme = localStorage.getItem('currentTheme');
    switch (theme) {
      case 'cosmic':
        return 'ag-theme-alpine-dark';
      case 'corporate':
        return 'ag-theme-alpine';
      default:
        return 'ag-theme-alpine';
    }
  }
  async ngOnInit(): Promise<void> {
    this.paginationPageSize = 100;

    setTimeout(() => {
      if (this.entityData.delete != false) {
        this.entityData.all.columnDefs.push({
          headerName: this.translate.instant('delete'),
          cellRenderer: 'deleteRender',
          editable: false,
          suppressMenu: true,
          cellRendererParams: {
            onClick: this.onDeleteClick.bind(this),
          },
        });
      }
      this.gridApi.setColumnDefs(this.entityData.all.columnDefs);
      this.gridApi.sizeColumnsToFit();
    }, 200);
    this.getData();
  }
  firstTimeData: boolean = true;
  lastId;
  getData() {
    if (this.entityData.getURI) {
      this.service.getAll(this.entityData.getURI).then(res => {
        res.subscribe(data => {
          // this.isArray(res) ? (this.rowData = data) : (this.rowData = [data]);
          this.gridApi.batchUpdateRowData({ add: [data[0]] });
        }
        );
      });
    }
    if (this.entityData.name && !this.receivedData) {

      if (!this.entityData.firestore) {
        if (this.entityData.paginated) {
          this.service
            .getAll(API_URLS[`${this.entityData.name}`]["get"], this.lastId)
            .then((res) => {
              res.subscribe((res) => {
                this.isArray(res) ? (this.rowData = res) : (this.rowData = [res]);
                this.gridApi.updateRowData({ add: [res] });
              });
            });
        } else {
          this.service
            .getAll(API_URLS[`${this.entityData.name}`]["get"])
            .then((res) => {
              res.subscribe((res) => {
                this.isArray(res) ? (this.rowData = res) : (this.rowData = [res]);
                this.gridApi.updateRowData({ add: [res] });
              });
            });
        }
      } else {
        this.firestore.get(this.entityData.name).subscribe(res => {
          let data = res.map(e => {
            return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
          });
          console.log(data);

          this.rowData = data;
        });
      }
    } else if (this.receivedData.length > 0) {
      this.rowData = this.receivedData;
    }
  }

  columnDefs: ColDef[] = [];
  entityId;

  selectedRows;
  defaultColDef = {
    editable: true,
    enableRowGroup: true,
    enablePivot: true,
    enableValue: true,
    sortable: true,
    resizable: true,
    filter: true,
    enableCellChangeFlash: true,
  };
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  cellEditingStopped(event) {
    const data = event.data;
    event.colDef.type == 'number'
      ? (event.data[event.colDef.field] = +event.data[event.colDef.field])
      : event.data[event.colDef.field];
    // console.log(event.data,event.colDef) ;
    if (API_URLS[`${this.entityData.name}`]['put']) {
      this.service
        .update(API_URLS[`${this.entityData.name}`]['put'], data, data.id)
        .then((res) => {
          res.subscribe(
            () => {
              this.gridApi.updateRowData({ update: [data] }).update;
            },
            (error) => {
              alert(this.translate.instant(error.error.error));
            }
          );
        });
    } else {
      this.service
        .save(API_URLS[`${this.entityData.name}`]['post'], data)
        .then((res) => {
          res.subscribe(
            () => {
              this.gridApi.updateRowData({ update: [data] }).update;
            },
            (error) => {
              alert(this.translate.instant(error.error.error));
            }
          );
        });
    }
  }
  onSelectionChanged() {
    this.selectedRows = this.gridApi.getSelectedRows();
  }

  rowSelected(event) {
    this.entityId = event.data.id;
    // console.log(this.entityId);
  }

  onDeleteClick(event) {
    const data = event.rowData.data;
    // console.log(data);

    if (confirm(this.translate.instant('sure?'))) {

      let route = this.activeRoute.parent.snapshot.pathFromRoot;
      let collection = route[this.activeRoute.parent.snapshot.pathFromRoot.length - 2].routeConfig.path;
      // console.log(collection);

      this.firestore.delete(data.id, collection);

      // .subscribe(resa => {
      //   console.log(resa);

      // })
      // this.firestore.delete(data.id, )
      // this.service
      //   .delete(API_URLS[`${this.entityData.name}`]['delete'], data, data.id)
      //   .then((res) => {
      //     res.subscribe(
      //       () => {
      //         this.gridApi.updateRowData({ remove: [data] });

      //         this.toastrService.show(
      //           this.translate.instant('record deleted successfully') +
      //           ` ${this.translate.instant('ID')} : (${data.id})`,
      //           this.translate.instant(`Success`),
      //           {
      //             status: 'success' as NbComponentStatus,
      //             destroyByClick: true,
      //             duration: 5000,
      //             hasIcon: false,
      //             position: this.override.isRTL()
      //               ? NbGlobalPhysicalPosition.TOP_LEFT
      //               : NbGlobalPhysicalPosition.TOP_RIGHT,
      //             preventDuplicates: false,
      //           }
      //         );
      //       },
      //       (error) => {
      //         this.toastrService.show(
      //           this.translate.instant(error.error),
      //           this.translate.instant(`Faild`),
      //           {
      //             status: 'danger' as NbComponentStatus,
      //             destroyByClick: true,
      //             duration: 5000,
      //             hasIcon: false,
      //             position: this.override.isRTL()
      //               ? NbGlobalPhysicalPosition.TOP_LEFT
      //               : NbGlobalPhysicalPosition.TOP_RIGHT,
      //             preventDuplicates: false,
      //           }
      //         );
      //         // alert(error.error)
      //       }
      //     );
      //   });
    }
  }

  isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
  }
  onPaginationChanged() {
    if (this.gridApi) {
      if (this.gridApi.paginationGetCurrentPage() > 0 && this.gridApi.paginationIsLastPageFound() && this.entityData.paginated) {

        if (this.entityData.paginatedOrientation === 'desc') {
          this.lastId = this.rowData.find(row => !!Math.max(row.id)).id;
        } else if (this.entityData.paginatedOrientation === 'asc') {
          this.lastId = this.rowData.find(row => !!Math.min(row.id)).id;
        }

        this.getData();
      }
    }
  }
  checkInArray(obj, arr: any[]): boolean {
    for (let i = 0; i < arr.length; i++) {
      const element = arr[i];
      if (element['id'] == obj['id']) {
        return true;
      }
    }
    return false;
  }
  windowRef: NbWindowRef;

  @ViewChild('contentTemplate', { static: false }) contentTemplate: TemplateRef<
    any
  >;
  openWindow() {
    this.windowRef = this.windowService.open(this.contentTemplate, {
      title: this.translate.instant('upload'),
      closeOnEsc: true,
      hasBackdrop: true,
      windowClass: 'danger',
    });
  }
  async uploadFile(event) {
    var array;
    this.fileName = event.target.files[0].name;
    await this.excel.fromExcelToJson(event);
    setTimeout(() => {
      this.windowRef.close();
      array = this.excel.json;
      let URI = this.entityData.excelURI
        ? this.entityData.excelURI
        : API_URLS[`${this.entityData.name}`]['postArr'];
      this.service
        .save(URI, array)
        .then((res) => {
          res.subscribe((data) => {
            this.gridApi.updateRowData({ add: [data] }).update;
            this.service.showSuccessToast('Uploaded Successfully');
          });
        })
        .catch((error) => {
          this.service.showErrorToast(this.translate.instant(error));
        });
    }, 500);
  }
  searchValue;

  filter() {
    this.gridApi.setQuickFilter(this.searchValue);
  }
}

