import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGenericComponent } from './all-generic.component';

describe('AllGenericComponent', () => {
  let component: AllGenericComponent;
  let fixture: ComponentFixture<AllGenericComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllGenericComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGenericComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
