import { Component, OnInit } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { ICellEditorParams, IAfterGuiAttachedParams, GridApi } from 'ag-grid-community';

@Component({
  selector: 'ngx-number-renderer',
  templateUrl: './number-renderer.component.html',
  styleUrls: ['./number-renderer.component.scss']
})
export class NumberRendererComponent implements ICellEditorAngularComp {

  number = 0;
  name = "";
  params: ICellEditorParams;
  gridApi: GridApi; 
  constructor() { }
  getValue() {
    return this.number 
  }
  isPopup?(): boolean {
    return false;
  }

  agInit(params: ICellEditorParams): void {
    this.params = params;
    this.gridApi = params.api;
  }



}
