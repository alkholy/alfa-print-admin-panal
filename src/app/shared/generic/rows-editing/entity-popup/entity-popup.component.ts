import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { EntityService } from '../../../services/entity.service';
import { OverrideService } from '../../../services/override.service';
import { ICellEditorParams, GridApi } from 'ag-grid-community';
import { API_URLS } from '../../../API_URLS';
import { TranslateService } from '@ngx-translate/core';
import { NbWindowService, NbWindowRef } from '@nebular/theme';

@Component({
  selector: 'ngx-entity-popup',
  templateUrl: './entity-popup.component.html',
  styleUrls: ['./entity-popup.component.scss']
})
export class EntityPopupComponent implements   ICellEditorAngularComp  {
  params: any;
  gridApi: any;
  cell: { row: any; col: string; };
  name: any;
  data: any;
  selected: any = {id: null,name1:null,name2:null};

  constructor   ( private entity: EntityService,
  private translate: TranslateService,
  private windowService: NbWindowService,
  public override: OverrideService) {}
  getValue() {
    return this.selected;
  }
  isPopup?(): boolean {
    return true;
  } 
 
  agInit(params: ICellEditorParams): void {
    this.params = params;
    this.gridApi = params.api;
    this.cell = {
      row:
        this.gridApi.getDisplayedRowAtIndex(params.node.rowIndex) != undefined
          ? this.gridApi.getDisplayedRowAtIndex(params.node.rowIndex).data
          : null,
      col: params.colDef.field,
    };

    this.name = this.params.colDef.name;
    let from = this.params.colDef.from;
    this.entity.getAll(API_URLS[from]["get"]).then((res) => {
      res.subscribe((data) => {
        this.rowData = data;
        this.openWindow(this.contentTemplate)

      });
    });
  }
  columnDefs = [
    {
      headerName: this.translate.instant("id"),
      field: "id",
      checkboxSelection: true,
      filter: true,
      sortable: true,
      editable: false,
    },
    {
      headerName: this.translate.instant("name1"),
      field: "name1",
      filter: true,
      sortable: true,
      editable: false,
    },
    {
      headerName: this.translate.instant("name2"),
      field: "name2",
      filter: true,
      sortable: true,
      editable: false,
    },
  ] 
  entityId: any;
  selectedRow: any;
  fieldVal = null;
  multipleValue = null;
  i = 0;
  rowData = []
  windowRef: NbWindowRef;
 
  selectGridApi: GridApi
  rowSelection = "single"; 
  gridColumnApi: any;
  frameworkComponents;
  selectedRows;
  onGridReady(params) {
    this.selectGridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  onSelectionChanged(event) {
    this.selectedRows = this.selectGridApi.getSelectedRows();
  }

  rowSelected(event) {
    this.selectedRow = event.data;
  }

  selectRow() {
    this.windowRef.close();    
   this.selected = this.selectGridApi.getSelectedRows()[0]
  }

  @ViewChild("contentTemplate", { static: false }) contentTemplate: TemplateRef<any>;
  openWindow(contentTemplate) {
    this.windowRef = this.windowService.open(this.contentTemplate, {
      title: "Select " ,
      closeOnEsc: true,
      hasBackdrop: true,
      windowClass: "danger",
    });
  }
  searchValue ;

  filter(){
    this.gridApi.setQuickFilter(this.searchValue)
    }

}
