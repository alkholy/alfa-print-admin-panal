import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ICellRendererAngularComp, INoRowsOverlayAngularComp, ICellEditorAngularComp } from "ag-grid-angular";
import {
  ICellRendererParams,
  IAfterGuiAttachedParams,
  GridApi,
} from "ag-grid-community";
import { EntityService } from "../../../services/entity.service";
import { API_URLS } from "../../../API_URLS";
import { OverrideService } from '../../../services/override.service';

@Component({
  selector: "ngx-select-entity",
  templateUrl: "./select-entity.component.html",
  styleUrls: ["./select-entity.component.scss"],
})
export class SelectEntityComponent implements   ICellEditorAngularComp  {
  params: any;
  gridApi: GridApi;
  cell: { row: any; col: string };
  name: string;

  constructor(private entity: EntityService,public override:OverrideService) {}

  agInit(params: any): void {
    this.params = params;
    this.gridApi = params.api;
    this.cell = {
      row:
        this.gridApi.getDisplayedRowAtIndex(params.node.rowIndex) != undefined
          ? this.gridApi.getDisplayedRowAtIndex(params.node.rowIndex).data
          : null,
      col: params.colDef.field,
    };

    this.name = this.params.colDef.name;
    let from = this.params.colDef.from;
    this.entity.getAll(API_URLS[from]["get"]).then((res) => {
      res.subscribe((data) => {
        this.data = data;
      });
    });
  }
  getValue() {    
return this.cell.row[this.cell.col];
  }
  
  
  data = [];
 
  isPopup(): boolean {
    return false;
  }


  selectedValue = null;
  selectedChange(event) {
    let data = this.data.find(d => d['id'] == event)
    this.cell.row[this.cell.col] = data
    this.gridApi.updateRowData({ update: [this.cell.row] }).update;
  }
}
