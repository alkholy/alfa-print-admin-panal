import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { GridApi, ColDef } from "ag-grid-community";
import { OverrideService } from "../../services/override.service";
import { rowsData } from "../rowsData";
import { SelectEntityComponent } from '../rows-editing/select-entity/select-entity.component';
import { IdRenderComponent } from '../id-render/id-render.component';
import { NumberRendererComponent } from '../rows-editing/number-renderer/number-renderer.component';
import { EntityPopupComponent } from '../rows-editing/entity-popup/entity-popup.component';

@Component({
  selector: "rows",
  templateUrl: "./rows.component.html",
  styleUrls: ["./rows.component.scss"],
})
export class RowsComponent implements OnInit {
  @Input() lines: rowsData;
  @Output() data = new EventEmitter();
  constructor(public override: OverrideService) {}

  frameworkComponents = {
    selectEntity: SelectEntityComponent,
    IdRender: IdRenderComponent,
    number:NumberRendererComponent,
    popup:EntityPopupComponent

  };
  get class(){
    var theme = localStorage.getItem('currentTheme')
    switch (theme) {
      case 'dark':
        return "ag-theme-alpine-dark"
        case 'cosmic':
          return "ag-theme-alpine-dark"
      default:
        return "ag-theme-alpine"
    }
  }
  ngOnInit() {
    this.columnDefs = this.lines.cols;
    this.lines.cols.forEach(col =>{
      let name = col.field
      if (name != 'id')
      this.rowData[0][name] = null
    })    
  }
  columnDefs: ColDef[] = [];
  searchValue: any;
  gridColumnApi: any;
  defaultColDef = {
    editable: false,
    enableRowGroup: true,
    enablePivot: false,
    enableValue: true,
    sortable: true,
    resizable: true,
    filter: true,
    enableCellChangeFlash: false,
  };
  gridOptions ={
    enableColResize: true,
    editType: 'fullRow',
    enableFilter: true,
    singleClickEdit: true,
    domLayout: 'autoHeight',
    onGridReady: function (params) {
        params.api.sizeColumnsToFit();
    }
  }
  rowData = [
  {
    id : 1
  }
  
  ];

  gridApi: GridApi;

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }
  cellkeyEvent(evt) {
   
    if (evt.event.key === "Enter" && evt.event.ctrlKey ) {
      console.log(evt);
      
      var currentCell = this.gridApi.getFocusedCell();
      var finalRowIndex = this.gridApi.paginationGetRowCount() ;
      this.gridApi.updateRowData({add : [{
        id: finalRowIndex +1
      }]})
      let colData = currentCell.column.getUserProvidedColDef() as any  ;
      // If we are editing the last row in the grid, don't move to next line
    
    }
    // console.log(event);
  }
}
