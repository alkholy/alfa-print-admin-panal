import { Component, OnInit, DoCheck } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { EntityService } from "../../services/entity.service";
import { API_URLS } from "../../API_URLS";
import { OverrideService } from "../../services/override.service";
import { ICellRendererParams, GridApi } from 'ag-grid-community';

@Component({
  selector: "app-id-render",
  templateUrl: "./id-render.component.html",
  styleUrls: ["./id-render.component.scss"]
})
export class IdRenderComponent implements ICellRendererAngularComp  {
  public cell: any;
  public id: any;
  public text: any;
  gridApi: GridApi;
  async agInit(params: ICellRendererParams) {
      this.gridApi = params.api;

    this.cell = { row: params.value, col: params.colDef.field };
  
    this.text = params.data[this.cell.col]
    this.cell.row != undefined  ? ( this.text = this.cell.row.name   ?  this.cell.row.name[this.override.currentLang]  : "") : (this.text = "");
  }
 

  constructor(
    private entity: EntityService,
    public override: OverrideService
  ) {}

  refresh(params?: any): boolean {
    this.text = this.override.isRTL()?(params.data?params.data[this.cell.col]['name1']:"" ): (params.data?params.data[this.cell.col]['name2']:"") ;
        return true;
  }
  isPopup(): boolean {
    return false;
  }
}
