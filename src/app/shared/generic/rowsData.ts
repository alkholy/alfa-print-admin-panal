import { FormGroup } from '@angular/forms';
import { ColDef } from 'ag-grid-community';

export interface rowsData{
    cols?: any[],
    data?:any,
    name?: string;
    path?:string;
}