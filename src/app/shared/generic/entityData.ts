import { ColDef } from 'ag-grid-community';
export interface entityData {
  name?: string;
  path?: string;
  label?: string;
  all?: {
    columnDefs: any[];
    controlSelector?: string;
  };
  pivotMode?: boolean;
  edit: boolean;
  view: boolean;
  add: boolean;
  upload?: boolean;
  delete?: boolean;
  data?: any;
  getURI?: string;
  excelURI?: string;
  paginated?: boolean;
  firestore?: boolean;
  paginatedOrientation?: 'asc'|'desc';
  noCard?: boolean;
}
