import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgGridResetPasswordComponent } from './ag-grid-reset-password.component';

describe('AgGridResetPasswordComponent', () => {
  let component: AgGridResetPasswordComponent;
  let fixture: ComponentFixture<AgGridResetPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgGridResetPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
