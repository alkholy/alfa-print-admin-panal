import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { OverrideService } from '../../../services/override.service';

@Component({
  selector: 'field-builder',
  templateUrl: './field-builder.component.html',
  styleUrls: ['./field-builder.component.scss']
})
export class FieldBuilderComponent implements OnInit {
  @Input() field: any;
  @Input() isArray: boolean;
  @Input() form: FormGroup;
  @Input() isEditMode: boolean;
  @Input() entityId: any;
  @Output() formValue = new EventEmitter();

  get isValid() {
    // if (! this.field.fromArray) {
    //   if (this.field.type == 'text-localized') {
    //     return this.form.controls[this.field.name].get('ar').valid && this.form.controls[this.field.name].get('en').valid;
    //   }
    //   return this.form.controls[this.field.name].valid;
    // }

    return true;

  }
  get isDirty() {
    // if (!this.field.fromArray) {
    //   if (this.field.type == 'text-localized') {
    //     return this.form.controls[this.field.name].get('ar').dirty && this.form.controls[this.field.name].get('en').valid;
    //   }
    //   return this.form.controls[this.field.name].dirty;

    // }
    return true;
  }

  constructor(public override: OverrideService) {}
  receiveFormValue(event){
    this.formValue.emit(event);

  }

  ngOnInit() {

  }
}
