import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { OverrideService } from '../../../../services/override.service';
import { EntityService } from '../../../../services/entity.service';
import { API_URLS } from '../../../../API_URLS';

@Component({
  selector: 'dropdown-entity',
  templateUrl: './dropdown-entity.component.html',
  styleUrls: ['./dropdown-entity.component.scss']
})
export class DropdownEntityComponent implements OnInit {


  @Input() field:any = {};
  @Input() form:FormGroup;
  @Input() isEditMode:boolean;
  @Output() formValue=new EventEmitter();
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }
  constructor(public override:OverrideService, private dataService:EntityService) { }
  options:any[];
  ngOnInit(): void {
    if (! this.isEditMode) {
      this.getAllRowsForEntity();
    } else{
      this.options = this.field.value;
    }
  }
  getAllRowsForEntity() {
    var getUrl = this.field.from?API_URLS[this.field.from]["get"]:API_URLS[this.field.name]["get"];
    this.dataService.getAll(getUrl).then(res => {
      res.subscribe(data => {
        this.options = data;        
      });
    });
  }
  selectedValue = null;
  selectedChange(event){
    this.form.get(this.field.name).setValue(+event);
    this.formValue.emit(this.form.value)
    this.selectedValue = this.options.find(value => value.id = event).id    
  }

}
