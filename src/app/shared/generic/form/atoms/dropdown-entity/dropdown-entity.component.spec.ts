import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownEntityComponent } from './dropdown-entity.component';

describe('DropdownEntityComponent', () => {
  let component: DropdownEntityComponent;
  let fixture: ComponentFixture<DropdownEntityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownEntityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownEntityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
