import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  DoCheck,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormArray,
  Validators,
  FormControl,
} from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.scss'],
})
export class ArrayComponent implements DoCheck {
  @Input() field: any = {};
  @Input() form: FormGroup;
  @Input() isEditMode: boolean;
  @Output() formValue = new EventEmitter();
  constructor(private fb: FormBuilder, private datepipe:DatePipe) {}

  i = 0;
  ngDoCheck(): void {
    if (this.form.value[this.field.name] != undefined){

    if (
      this.isEditMode &&
      this.form.value ? this.form.value[this.field.name].length : 1 > 1 &&
      this.i === 0
    ) {
      let newValues = this.form.value[this.field.name] as any[];

      if (this.Forms.length < newValues.length) {
        newValues.forEach((element) => {
          const fieldsCtrls = {};
          for (const key in element) {
            if (element.hasOwnProperty(key)) {
              const prop = element[key];
              for (let f of this.field?.children) {
                if (f.name == key) {
                  if (f.type == 'time') {
                    let time = prop.substring(0, 5);
                    fieldsCtrls[f.name] = new FormControl(
                      time,
                      f.validators ? f.validators : []
                    );
                    f.value = time;
                  } else if (f.type == 'weekDays') {
                    let val = prop;
                    f['value'] = val;
                    fieldsCtrls[f.name] = new FormControl(
                      val,
                      f.validators ? f.validators : []
                    );
                  } else if (f.type == 'date') {
                    let val = prop;
                    f['value'] = new Date(val.seconds) ;
                    // console.log(new Date(val.seconds));
                    console.log(val);


                    fieldsCtrls[f.name] = new FormControl(
                      new Date(val.seconds),
                      f.validators ? f.validators : []
                    );
                  }else {
                    f.value = prop;
                    fieldsCtrls[f.name] = new FormControl(
                      prop,
                      f.validators ? f.validators : []
                    );
                  }
                }
              }
            }
          }

          const newGroup = this.fb.group(fieldsCtrls);
          this.Forms.push(newGroup);
        });
      }
      if (this.Forms.length > newValues.length) {
        let val = this.Forms.value as [];
        const index = val.findIndex((a) => !newValues.includes(a));
        this.Forms.removeAt(index);
        this.Forms.removeAt(0);
      }
      this.i++;
    }
  }
  }

  get Forms() {
    return this.form.controls[this.field.name] as FormArray;
  }

  addRow() {
    const fieldsCtrls = {};
    for (let f of this.field.children) {
      if (f.type !== 'entity') {
        fieldsCtrls[f.name] = new FormControl(
         { value: null || '',
          disabled: f.disabled},
          f.validators ? f.validators : []
        );
      } else if (f.type == 'entity') {
        fieldsCtrls[f.name] = new FormControl(
          {   },
          f.validators ? f.validators : []
        );
      }
    }
    const newGroup = this.fb.group(fieldsCtrls);
    console.log(newGroup);

    this.Forms.push(newGroup);
  }
  removeRow(i) {
    this.Forms.removeAt(i);
  }

  get isValid() {
    return this.form.controls[this.field.name].valid;
  }
  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }

  receiveFormValue(event) {
    this.formValue.emit(event);
  }
}
