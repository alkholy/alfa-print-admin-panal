import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { OverrideService } from '../../../../services/override.service';

@Component({
  selector: 'editor-localized',
  templateUrl: './editor-localized.component.html',
  styleUrls: ['./editor-localized.component.scss']
})
export class EditorLocalizedComponent implements OnInit {
  public Editor = ClassicEditor;

  @Input() field:any = {};
  @Input() isArray: boolean;
  @Input() form:FormGroup;
  @Input() isEditMode:boolean;
  @Output() formValue=new EventEmitter();
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }

  date = Date.now()

  constructor(public override:OverrideService) {
  }

  ngOnInit(): void {
  }

}
