import { Component, OnInit, Input, Output, EventEmitter, Renderer2, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NbDateService } from '@nebular/theme';
// import { NgxMaterialTimepickerService } from ظ'ngx-material-timepicker/src/app/material-timepicker/services/ngx-material-timepicker.service';

@Component({
  selector: 'time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss']
})
export class TimeComponent implements OnInit {

  @Input() field:any = {};
  @Input() form:FormGroup;
  @Input() isArray: boolean = false;
  @Input() isEditMode:boolean;
  @Output() formValue=new EventEmitter();
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }
  constructor(private dateService: NbDateService<Date>) { }
  date = Date.now()

  ngOnInit() {    
  }



}
 