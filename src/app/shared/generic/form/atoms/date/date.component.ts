import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NbDateService } from '@nebular/theme';

@Component({
  selector: 'date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent implements OnInit {

  @Input() field: any = {};
  @Input() form: FormGroup;
  @Input() isEditMode: boolean;
  @Output() formValue = new EventEmitter();
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }
  constructor(protected dateService: NbDateService<Date>) { }

  ngOnInit() {
    if (this.isEditMode){
      console.log('date', this.form.get(this.field.name) );

    }
  }
  dateChange(event: Date){
    var date = event.toLocaleString();
    // console.log(event, 'data ' + date , 'field ' + this.field);

    this.form.controls[this.field.name].setValue(event);
    this.formValue.emit(this.form.value);
  }

}
