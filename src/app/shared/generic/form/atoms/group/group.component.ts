import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  @Input() field:any = {};
  @Input() form:FormGroup;
  @Input() isEditMode:boolean;
  @Output() formValue=new EventEmitter();
  outputForm:any;
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }
  constructor(private fb:FormBuilder) { }
  ngOnInit(): void {
    this.outputForm = this.form.controls[this.field.name]
  }


}
