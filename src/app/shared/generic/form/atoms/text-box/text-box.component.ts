import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'textbox',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss']
})
export class TextBoxComponent  implements OnInit  {
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }

  @Input() field: any = {};
  @Input() isArray: boolean;
  @Input() form: FormGroup;
  @Input() isEditMode: boolean;
  @Output() formValue = new EventEmitter();

  constructor() { }
  ngOnInit(): void {
  }
  date = Date.now();

  change() {
    let formVal = this.form.value[this.field.name];
    let contName = this.form.controls[this.field.name];
    contName.setValue(formVal);
  }
}
