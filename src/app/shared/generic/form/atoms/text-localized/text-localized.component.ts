import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Lang } from '../../../../lang.enum';
import { OverrideService } from '../../../../services/override.service';

@Component({
  selector: "text-localized",
  templateUrl: "./text-localized.component.html",
  styleUrls: ["./text-localized.component.scss"],
})
export class TextLocalizedComponent implements OnInit {
  @Input() field: any = {};
  @Input() form: FormGroup;
  @Input() isArray: boolean;
  @Input() isEditMode: boolean;
  @Output() formValue = new EventEmitter();
  date = Date.now();
  lang = ["ar", "en"];
  get isValid() {
    console.log("valid from text-localized!");
    return this.form.controls[this.field.name].valid;
  }
  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }
  constructor(public override: OverrideService) {}
  ngOnInit(): void {
    // console.log(this.form);
  }
}
