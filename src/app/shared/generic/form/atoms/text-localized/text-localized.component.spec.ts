import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextLocalizedComponent } from './text-localized.component';

describe('TextLocalizedComponent', () => {
  let component: TextLocalizedComponent;
  let fixture: ComponentFixture<TextLocalizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextLocalizedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextLocalizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
