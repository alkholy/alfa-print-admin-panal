import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import {
  AngularFireUploadTask,
  AngularFireStorage
} from '@angular/fire/storage';
import { NbIconConfig } from '@nebular/theme';

@Component({
  selector: 'file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
})
export class FileComponent implements OnInit{
  @Input()
  field: any = {};
  @Input() disabled;
  @Input()
  form: FormGroup;
  @Input()
  isEditMode: boolean;
  get isValid() {
    return this.form.controls[this.field.name].valid;
  }
  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }
  isHovering;
  constructor(private storage: AngularFireStorage) { }
  ngOnInit(): void {
  }

  toggleHover($event) {
    console.log($event);
  }
  // tslint:disable-next-line: member-ordering
  task: AngularFireUploadTask;
  // tslint:disable-next-line: member-ordering
  percentage: Observable<number> = new Observable<number>();
  // tslint:disable-next-line: member-ordering
  downloadURL: string;
  // tslint:disable-next-line: member-ordering
  fileType = '';
  async uploadFile(event) {
    const file = event.target.files[0];
    let type = file.type as string;

    let TypeToSave = checkFileType(type);
    this.fileType = TypeToSave;
    if (this.form.get('fileType')) {
      this.form.get('fileType').setValue(TypeToSave);
    }

    // The storage path
    const path = `${this.field.name}/${Date.now()}_${file.name}`;
    // Reference to storage bucket
    const ref = this.storage.ref(path);
    // The main task
    const customMetadata = { app: 'alfaprint' };
    this.task = this.storage.upload(path, file, { customMetadata });
    // Progress monitoring
    this.percentage = this.task.percentageChanges();
    this.downloadURL = await (await this.task).ref.getDownloadURL();
    console.log(this.form, ' name ' + this.field.name);

    this.form.controls[this.field.name].setValue(
      await (await this.task).ref.getDownloadURL()
    );
  }
  deleteFile(url: any) {
    this.storage.storage
      .refFromURL(url)
      .delete()
      .then((res) => {
        this.form.controls[this.field.name].setValue(null);
      });
  }
  trashIcon: NbIconConfig = {
    icon: 'trash',
    status: 'danger'
  };
  viewIcon: NbIconConfig = {
    icon: 'eye-outline',
    status: 'primary'
  };
}
function checkFileType(type: String) {
  let TypeToSave = '';
  if (type.indexOf('image') >= 0) TypeToSave = 'image';
  if (type.indexOf('pdf') >= 0) TypeToSave = 'pdf';
  if (type.indexOf('video') >= 0) TypeToSave = 'video';
  if (
    type.indexOf('STL') >= 0 ||
    type.indexOf('OBJ') >= 0 ||
    type.indexOf('FBX') >= 0 ||
    type.indexOf('COLLADA') >= 0 ||
    type.indexOf('3DS') >= 0 ||
    type.indexOf('IGES') >= 0
  )
    TypeToSave = 'threeDimensionImage';

  return TypeToSave;
}
