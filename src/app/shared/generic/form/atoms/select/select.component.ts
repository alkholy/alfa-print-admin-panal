import {
  Component,
  OnInit,
  Input,
  DoCheck,
  Output,
  EventEmitter,
  ViewChild,
  TemplateRef,
  Inject,
  ElementRef,
  Renderer2,
} from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { GridApi } from "ag-grid-community";
import { TranslateService } from "@ngx-translate/core";
import { API_URLS } from "../../../../API_URLS";
import { EntityService } from "../../../../services/entity.service";
import { OverrideService } from "../../../../services/override.service";
import { NbWindowService, NbWindowRef } from "@nebular/theme";
import { TagContentType } from '@angular/compiler';

@Component({
  selector: "entity",
  templateUrl: "./select.component.html",
})
export class SelectComponent implements OnInit,  DoCheck {
  @Input() field: any = {};
  @Input() form: any;
  @Input() disabled: any;
  @Input() isEditMode: boolean;
  @Output() formValue = new EventEmitter();
  entityId: any;
  selectedRow: any;
  fieldVal = null;
  multipleValue = null;
  i = 0;
  columnDefs:any[] = [];
  ngDoCheck(): void {
    
    if (
      this.form.get(this.field.name) != undefined && this.isEditMode
    ) {      
      if (!this.field.multiple) {
        if (this.form.value[this.field.name])
        this.fieldVal = this.form.value[this.field.name][this.field.optionName];
      }
      if (this.field.multiple) {
        let value = this.form.value[this.field.name];
        let optionValue = [];        
        if (value)
          value.forEach((val) => {
            optionValue.push(
              val[this.field.optionName]
            );
          });
        this.multipleValue = optionValue.toString();
      }
    }
    this.i++
  }
  constructor(
    private dataService: EntityService,
    private translate: TranslateService,
    private windowService: NbWindowService,
    public override: OverrideService
  ) {}
  ngOnInit(): void { 
    // let val = this.form.get(this.field.name).value;
    // console.log(val);
    
    // if(val == null ||val == undefined || val == null || (val.id == null)  )
    // {
    //   this.fieldVal = null;
    // }
  }
  windowRef: NbWindowRef;

  getAllRowsForEntity() {
    var getUrl = this.field.from
      ? API_URLS[this.field.from]["get"]
      : API_URLS[this.field.name]["get"];

    
    if (this.field.pathParams != undefined) {
      let params = this.field.pathParams as [];
      
     for (let i = 0; i < params.length; i++) {
       const element = params[i];
       if (i==0)
         getUrl += "?" + element["key"] + "=" + element["value"];
       else 
                  getUrl += "&" + element["key"] + "=" + element["value"];

     }
    }
      this.dataService.getAll(getUrl).then((res) => {
        // res.subscribe((data) => {
        //   this.rowData = data;

        // });
        res.subscribe((data) => {
          if (this.field.subObjectName){
            let dataToMap = data as []
            data = [];
            dataToMap.forEach(element => {
              data.push(element[this.field.subObjectName])
            });
          }        
          
       
          var i = 0;
          var cloumns = [];
          let exclude = this.field.exludeView as [];
          let localized=this.field.localized as [];
          
          if (data.length>0 && ! this.field.columns)
           Object.keys(data[0]).forEach((col) => {
            if (exclude != undefined && arrayContains(col, exclude)) {
            } else {
              var column = {
                headerName: this.translate.instant(col),
                field: col,
                checkboxSelection: i === 0 ? true : false,
                filter: true,
                sortable: false,
                editable: false,
                cellRenderer: (params)=>{
                  let val = params.value;                 
                  if (localized != undefined && arrayContains(col, localized)){
                    return this.translate.instant(val)
                  }else {
                    return val;
                  }
                }
              };
              cloumns.push(column);
              i++;
            }
          });
          if (this.field.columns)
            this.field.columns.forEach(c => {
              var column = {
                headerName: c.view,
                field: c.col,
                checkboxSelection: i === 0 ? true : false,
                filter: true,
                sortable: false,
                editable: false,
              };
              cloumns.push(column);
              i++;
            });
          this.columnDefs = cloumns;
          setTimeout(() => {
            this.rowData = data;
          }, 200);
        });
      });
  }

  rowData: any;
  rowSelection;
  gridApi: GridApi;
  gridColumnApi: any;
  frameworkComponents;
  selectedRows;
  onGridReady(params) {
    this.field.multiple
      ? (this.rowSelection = "multiple")
      : (this.rowSelection = "single");
    this.getAllRowsForEntity();
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  onSelectionChanged(event) {
    this.selectedRows = this.gridApi.getSelectedRows();
  }

  rowSelected(event) {
    this.selectedRow = event.data;
  }

  get class() {
    var theme = localStorage.getItem("currentTheme");
    switch (theme) {
      case "dark":
        return "ag-theme-alpine-dark";
      case "cosmic":
        return "ag-theme-alpine-dark";
      default:
        return "ag-theme-alpine";
    }
  }
  
  selectRow() {
    this.windowRef.close();
    var selectedRow;
    // this.form.removeControl(this.field.name);
    if (!this.field.multiple) selectedRow = this.gridApi.getSelectedRows()[0];
    else selectedRow = this.gridApi.getSelectedRows();

    // this.fieldVal=this.form.value[this.field.name][this.field.optionName]

    var formVal = (this.form.value[this.field.name] = selectedRow);
    var contName = this.form.controls[this.field.name];
    contName.setValue(formVal);
    if (!this.field.multiple) {
      let optionName = this.field.optionName as String ;
      if (optionName && optionName.includes(".")){
        let firstOption = optionName.substr(0,optionName.indexOf("."))
        let secondOption = optionName.substr(optionName.indexOf(".")+1)
        
        this.fieldVal = this.form.value[this.field.name][firstOption][secondOption]
        
      }else {
        this.fieldVal = this.form.value[this.field.name][this.field.optionName]

      }
      console.log(this.fieldVal);

      
    }
    if (this.field.multiple) {
      let value = this.form.value[this.field.name];
      let optionValue = [];
      if (value)
        value.forEach((val) => {
          optionValue.push(
            val[this.field.optionName]
          );
        });
      this.multipleValue = optionValue.toString();
    }
    // this.form.controls[this.field.optionName].setValue(this.gridApi.getSelectedRows()[0])
    // this.form.addControl( this.field.name, new FormControl(this.gridApi.getSelectedRows()[0]) );
    this.formValue.emit(this.form.value);
  }

  @ViewChild("contentTemplate", { static: false }) contentTemplate: TemplateRef<
    any
  >;
  openWindow(contentTemplate) {
    this.getAllRowsForEntity();
    this.windowRef = this.windowService.open(this.contentTemplate, {
      title: "Select " + this.field.name,
      closeOnEsc: true,
      hasBackdrop: true,
      windowClass: "danger",
    });
  }
  searchValue ;

  filter(){
    this.gridApi.setQuickFilter(this.searchValue)
    }
}
function arrayContains(needle, arrhaystack)
{
    return (arrhaystack.indexOf(needle) > -1);
}