import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EntityService } from '../../../../services/entity.service';
import { API_URLS } from '../../../../API_URLS';


@Component({
  selector: 'dropdown',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss']
})
export class DropDownComponent  implements OnInit {

  @Input() field:any = {};
  @Input() form:FormGroup;
  @Input() isEditMode:boolean;
  @Output() formValue=new EventEmitter();
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }
  constructor() { }
  ngOnInit(): void {
      setTimeout(() => {
        this.form.get(this.field.name).setValue(this.field.value)
      }, 200);
  }
  selectedChange(event){    
    this.form.controls[this.field.name].setValue( event)
    this.formValue.emit(this.field.multiple ? [this.form.value]: this.form.value)
  }

}
