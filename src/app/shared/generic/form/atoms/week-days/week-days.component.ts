import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  TemplateRef,
  OnChanges,
  SimpleChanges,
  DoCheck,
} from "@angular/core";
import { FormGroup } from "@angular/forms";
import { EntityService } from "../../../../services/entity.service";
import { TranslateService } from "@ngx-translate/core";
import { NbWindowService, NbWindowRef } from "@nebular/theme";
import { OverrideService } from "../../../../services/override.service";

@Component({
  selector: "week-days",
  templateUrl: "./week-days.component.html",
  styleUrls: ["./week-days.component.scss"],
})
export class WeekDaysComponent implements DoCheck {
  @Input() field: any = {};
  @Input() isArray: boolean;
  @Input() form: FormGroup;
  @Input() isEditMode: boolean = false;
  @Output() formValue = new EventEmitter();
  date = Date.now();
  fieldVal: any = "";
  selected = [];
  rowSelection: string;
  gridApi: any;
  gridColumnApi: any;
  selectedRows: any;
  selectedRow: any;
  get isValid() {
    return this.form.controls[this.field.name].valid;
  }
  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }

  constructor(
    private dataService: EntityService,
    private translate: TranslateService,
    private windowService: NbWindowService,
    public override: OverrideService
  ) {}

  i = 0;

  ngDoCheck(): void {
    if (   this.form.value[this.field.name]  != undefined && this.isEditMode && this.i == 0 ) {
      let value = this.form.get(this.field.name).value;
      let values = new Array();
      if (this.field.multiple) {
        value.forEach((val) => {
          const elem = this.rowData.find((item) => item.value === val);
          if (elem != undefined)
           values.push(this.translate.instant(elem.name[this.override.currentLang]));
        });
      } else {        
        const elem = this.rowData.find((item) => item.value === value);
        if(elem != undefined)
          values.push(this.translate.instant(elem.name[this.override.currentLang]));
      }

      this.fieldVal = values.toString();
    }
    // throw new Error("Method not implemented.");
  }

  columnDefs = [
    {
      headerName: this.translate.instant("id"),
      field: "value",
      checkboxSelection: true,
      filter: true,
      sortable: true,
      editable: false,
    },
    {
      headerName: this.translate.instant("day"),
      field: "name."+[this.override.currentLang],
      filter: true,
      sortable: true,
      editable: false,
    },
  ];

  rowData = [
    {
      name: {
        en: "Sunday",
        ar: "الأحد",
      },
      value: 1,
    },
    {
      name: {
        en: "Monday",
        ar: "الاثنين",
      },
      value: 2,
    },
    {
      name: {
        en: "Tuesday",
        ar: "الثلاثاء",
      },
      value: 3,
    },
    {
      name: {
        en: "Wednesday",
        ar: "الاربعاء",
      },
      value: 4,
    },
    {
      name: {
        en: "Thursday",
        ar: "الخميس",
      },
      value: 5,
    },
    {
      name: {
        en: "Friday",
        ar: "الجمعة",
      },
      value: 6,
    },
    {
      name: {
        en: "Saturday",
        ar: "السبت",
      },
      value: 7,
    },
  ];

  onGridReady(params) {
    this.field.multiple
      ? (this.rowSelection = "multiple")
      : (this.rowSelection = "single");
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onSelectionChanged(event) {
    this.selectedRows = this.gridApi.getSelectedRows();
  }

  rowSelected(event) {
    this.selectedRow = event.data;
  }
  selectRow() {
    this.windowRef.close();

    let value = this.selectedRows;

    if (this.field.multiple) {
      let toSendArray = [];
      if (value != undefined)
      value.forEach((val) => {
        toSendArray.push(val.value);
      });
      
      this.form.controls[this.field.name].setValue(toSendArray);
      this.formValue.emit(this.form.value);
    } else {
      this.form.controls[this.field.name].setValue(value[0].value);
      this.formValue.emit(this.form.value);
    }

    let textVal = [];
    if (value != undefined)
    value.forEach((val) => {
      textVal.push(val.name[this.override.currentLang]);
    });
    
    this.fieldVal = textVal.toString();
  }

  windowRef: NbWindowRef;
  @ViewChild("contentTemplate", { static: false }) contentTemplate: TemplateRef<
    any
  >;
  openWindow(contentTemplate) {
    this.windowRef = this.windowService.open(this.contentTemplate, {
      title: this.translate.instant("Select Days"),
      closeOnEsc: true,
      hasBackdrop: true,
      windowClass: "danger",
    });
  }
}
