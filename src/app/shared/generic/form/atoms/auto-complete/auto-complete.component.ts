import { Component, OnInit, ViewChild, Input, Output, EventEmitter, DoCheck } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { EntityService } from '../../../../services/entity.service';
import { API_URLS } from '../../../../API_URLS';

@Component({
  selector: 'auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.scss']
})
export class AutoCompleteComponent implements OnInit {
  @Input() field: any = {};
  @Input() isArray: boolean;
  @Input() form: FormGroup;
  @Input() isEditMode: boolean;
  @Output() formValue = new EventEmitter();
  @Input()disabled: boolean
  data: any[];
  fieldVal: any = null;
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }

  options: any[] = [];
  filteredOptions$: Observable<string[]>;

  @ViewChild('autoInput', { static: false }) input;
  typed: String = "";

  constructor(private entity: EntityService) { }
 

  ngOnInit() {
    // this.getAllRowsForEntity()
    this.filteredOptions$ = of(this.options);
  }

  // ngDoCheck(): void {
  //   this.fieldVal = this.form.get(this.field.name)!=undefined?(this.form.get(this.field.name).value!=null?this.form.get(this.field.name).value[this.field.optionName]:null) : null
  // }

  getAllRowsForEntity(text?: String) {
    var getUrl = this.field.from
      ? API_URLS[this.field.from]["get"]
      : API_URLS[this.field.name]["get"];

    if (this.field.pathParams != undefined) {
      let params = this.field.pathParams as [];

      for (let i = 0; i < params.length; i++) {
        const element = params[i];
        if (i == 0)
          getUrl += "?" + element["key"] + "=" + element["value"];
        else
          getUrl += "&" + element["key"] + "=" + element["value"];

      }
    }
    if (text) {
      if (this.field.pathParams == undefined)
        getUrl += `?${this.field.optionName}=${text}`
      else
        getUrl += `&${this.field.optionName}=${text}`
    }

    this.entity.getAll(getUrl).then((res) => {
      res.subscribe((data) => {
        this.options = data as Array<any>;
        // this.data.forEach(item => this.options.push(item[this.field.optionName]));
      })
    })
  }
  private filter(value: {}): string[] {
    const filterValue = value;
    return this.options.filter(optionValue => optionValue.includes(filterValue));
  }

  getFilteredOptions(value: {}): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }

  onChange() {
    this.filteredOptions$ = this.getFilteredOptions(this.input.nativeElement.value);
    let val = this.input.nativeElement.value as String;
    if (val.length >= this.field.minL?this.field.minL:2) {
      this.typed = val;
      this.getAllRowsForEntity(val)
    }

  }

  date = new Date().getTime()
  onSelectionChange(event) {
  
    if (event && event != "") {
      this.fieldVal = event[this.field.optionName]
      this.form.get(this.field.name).setValue(event)
    
      this.formValue.emit(event);      
    }
  }
}
