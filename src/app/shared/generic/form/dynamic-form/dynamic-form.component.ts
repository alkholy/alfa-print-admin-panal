import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { genericForm } from '../genericForm';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { EntityService } from '../../../services/entity.service';
import { API_URLS } from '../../../API_URLS';

@Component({
  selector: 'dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit {
  @Input() input: genericForm;
  @Output() formValue = new EventEmitter(); ;
  @Input() isEditMode : boolean;
  form: FormGroup;
  entityId: any;
  constructor(
    private translate: TranslateService,
    private dataService: EntityService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

   async ngOnInit() {
    let fieldsCtrls = {};
    for (let f of this.input.fields) {

      if (f.type != 'checkbox' && f.type != 'file' && f.type != 'number' && !f.dateType) {
        fieldsCtrls[f.name] = new FormControl(
          {value: f.value , disabled: f.disabled},
          [f.required ? Validators.required : null, f.pattern ? Validators.pattern(f.pattern) : null]
        );
      }else if (f.dateType){
        fieldsCtrls[f.name] = new FormControl(
          {
            value: new DatePipe('en-US').transform(f.value, 'yyyy/MM/dd'),
            disabled: f.disabled
          },
          [f.required ? Validators.required : null, f.pattern ? Validators.pattern(f.pattern) : null]
        );
      } else if (f.type == 'number'){
        fieldsCtrls[f.name] = new FormControl(
          {value: f.value , disabled: f.disabled, },
          [f.required ? Validators.required : null, f.pattern ? Validators.pattern(f.pattern) : null]
        );
      } else if (f.type == 'email'){
        fieldsCtrls[f.name] = new FormControl(
          {value: f.value , disabled: f.disabled},
          [f.required ? Validators.required : null, f.pattern ? Validators.pattern(f.pattern) : null]
        );
      }else if (f.type == 'file'){
        fieldsCtrls[f.name] = new FormControl(
          {value: f.value , disabled: f.disabled},
          [f.required ? Validators.required : null, f.pattern ? Validators.pattern(f.pattern) : null]

        );
      } else {
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = new FormControl(opt.value);
        }
        fieldsCtrls[f.name] = new FormGroup(opts);
      }
    }

  this.form = new FormGroup(fieldsCtrls);

    this.entityId = this.route.snapshot.paramMap.get('entityId');
    if (this.entityId) {
      this.isEditMode = true;
      if (API_URLS[`${this.input.entityName}`]['getOne']){
        this.dataService.getOne(API_URLS[`${this.input.entityName}`]['getOne'], this.entityId).then(res => {
          res.subscribe(data => {
            for (let f of this.input.fields) {
              this.form.controls[f.name].setValue(data[f.name]);
            }
          });
        });
      } else {
        this.dataService.getOne(API_URLS[`${this.input.entityName}`]['get'], this.entityId).then(res => {
          res.subscribe(data => {
            for (let f of this.input.fields) {
              this.form.controls[f.name].setValue(data[f.name]);
            }
          });
        });
      }
    }

  }
  onSubmit(formValue: any) {
   if (this.isEditMode && API_URLS[`${this.input.entityName}`]['put']){
    this.dataService
    .update(API_URLS[`${this.input.entityName}`]['put'], formValue, this.entityId)
    .then(res => {
      res.subscribe(
        data => {
          this.router.navigate(['/' + this.input.path ? this.input.path : this.input.entityName + '/all']);
        },
        error => {
          // this.form.reset();
          this.dataService.showSuccessToast(error);

        }
      );
    });
   }
   else {
    this.dataService
    .save(API_URLS[`${this.input.entityName}`]['post'], formValue)
    .then(res => {
      res.subscribe(
        data => {
          this.router.navigate(['/' + this.input.path ? this.input.path : this.input.entityName + '/all']);
        },
        error => {
          // this.form.reset();
          this.dataService.showSuccessToast(error);

          // alert(this.translate.instant(error.error.error));
          // this.router.navigate(['/'+this.input.path?this.input.path:this.input.entityName+'/all'])
        }
      );
    });
   }
  }
  receiveFormValue(event){
    this.formValue.emit(event);
  }
}
