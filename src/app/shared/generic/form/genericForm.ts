export interface genericForm{
    entityName?: string;
    path?: string;
    fields?: any[] ;
    data?: [];
    processOut?: boolean;
    viewTitle?: string;
    noCard?: boolean;
    firestore?: boolean;
    NoButtons?: boolean;
}
