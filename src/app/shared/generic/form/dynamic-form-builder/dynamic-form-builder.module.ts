import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { DynamicFormBuilderComponent } from "./dynamic-form-builder.component";
import { FieldBuilderComponent } from "../field-builder/field-builder.component";
import { CheckBoxComponent } from "../atoms/check-box/check-box.component";
import { DropDownComponent } from "../atoms/drop-down/drop-down.component";
import { RadioComponent } from "../atoms/radio/radio.component";
import { TextBoxComponent } from "../atoms/text-box/text-box.component";
import { TranslateModule } from "@ngx-translate/core";
import { RouterModule } from "@angular/router";
import { SelectComponent } from "../atoms/select/select.component";
import { AgGridModule } from "ag-grid-angular";
import { ViewFormBuilderComponent } from "../view-form-builder/view-form-builder.component";
import { DynamicFormComponent } from "../dynamic-form/dynamic-form.component";
import { UrlComponent } from "../atoms/url/url.component";
import {
  NbInputModule,
  NbCardModule,
  NbButtonModule,
  NbWindowModule,
  NbDialogModule,
  NbCheckboxModule,
  NbTabsetModule,
  NbPopoverModule,
  NbSelectModule,
  NbTooltipModule,
  NbDatepickerModule,
  NbAlertModule,
  NbIconModule,
  NbActionsModule,
  NbProgressBarModule,
  NbThemeModule,
  NbListModule,
  NbUserModule,
} from "@nebular/theme";
import { TextLocalizedComponent } from "../atoms/text-localized/text-localized.component";
import { ThemeModule } from "../../../../@theme/theme.module";
import { WeekDaysComponent } from "../atoms/week-days/week-days.component";
import { DateComponent } from "../atoms/date/date.component";
// import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { TimeComponent } from '../atoms/time/time.component';
import { GroupComponent } from '../atoms/group/group.component';
import { ArrayComponent } from '../atoms/array/array.component';
import { DropdownEntityComponent } from '../atoms/dropdown-entity/dropdown-entity.component';
import { NgxPrintModule } from 'ngx-print';
import { FileComponent } from "../atoms/file/file.omponent";
import { AutoCompleteComponent } from '../atoms/auto-complete/auto-complete.component';
import { EditorComponent } from '../atoms/editor/editor.component';
import { EditorLocalizedComponent } from '../atoms/editor-localized/editor-localized.component';
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { BreadcrumbComponent } from 'src/app/pages/breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [
    FieldBuilderComponent,
    DynamicFormBuilderComponent,
    CheckBoxComponent,
    DropDownComponent,
    FileComponent,
    RadioComponent,
    SelectComponent,
    TextBoxComponent,
    UrlComponent,
    TextLocalizedComponent,
    ViewFormBuilderComponent,
    DynamicFormComponent,
    WeekDaysComponent,
    DateComponent,
    TimeComponent,
    GroupComponent,
    ArrayComponent,
    DropdownEntityComponent,
    AutoCompleteComponent,
    EditorComponent,
    EditorLocalizedComponent,
    BreadcrumbComponent

  ],
  imports: [
    CommonModule,
    AgGridModule.withComponents([SelectComponent]),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbWindowModule.forChild(),
    ThemeModule,
    NbDialogModule.forChild(),
    NbCheckboxModule,
    NbTabsetModule,
    NbPopoverModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbTooltipModule,
    NbCheckboxModule,
    NbDatepickerModule,
    // NgxMaterialTimepickerModule,
    NbTooltipModule,
    NbAlertModule,
    NbIconModule,
    NbActionsModule,
    NgxPrintModule,
    NbProgressBarModule,
    NbThemeModule,
    CKEditorModule,
    NbListModule,
    NbUserModule,
  ],
  exports: [
    DynamicFormBuilderComponent,
    ViewFormBuilderComponent,
    DynamicFormComponent,
    FieldBuilderComponent,
    DynamicFormBuilderComponent,
    CheckBoxComponent,
    DropDownComponent,
    FileComponent,
    RadioComponent,
    SelectComponent,
    TextBoxComponent,
    UrlComponent,
    TextLocalizedComponent,
    DynamicFormComponent,
    WeekDaysComponent,
    DateComponent,
    TimeComponent,
    GroupComponent,
    ArrayComponent,
    DropdownEntityComponent,
    AutoCompleteComponent,
    EditorComponent,
    EditorLocalizedComponent,
    BreadcrumbComponent
  ],
  providers: [DatePipe],
  entryComponents: [SelectComponent]
})
export class DynamicFormBuilderModule {}
