import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { DynamicFormBuilderComponent } from './dynamic-form-builder.component';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: "root",
})
export class CanDeactivateGuard

  implements CanDeactivate<DynamicFormBuilderComponent> {
      constructor(private translate:TranslateService){}

  canDeactivate(
    component: DynamicFormBuilderComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    
    if (component.forms['touched']) {
      if (component.saved)
        return true;
      else
    return confirm(
      this.translate.instant(
        "You have unsaved changes. Are you sure you want to leave? "
      )
    );
    } 
    // or, you can also handle the guard asynchronously, e.g.
    // asking the user for confirmation.
    return true;
  }
}

