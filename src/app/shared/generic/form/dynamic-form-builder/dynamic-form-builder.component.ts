import { FirebaseService } from './../../../services/firebase.service';
import {
  Component, OnInit, Output, EventEmitter, Input,  OnDestroy, DoCheck, } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, } from '@angular/forms';
import { genericForm } from '../genericForm';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart, NavigationCancel, ResolveEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { EntityService } from '../../../services/entity.service';
import { API_URLS } from '../../../API_URLS';
import { OverrideService } from '../../../services/override.service';

@Component({
  selector: 'dynamic-form-builder',
  templateUrl: './dynamic-form-builder.component.html',
  styleUrls: ['./dynamic-form-builder.component.scss'],
})
export class DynamicFormBuilderComponent implements OnInit, OnDestroy , DoCheck{
  @Input() input: genericForm;
  form: FormGroup;
  isEditMode: boolean;
  @Output() forms = new EventEmitter<FormGroup>();
  @Output() process = new EventEmitter();
  entityId: any;
  updatedAt: any;
  @Output() saved = new EventEmitter<boolean>() ;
  touched: boolean = false;
  constructor(
    private translate: TranslateService,
    private dataService: EntityService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public override: OverrideService,
    private datepipe: DatePipe,
    private firestore: FirebaseService,

  ) {
  //   router.events.subscribe((val) => {

  //     // see also
  //     // if(val instanceof NavigationStart) {
  //     //   // consol/e.log(this.form);
  //     //   if(this.form.touched === true && confirm(this.translate.instant("You have unsaved changes. Are you sure you want to leave?"))){

  //     //   } else {
  //     //    this.router.dispose()
  //     //   }
  //     // }


  // });
  }
  lang = ['ar', 'en'];
  currentLang = this.override.currentLang;
  ngOnDestroy(): void {
    this.saved.emit(false);
    this.touched = false;
  }
  ngDoCheck(): void {
    this.forms.emit(this.form);
  }
  async ngOnInit() {
    this.saved.emit(false);

    this.currentLang = this.override.currentLang;
    let fieldsCtrls = {};


    for (let f of this.input.fields) {

      if (
        f.type != 'file' &&
        f.type != 'number' &&
        !f.dateType &&
        f.type != 'text-localized' &&
        f.type != 'editor-localized' &&
        f.type != 'weekDays' &&
        f.type != 'date' &&
        f.type != 'time' &&
        f.type != 'checkbox' &&
        f.type != 'group' &&
        f.type != 'dropdown' &&
        f.type != 'array'
      ) {
        fieldsCtrls[f.name] = this.fb.control(
          { value: f.value || '', disabled: f.disabled },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'entity') {
        fieldsCtrls[f.name] = this.fb.control(
          { value: f.value || f.multiple ? [] : {}, disabled: f.disabled },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'text-localized') {
        let ctrl = {};
        ctrl['ar'] = this.fb.control(
          {
            value: f.value['ar']
              ? f.value['ar']
              : '' || '',
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
        ctrl['en'] = this.fb.control(
          {
            value: f.value['en']
              ? f.value['en']
              : '' || '',
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );

        fieldsCtrls[f.name] = this.fb.group(ctrl,  f.validators ? f.validators : [] );
        // console.log(fieldsCtrls[f.name]);

        // fieldsCtrls[f.name] = this.fb.group(ctrl);
        // console.log(fieldsCtrls[f.name]);
      }else if (f.type == 'editor-localized') {
        let ctrl = {};
        ctrl[this.override.currentLang] = this.fb.control(
          {   value: f.value[this.override.currentLang]  ? f.value[this.override.currentLang]     : '' || '',   disabled: f.disabled,  },  f.validators ? f.validators : []);

        fieldsCtrls[f.name] = this.fb.group(ctrl);
      }
       else if (f.type == 'checkbox') {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || false,
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'dropdown') {
        fieldsCtrls[f.name] = this.fb.control(

          {
            value: f.multiple ? [f.value] : f.value || f.multiple ? [] : '',
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'number') {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || 0,
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'weekDays') {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: [ f.value] || [],
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'date') {

        fieldsCtrls[f.name] = this.fb.control(
          {
            value: this.datepipe.transform(f.value, 'dd/MM/yyyy')   ,
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'time') {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: '',
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.dateType) {
        // console.log(f);

        fieldsCtrls[f.name] = this.fb.control(
          {

            value: this.datepipe.transform(f.value, 'dd/MM/yyyy'),
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'group') {
        let childs = {};

        f.children.forEach((child) => {
          childs[child.name] = this.fb.control(
            {
              value: child.value || '',
              disabled: child.disabled,
            },
            child.validators ? child.validators : []
          );
        });
        fieldsCtrls[f.name] = this.fb.group(
          childs,
          f.validators ? f.validators : []
        );
      } else if (f.type == 'array') {
        let childs = {};
        f.children.forEach(
          (child) => {
            childs[child.name] = this.fb.control(
              {
                value: child.value || '',
                disabled: child.disabled,
              },
              child.validators ? child.validators : []
            );
          },
          f.validators ? f.validators : []
        );
        let fg = this.fb.group(childs);
        fieldsCtrls[f.name] = this.fb.array([fg]);

      } else if (f.type == 'number') {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || 0,
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'email') {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || 0,
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == 'file') {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || '',
            disabled: f.disabled,
          },
          f.validators ? f.validators : []
        );
      } else {
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = this.fb.control(
            opt.value,
            f.validators ? f.validators : []
          );
        }

        fieldsCtrls[f.name] = this.fb.control(opts);
      }

    }

    this.form = this.fb.group(fieldsCtrls);
    this.entityId = this.route.snapshot.paramMap.get('entityId');

    if (this.entityId) {
      this.isEditMode = true;
      if (!this.input.firestore) {
        if (API_URLS[`${this.input.entityName}`]['getOne']) {
          await this.dataService
            .getOne(
              API_URLS[`${this.input.entityName}`]['getOne'],
              this.entityId
            )
            .then((res) => {
              res.subscribe((data) => {
                this.updatedAt = data.updatedAt;
                for (let f of this.input.fields) {
                  if (this.form.controls[f.name] && data[f.name]) {
                    if (f.type == 'text-localized') {
                      let field = this.form.controls[f.name] as FormGroup;
                      if (data[f.name][this.override.currentLang]) {
                        field.controls[this.override.currentLang].setValue(
                          data[f.name][this.override.currentLang]
                        );
                      }
                    } else if (f.type == 'editor-localized') {
                      let field = this.form.controls[f.name] as FormGroup;
                      if (data[f.name][this.override.currentLang]) {
                        field.controls[this.override.currentLang].setValue(
                          data[f.name][this.override.currentLang]
                        );
                      }
                    } else if (f.type == 'group') {
                      let childs = {};
                      let field = this.form.controls[f.name] as FormGroup;
                      f.children.forEach((child) => {
                        if (child.type == 'weekDays') {
                          let days =  data[f.name][child.name];
                          field.controls[child.name].setValue(days);
                        } else if (child.type == 'time'){
                            let time = data[f.name][child.name].substring(0, 5);
                            field.controls[child.name].setValue(time );
                        } else {
                          field.controls[child.name].setValue(
                            data[f.name][child.name]
                          );
                        }
                      });
                      fieldsCtrls[f.name] = this.fb.group(childs);
                    } else if (f.type === 'array') {
                      const field = this.form.controls[f.name] as FormArray;
                      // field.setValue([]);
                      // tslint:disable-next-line: prefer-for-of
                      for (let i = 0; i < data[f.name].length; i++) {
                        const element = data[f.name][i];
                        field.value.push(element);
                      }

                    } else if (f.type == 'date') {
                         this.form.controls[f.name].setValue(new Date(data[f.name]));

                    }



                    else {

                      this.form.controls[f.name].setValue(data[f.name]);
                      console.log(this.form);
                    }
                  }
                }
              });
            });
        }
      }else {
        this.firestore.getOne(this.entityId, this.input.entityName).subscribe(data => {
          // console.log(data);
          // this.form.patchValue(data);
          // console.log(this.form);
          for (let f of this.input.fields) {
            if (this.form.controls[f.name] && data[f.name]) {
              if (f.type == 'text-localized') {
                let field = this.form.controls[f.name] as FormGroup;
                if (data[f.name]['ar']) {
                  field.controls['ar'].setValue(
                    data[f.name]['ar']
                  );
                }
                if (data[f.name]['en']){
                  field.controls['en'].setValue(
                    data[f.name]['en']
                  );
                }
              } else if (f.type == 'editor-localized') {
                let field = this.form.controls[f.name] as FormGroup;
                if (data[f.name][this.override.currentLang]) {
                  field.controls[this.override.currentLang].setValue(
                    data[f.name][this.override.currentLang]
                  );
                }
              } else if (f.type == 'group') {
                let childs = {};
                let field = this.form.controls[f.name] as FormGroup;
                f.children.forEach((child) => {
                  if (child.type == 'weekDays') {
                    let days =  data[f.name][child.name];
                    field.controls[child.name].setValue(days);
                  } else if (child.type == 'time'){
                      let time = data[f.name][child.name].substring(0, 5);
                      field.controls[child.name].setValue(time );
                  } else {
                    field.controls[child.name].setValue(
                      data[f.name][child.name]
                    );
                  }
                });
                fieldsCtrls[f.name] = this.fb.group(childs);
              } else if (f.type === 'array') {
                const field = this.form.controls[f.name] as FormArray;
                // field.setValue([]);
                // tslint:disable-next-line: prefer-for-of
                for (let i = 0; i < data[f.name].length; i++) {
                  const element = data[f.name][i];
                  field.value.push(element);
                }

              } else if (f.type === 'date') {
                console.log("da",data[f.name]);

                   this.form.controls[f.name].setValue(new Date(data[f.name]));

              }



              else {

                this.form.controls[f.name].setValue(data[f.name]);
                console.log(this.form);
              }
            }
          }

        });


      }

    }
  }
  onSubmit(formValue: any) {
    // cast integers
    this.input.fields.forEach((field) => {

      if (field.type === 'number') {
         this.form.controls[field.name].setValue(
            +this.form.controls[field.name].value
          );
      }

      if (field.type === 'entity' && this.form.get(field.name).value === ''){
            this.form.controls[field.name].setValue(null);
          }

      if (field.type === 'time'){
            const val = this.form.controls[field.name].value;
            console.log(val);
          }


    });

    let value = this.form.value;
    if (! this.input.processOut){
      if (!this.input.firestore) {
        if (this.isEditMode && API_URLS[`${this.input.entityName}`]['put']) {

          var val = value;
          val['id'] = this.entityId;
          this.dataService
            .update(
              API_URLS[`${this.input.entityName}`]['put'],
              val,
              this.entityId
            )
            .then((res) => {
              res.subscribe(
                (data) => {
                  this.saved.emit(true);
                  this.router.navigate([ '/pages/' + this.input.entityName + '/all' ]);
                  // this.form.reset();
                },
                (error) => {
                  // this.router.navigate(["/pages/" + this.input.path || this.input.entityName + "/all"]);
                  this.dataService.showErrorToast(error);
                  // alert(error.error);
                  // this.form.reset();
                }
              );
            });
        } else {
          // console.log(value);
          // return false
          this.dataService
            .save(
              API_URLS[`${this.input.entityName}`]['post'],
              value
            )
            .then((res) => {

              res.subscribe(
                (data) => {

                  this.saved.emit(true);
                  this.router.navigate([ '/pages/' + this.input.entityName + '/all' ]);
                  // this.form.reset();
                },

                (error) => {
                  this.dataService.showErrorToast(error);

                  // this.router.navigate(["/pages/" + this.input.path || this.input.entityName + "/all"]);
                  // alert(error.error);
                  // this.form.reset();
                }
              );
            });
        }
      }else{
        if (!this.entityId) {
          this.firestore.create(value, this.input.entityName).then(res => {
            this.router.navigate([this.input.path || 'pages/' + this.input.entityName + '/all']);
          })
        } else {
          value.id = this.entityId

          this.firestore.update(value, this.input.entityName)
          this.router.navigate([this.input.path || 'pages/' + this.input.entityName + '/all']);
        }
      }

    } else {
      this.process.emit(true);
    }
  }
  saveDraft(){
    alert(this.translate.instant('Coming Soon'));
  }
  print(): void {
    window.print();
}

logform(formstatus){
  console.log(formstatus);

}
}

