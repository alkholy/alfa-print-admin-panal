import { Component, OnInit, Input, Output } from '@angular/core';
import { genericForm } from '../genericForm';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { EventEmitter } from '@angular/core';
import { EntityService } from '../../../services/entity.service';
import { API_URLS } from '../../../API_URLS';
import { OverrideService } from '../../../services/override.service';

@Component({
  selector: 'view-form',
  templateUrl: './view-form-builder.component.html',
  styleUrls: ['./view-form-builder.component.scss']
})
export class ViewFormBuilderComponent implements OnInit {
  @Input() input: genericForm;
  form: FormGroup;
  isEditMode: boolean;
  @Output() forms = new EventEmitter<FormGroup>()
  entityId: any;
  updatedAt: any;
  constructor(
    private translate: TranslateService,
    private dataService: EntityService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public override: OverrideService
  ) {}
  lang = ["ar", "en"];
  currentLang = this.override.currentLang;
  async ngOnInit() {
    this.currentLang = this.override.currentLang;
    let fieldsCtrls = {};
    for (let f of this.input.fields) {
      
      if (
        f.type != "file" &&
        f.type != "number" &&
        !f.dateType &&
        f.type != "text-localized" &&
        f.type != "weekDays" &&
        f.type != "date" &&
        f.type != "time" &&
        f.type != "checkbox" &&
        f.type != "group" &&
        f.type != "dropdown"&&
        f.type != "array"
      ) {
        fieldsCtrls[f.name] = this.fb.control(
          { value: f.value || "", disabled: true },
          f.validators ? f.validators : []
        );
      } else if (f.type == "entity") {
        fieldsCtrls[f.name] = this.fb.control(
          { value: f.value || f.multiple ? [] : {}, disabled: true },
          f.validators ? f.validators : []
        );
      } else if (f.type == "text-localized") {
        let ctrl = {};
        ctrl[this.override.currentLang] = this.fb.control(
          {
            value: f.value[this.override.currentLang]
              ? f.value[this.override.currentLang]
              : "" || "",
            disabled: true,
          },
          f.validators ? f.validators : []
        );
        fieldsCtrls[f.name] = this.fb.group(ctrl);
      } else if (f.type == "checkbox") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || false,
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "dropdown") {
        fieldsCtrls[f.name] = this.fb.control(
          
          {
            value: f.multiple ? [f.value]:f.value || f.multiple?[]:"",
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "number") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || 0,
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "weekDays") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value:[ f.value] || [],
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "date") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value:new DatePipe("en-US").transform(f.value, "yyyy/MM/dd")   ,
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "time") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: '',
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.dateType) {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: new DatePipe("en-US").transform(f.value, "yyyy/MM/dd"),
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "group") {
        let childs = {};

        f.children.forEach((child) => {
          childs[child.name] = this.fb.control(
            {
              value: child.value ,
              disabled: true,
            },
            child.validators ? child.validators : []
          );
        });
        fieldsCtrls[f.name] = this.fb.group(
          childs,
          f.validators ? f.validators : []
        );
      } else if (f.type == "array") {
        let childs = {};
        f.children.forEach(
          (child) => {
            childs[child.name] = this.fb.control(
              {
                value: child.value ,
                disabled: true,
              },
              child.validators ? child.validators : []
            );
          },
          f.validators ? f.validators : []
        );
        let fg = this.fb.group(childs);
        fieldsCtrls[f.name] = this.fb.array([fg]);
        
      } else if (f.type == "number") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value ,
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "email") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value ,
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else if (f.type == "file") {
        fieldsCtrls[f.name] = this.fb.control(
          {
            value: f.value || "",
            disabled: true,
          },
          f.validators ? f.validators : []
        );
      } else {
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = this.fb.control(
            opt.value,
            f.validators ? f.validators : []
          );
        }

        fieldsCtrls[f.name] = this.fb.control(opts);
      }
    }

    this.form = this.fb.group(fieldsCtrls);
    this.entityId = this.route.snapshot.paramMap.get("entityId");

    if (this.entityId) {
      this.isEditMode = true;
      if (API_URLS[`${this.input.entityName}`]["getOne"]) {
        await this.dataService
          .getOne(
            API_URLS[`${this.input.entityName}`]["getOne"],
            this.entityId
          )
          .then((res) => {
            res.subscribe((data) => {
              this.updatedAt= data.updatedAt;
              for (let f of this.input.fields) {
                if (this.form.controls[f.name] && data[f.name])
                  if (f.type == "text-localized") {
                    let field = this.form.controls[f.name] as FormGroup;
                    if (data[f.name][this.override.currentLang]) {
                      field.controls[this.override.currentLang].setValue(
                        data[f.name][this.override.currentLang]
                      );
                    }
                  } else if (f.type == "group") {
                    let childs = {};
                    let field = this.form.controls[f.name] as FormGroup;
                    f.children.forEach((child) => {
                      if (child.type == "weekDays") {
                        let days =  data[f.name][child.name];
                        field.controls[child.name].setValue(days);
                      } else if (child.type=="time"){
                          let time = data[f.name][child.name].substring(0,5);                          
                          field.controls[child.name].setValue(time );
                      } else {
                        field.controls[child.name].setValue(
                          data[f.name][child.name]
                        );
                      }
                    });
                    fieldsCtrls[f.name] = this.fb.group(childs);
                  } else if (f.type == "array") {                
                    let field = this.form.controls[f.name] as FormArray;
                    // field.setValue([]);
                    for (let i = 0; i < data[f.name].length; i++) {
                      const element = data[f.name][i];
                      field.value.push(element);                      
                    }
                    
                  } else if (f.type == "date") {
                       this.form.controls[f.name].setValue(new Date(data[f.name]));


                  } 
                  
                  
                  
                  else {
                    console.log(data[f.name]);
                    
                    this.form.controls[f.name].setValue(data[f.name]);
                  }
              }
            });
          });
      }
    }
  }
  print(){
    window.print()
  }
}
