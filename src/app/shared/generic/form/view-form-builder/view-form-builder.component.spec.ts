import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFormBuilderComponent } from './view-form-builder.component';

describe('ViewFormBuilderComponent', () => {
  let component: ViewFormBuilderComponent;
  let fixture: ComponentFixture<ViewFormBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFormBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFormBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
