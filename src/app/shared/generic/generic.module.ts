import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllGenericComponent } from './all-generic/all-generic.component';

// import { AgGridModule } from "ag-grid-angular";
import { AgGridModule } from 'ag-grid-angular';

import {
  TranslateModule,
} from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridButtonDeleteComponent } from './ag-grid-button-delete/ag-grid-button-delete.component';
import { DynamicFormBuilderModule } from './form/dynamic-form-builder/dynamic-form-builder.module';
import { RouterModule } from '@angular/router';
import { IdRenderComponent } from './id-render/id-render.component';
import { AgGridButtonApproveComponent } from './ag-grid-button-approve/ag-grid-button-approve.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import {
  NbCardModule,
  NbButtonModule,
  NbActionsModule,
  NbIconModule,
  NbPopoverModule,
  NbToastrModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import { AgGridButtonRunReportComponent } from './ag-grid-button-run-report/ag-grid-button-run-report.component';
import { AgGridResetPasswordComponent } from './ag-grid-reset-password/ag-grid-reset-password.component';
import { RowsComponent } from './rows/rows.component';
import { SelectEntityComponent } from './rows-editing/select-entity/select-entity.component';
import { NumberRendererComponent } from './rows-editing/number-renderer/number-renderer.component';
import { EntityPopupComponent } from './rows-editing/entity-popup/entity-popup.component';
import { TableComponent } from 'src/app/pages/table/table.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AllGenericComponent,
    AgGridButtonDeleteComponent,
    AgGridButtonApproveComponent,
    IdRenderComponent,
    AgGridButtonRunReportComponent,
    AgGridResetPasswordComponent,
    RowsComponent,
    SelectEntityComponent,
    NumberRendererComponent,
    EntityPopupComponent,
    TableComponent,

  ],
  imports: [
    RouterModule,
    CommonModule,
    TranslateModule.forChild(),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DynamicFormBuilderModule,
    AgGridModule.withComponents([AllGenericComponent, RowsComponent]),
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbIconModule,
    NbPopoverModule,
    NbToastrModule,
    NbInputModule,
    NbSelectModule
  ],
  exports: [AllGenericComponent, AgGridButtonDeleteComponent, SelectEntityComponent, RowsComponent],
  entryComponents: [
    AgGridButtonDeleteComponent,
    IdRenderComponent,
    AgGridButtonApproveComponent,
    AgGridButtonRunReportComponent,
    AgGridResetPasswordComponent,
    SelectEntityComponent, NumberRendererComponent, EntityPopupComponent
  ],
})
export class GenericModule {}
