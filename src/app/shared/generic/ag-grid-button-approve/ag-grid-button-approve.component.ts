import { Component, OnInit } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { EntityService } from "../../services/entity.service";
import { API_URLS } from "../../API_URLS";

@Component({
  selector: "app-ag-grid-button-approve",
  templateUrl: "./ag-grid-button-approve.component.html",
  styleUrls: ["./ag-grid-button-approve.component.css"]
})
export class AgGridButtonApproveComponent implements ICellRendererAngularComp {
  constructor(private entity: EntityService) { }
  params: any;
  getLabelFunction: any;
  cell: { row: any; col: any };

  agInit(params: any): void {
    this.params = params;
    this.getLabelFunction = this.params.getLabelFunction;
    this.cell = { row: params.value, col: params.colDef.field };
  }

  refresh(params?: any): boolean {
    return true;
  }
  isPopup(): boolean {
    return false;
  }
  onClick(changed?:string) {
    var data = {
      id: this.params.data.id,
      status: this.params.data.status
    }
   

    data.status = changed

    
    this.entity
      .update(API_URLS[this.params.colDef.from].put, data, data.id)
      .then(res => {
        res.subscribe(rDta => {
          this.cell.row = rDta.status
        });
      });
  }
}
