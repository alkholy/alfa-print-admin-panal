import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { user } from '../login/user';
import { NbAuthSocialLink } from '@nebular/auth';
import { ApiDataService } from '../../services/api-data.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EntityService } from '../../services/entity.service';
import { NbToastrService } from '@nebular/theme';
import { OverrideService } from '../../services/override.service';

@Component({
  selector: "ngx-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  options: {};
  cd: ChangeDetectorRef;
  form: FormGroup;
  redirectDelay: number;
  showMessages: any;
  strategy: string;
  errors: string[];
  messages: string[];
  user: user = {
    username: "",
    password: "",
    fullName: "",
    phoneNumber: "",
    userType: "",
    email:""
  };
  submitted: boolean;
  socialLinks: NbAuthSocialLink[];
  rememberMe: boolean;
  constructor(
    private data: ApiDataService,
    private router: Router,
    private translate: TranslateService,
    private entity: EntityService,
    private toastrService: NbToastrService,
    private override: OverrideService
  ) {}
  ngOnInit() {}
  register() {
    console.log(this.user);
    
  }
}
