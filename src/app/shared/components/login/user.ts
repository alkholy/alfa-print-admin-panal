export interface user {
  username?: string;
  password?: string;
  confirmPassword?:string,
  fullName?: string;
  phoneNumber?: string;
  email?: string;
  userType?:string
}
