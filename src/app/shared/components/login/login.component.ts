import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NbAuthService, NbAuthSocialLink } from '@nebular/auth';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { user } from './user';
import { ApiDataService } from '../../services/api-data.service';
import { API_URLS } from '../../API_URLS';
import { CookieConsentService } from '../../services/cookie-consent.service';
import { TranslateService } from '@ngx-translate/core';
import { EntityService } from '../../services/entity.service';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';
import { OverrideService } from '../../services/override.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  options: {};
  cd: ChangeDetectorRef;
  form: FormGroup;
  redirectDelay: number;
  showMessages: any;
  strategy: string;
  errors: string[];
  messages: string[];
  user: user = {
    username: '',
    password: ''
  };
  submitted: boolean;
  socialLinks: NbAuthSocialLink[];
  rememberMe: boolean;
  constructor(
    private data: ApiDataService,
    private router: Router,
    private translate: TranslateService,
    private entity: EntityService,
    private toastrService: NbToastrService,
    private override: OverrideService
  ) { }
  login() {
    this.data.login(API_URLS.auth.auth, this.user).then(res => {
      res.subscribe(data => {
        sessionStorage.setItem(btoa('userName'), btoa(this.user.username))
        sessionStorage.setItem("toekn", data.access_token);

        this.toastrService.show(
          this.translate.instant('Logged In Successfully'), '', {
          status: 'info' as NbComponentStatus,
          destroyByClick: true,
          duration: 5000,
          hasIcon: false,
          position: this.override.isRTL() ? NbGlobalPhysicalPosition.BOTTOM_LEFT : NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          preventDuplicates: false,
        });

        this.entity.getAll(API_URLS.user.get + "?userName=" + this.user.username).then(res => {
          res.subscribe(data => {
            let user = data[0];
            
            sessionStorage.setItem("fullName", user.fullName);

          })
        });
        this.router.navigate(["/"]);

      }, error => this.showErrorToast(error))
    }, error => {
      this.showErrorToast(error)
    })


  }
  // getConfigValue(key: string): any;
  ngOnInit() {
    sessionStorage.clear()
  }

  showErrorToast(error) {
    this.toastrService.show(
      this.translate.instant(`${error.error.message} Please Check Your Username and Password`), error.error.status, {
      status: 'danger' as NbComponentStatus,
      destroyByClick: true,
      duration: 5000,
      hasIcon: false,
      position: this.override.isRTL() ? NbGlobalPhysicalPosition.TOP_LEFT : NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    });
  }

}
