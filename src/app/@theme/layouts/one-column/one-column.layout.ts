import { Component } from '@angular/core';
import { OverrideService } from 'src/app/shared/services/override.service';

@Component({
  selector: 'ngx-one-column-layout',
  styleUrls: ['./one-column.layout.scss'],
  template: `
    <nb-layout windowMode [ngStyle]="{'direction': override.currentLang == 'en'? 'ltr': 'rtl','text-align': override.currentLang == 'en'? 'left': 'right'}" >
      <nb-layout-header fixed>
        <!-- <ngx-header></ngx-header> -->
        <app-navbar></app-navbar>

      </nb-layout-header>

      <nb-sidebar class="menu-sidebar" tag="menu-sidebar"  responsive>
        <ng-content select="app-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column>
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>

      <nb-layout-footer fixed>
        <ngx-footer></ngx-footer>
      </nb-layout-footer>
    </nb-layout>
  `,
})
export class OneColumnLayoutComponent {
  constructor(
    public override: OverrideService
  ){}
}
