// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// export const environment = {
//   production: false,
//   serverUrl: "",
//   contextURI: "",
//   clientId: ""
// };

export const environment = {
  production: false,
  owner: 'Override',
  clientId: "",
  // serverUrl: 'http://localhost:8080/opass',
  // serverUrl: 'http://196.221.165.241:8080/opass',
  // serverUrl: 'http://localhost:8080/EWJ',
  serverUrl: 'http://169.51.198.76:8082/EWJ',
  contextURI: '',
  firebaseConfig : {
    apiKey: 'AIzaSyAd1CalmN09seTTgStX7CgKfuFtk2-UEPs',
    authDomain: 'alfaprint-9cb7b.firebaseapp.com',
    databaseURL: 'https://alfaprint-9cb7b.firebaseio.com',
    projectId: 'alfaprint-9cb7b',
    storageBucket: 'alfaprint-9cb7b.appspot.com',
    messagingSenderId: '449583126700',
    appId: '1:449583126700:web:a6b50935370ef9a0182709',
    measurementId: 'G-MC1WMF7RGY'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
